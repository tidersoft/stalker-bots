using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {


public int health;
public GameObject Body;
public GameObject deadR;
private bool  isDead;

void Start (){
	
}

void Update (){
	if(health <= 0)
	{
		if(!isDead)
			Die();
		else
			return;
	}
}

void ApplyDamage ( int damage  ){
	health -= damage;
	//FIXME_VAR_TYPE rotation= Quaternion.FromToRotation(Vector3.up, Vector3.up);
	//Instantiate (deadR, transform.position, rotation);
	 
}

void Die (){
	isDead = true;
	Quaternion rotation= Quaternion.FromToRotation(Vector3.up, Vector3.up);
	Instantiate (deadR, transform.position, rotation);
	Destroy(Body);
}

void OnGUI (){
	if(gameObject.tag == "Player")
	{
		//GUI.skin = mySkin;
		//FIXME_VAR_TYPE style1= mySkin.customStyles[0];
		GUI.Label ( new Rect(Screen.width - 200,Screen.height-45,200,80),""+health);
	}	
}



}