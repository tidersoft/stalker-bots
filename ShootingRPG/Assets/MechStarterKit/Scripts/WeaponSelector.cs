using UnityEngine;
using System.Collections;

public class WeaponSelector : MonoBehaviour {


private Weapon selectedWep;


    public  GameObject[] wepons;
    public GameObject[] sockets;

private int wepNum;

public void Setup (int loud){

        wepons = new GameObject[ItemData.instance.mechloud[loud].weapons.Length];

        for (int i = 0; i < wepons.Length; i++) {
            if (wepons[i] == null) {
                GameObject obj = Instantiate(ItemData.instance.mechloud[loud].weapons[i].gameObject,sockets[i].transform.position,sockets[i].transform.rotation) as GameObject;
                obj.transform.parent = sockets[i].transform;
                wepons[i] = obj;
                wepons[i].transform.localPosition = wepons[i].GetComponent<Weapon>().socetPos;
                wepons[i].GetComponent<Weapon>().wepCamera=sockets[i].GetComponentInChildren<Camera>().gameObject;
                wepons[i].GetComponentInChildren<Crosshair>().cam = sockets[i].GetComponentInChildren<Camera>();
            }
        }

        SelectWeapon(0);
    
    }

void Update (){
	
	if(GameMenager.Instance.InputControler.changeWeapon)
	{
		wepNum += 1;

            
                wepNum = wepNum % wepons.Length;

            SelectWeapon(wepNum);
		GetComponent<AudioSource>().Play();
	}
	/*else if(Input.GetKeyDown(KeyCode.E))
	{
		wepNum -= 1;

            if (wepNum < 0)
                wepNum = wepons.Length - 1;

            SelectWeapon(wepNum);
		GetComponent<AudioSource>().Play();
	}
		*/
	

}

    public void SelectWeapon(int j) {
        if (wepons.Length > 0 && j>-1 && j<wepons.Length)
        {
            selectedWep = wepons[j].GetComponent<Weapon>();
            for (int i = 0; i < wepons.Length; i++)
            {
                if (j == i)
                {
                    wepons[i].GetComponent<Weapon>().selected = true;
                }
                else
                {
                    wepons[i].GetComponent<Weapon>().selected = false;
                }
            }
        }
    }

void OnGUI (){
        if (selectedWep == null)
            return;

		GUI.Label ( new Rect(Screen.width - 200, Screen.height-25 ,200,80),"" + selectedWep.GetComponent<Weapon>().nameS);
}






}