using UnityEngine;
using System.Collections;
using Photon.Pun;

public class Turret : MonoBehaviour {


public Transform target;
public float minDis;

public float fireRate = 0.1f;

private int m_LastFrameShot = -10;
private float nextFireTime = 0.0f;

public GameObject muzzleFlash;

public GameObject projectile;
private int ammoLeft;
public int maxAmmo;
public int magazines;
//float projectileSpeed;
public Transform Spawn;
public float reloadTime;

//KickBack
public Transform kickGO;
public float kickUpside = 0.5f;
public float kickSideways = 0.5f;
private bool  canFire;
    public int str = 0;
    public int matk = 0;
    public string shooter = "Enemy";
public Rigidbody Shell;

void Start (){
//	target = GameObject.FindGameObjectWithTag("Player").transform;
	ammoLeft = maxAmmo;
	canFire = true;
}

void Update (){
	if(GameObject.FindGameObjectWithTag("Player")!=null)
		target = GameObject.FindGameObjectWithTag("Player").transform;
    if (target == null)
            return;
	Vector3 relativePos= target.position - transform.position;
	Quaternion rotation= Quaternion.LookRotation(relativePos);
	transform.rotation = rotation;
	
	float distance= Vector3.Distance(target.position, transform.position);

        if (distance <= minDis && canFire == true)
        {
            RaycastHit hit;
            // targetpos *= 100000.0f;

            if (Physics.Raycast(transform.position, transform.forward, out hit, distance+1000))
            {
                if(hit.collider.tag.Equals("Player"))
                Fire();
            }
        }
	
}

IEnumerator Reload (){
	canFire = false;
	yield return new WaitForSeconds(reloadTime);
	canFire = true;
		ammoLeft = maxAmmo;
		magazines -= 1;
		Fire();
}

void LateUpdate (){
	if (m_LastFrameShot == Time.frameCount){
		muzzleFlash.transform.localRotation = Quaternion.AngleAxis(Random.value * 360, Vector3.forward);
		muzzleFlash.active = true;		
    }else{
		muzzleFlash.active = false;
	}	
}

void Fire (){
	if (Time.time - fireRate > nextFireTime)
		nextFireTime = Time.time - Time.deltaTime;

	while(nextFireTime < Time.time)
	{
		if(ammoLeft <= 0 && magazines > 0)
		{
		StartCoroutine(Reload());
		}
			
		FireOneShot();
		nextFireTime = Time.time + fireRate;
    }
}

void FireOneShot (){
	if(ammoLeft <= 0)
		return;
		
		
	
//	Rigidbody bullet= Instantiate(projectile,Spawn.transform.position,Spawn.transform.rotation);
        //Physics.IgnoreCollision(bullet.collider, gameObject.FindWithTag("Turret").collider);
        GameObject bullet = Instantiate(projectile, Spawn.transform.position, Spawn.transform.rotation);
        if (bullet.GetComponent<BulletStatus>() != null)
            bullet.GetComponent<BulletStatus>().Setting(str, matk, shooter, transform.root.gameObject, bullet.transform.forward,bullet.transform.forward);
        else
            bullet.GetComponent<BulletColider>().Setting(str, matk, shooter, transform.root.gameObject, bullet.transform.forward, bullet.transform.up);

        Rigidbody bullets = Instantiate(Shell,Spawn.transform.position,Spawn.transform.rotation);
	//Physics.IgnoreCollision(bullets.collider, gameObject.FindWithTag("Turret").transform.root.collider);
	bullets.velocity = transform.TransformDirection(new Vector3 ( Random.Range(-5,5) , 10 , Random.Range(-5,5) ));
	
	GetComponent<AudioSource>().Play();
	m_LastFrameShot = Time.frameCount;
	KickBack();
	ammoLeft -= 1;
}

void KickBack (){
    kickGO.localRotation = Quaternion.Euler(kickGO.localRotation.eulerAngles - new Vector3(kickUpside, Random.Range(-kickSideways, kickSideways), 0));   
}



}