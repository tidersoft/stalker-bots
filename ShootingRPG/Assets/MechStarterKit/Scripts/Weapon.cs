using UnityEngine;
using System.Collections;
using Photon.Pun;

public class Weapon : MonoBehaviour {

public string nameS;


public bool  selected;

public GameObject wepCamera;

public GameObject projectile;
private int ammoLeft;
public int maxAmmo;
public int magazines;
//float projectileSpeed;
public Transform Spawn;
//AudioClip soundFire;

public float fireRate = 0.1f;

private int m_LastFrameShot = -10;
private float nextFireTime = 0.0f;
public GameObject muzzleFlash;
    public Vector3 socetPos;
public bool  isAuto;

//KickBack
public Transform kickGO;
public float kickUpside = 0.5f;
public float kickSideways = 0.5f;

public GUISkin mySkin;
  public   bool reloading=false;
   public  float ReloadTime = 5.0f;
    float ReloadCounter = 0.0f;
    float ReloadStart;
    public int str = 0;
    public int matk = 0;
private MouseLookMech mouseLookScript;
public Rigidbody Shell;
    public SimpleHealthBar reloadBar;

    void Awake (){
	muzzleFlash.active = false;
	ammoLeft = maxAmmo;
        GameObject.Find("Canvas").transform.Find("Simple Bar").gameObject.SetActive(true);
        reloadBar = GameObject.Find("Canvas").GetComponentInChildren<SimpleHealthBar>();
}

void Update (){
        if(mouseLookScript==null)
            if(transform.parent.GetComponent<MouseLookMech>()!=null)
        mouseLookScript = transform.parent.GetComponent<MouseLookMech>();

        if (reloadBar==null)
            reloadBar = GameObject.Find("Canvas").transform.Find("Simple Bar").GetComponentInChildren<SimpleHealthBar>();
        if (reloading)
        {
            if (ReloadCounter > ReloadTime)
            {
                ammoLeft = maxAmmo;
                reloadBar.transform.parent.gameObject.SetActive(false);

                reloading = false;
            }
            ReloadCounter += Time.deltaTime;

        }
        if (selected)
	{
		mouseLookScript.enabled = true;
		wepCamera.active = true;
            if (!reloading)
            {
                reloadBar.transform.parent.gameObject.SetActive(false);
                if (GameMenager.Instance.InputControler.Fire1)
                {
                    if (!isAuto)
                        Fire();
                }
                if (GameMenager.Instance.InputControler.Fire1)
                {
                    if (isAuto)
                        Fire();
                }

                if (GameMenager.Instance.InputControler.Reload)
                {

                    ReloadStart = Time.time;
                    ReloadCounter = 0;
                    reloadBar.transform.parent.gameObject.SetActive(true);
                    reloadBar.UpdateBar(0, ReloadTime);
                    reloading = true;




                }
            }
            else {
                reloadBar.transform.parent.gameObject.SetActive(true);
                reloadBar.UpdateBar(ReloadCounter, ReloadTime);
            }
           
        }
	else
        {
            wepCamera.active = false;
		mouseLookScript.enabled = false;
	}
       
    }

void LateUpdate (){
	if (m_LastFrameShot == Time.frameCount){
		muzzleFlash.transform.localRotation = Quaternion.AngleAxis(Random.value * 360, Vector3.forward);
		muzzleFlash.active = true;		
    }else{
		muzzleFlash.active = false;
	}	
}

void Fire (){
	if (Time.time - fireRate > nextFireTime)
		nextFireTime = Time.time - Time.deltaTime;

	while(nextFireTime < Time.time)
	{
		if(ammoLeft <= 0)
			return;
			
		FireOneShot();
		nextFireTime = Time.time + fireRate;
    }
}

void FireOneShot (){
	if(ammoLeft <= 0)
		return;
	
	//Rigidbody bullet= Instantiate(projectile,Spawn.transform.position,Spawn.transform.rotation);
	//Physics.IgnoreCollision(bullet.GetComponent<Collider>(), GameObject.FindWithTag("Player").GetComponent<Collider>());//.transform.root.collider);
        GameObject bullet = Instantiate(projectile, Spawn.transform.position, Spawn.transform.rotation);
        if (bullet.GetComponent<BulletStatus>() != null)
            bullet.GetComponent<BulletStatus>().Setting(str, matk, "Player", transform.root.gameObject, bullet.transform.forward,bullet.transform.forward);
        else
            bullet.GetComponent<BulletColider>().Setting(str, matk, "Player", transform.root.gameObject, bullet.transform.forward, bullet.transform.up);

        Debug.Log(transform.root.gameObject.name);

        Rigidbody bullets = Instantiate(Shell,Spawn.transform.position,Spawn.transform.rotation);
	//Physics.IgnoreCollision(bullets.collider, gameObject.FindWithTag("Turret").transform.root.collider);
	bullets.velocity = transform.TransformDirection(new Vector3 ( Random.Range(-5,5) , 10 , Random.Range(-5,5) ));
	bullets.transform.localRotation = Quaternion.AngleAxis(Random.value * 360, Vector3.right);

	//bullet.velocity = transform.TransformDirection(Vector3 (0, 0, projectileSpeed));
	
	GetComponent<AudioSource>().Play();
	m_LastFrameShot = Time.frameCount;
	KickBack();
	ammoLeft -= 1;
}

void KickBack (){
    kickGO.localRotation = Quaternion.Euler(kickGO.localRotation.eulerAngles - new Vector3(kickUpside, Random.Range(-kickSideways, kickSideways), 0));   
}
void OnGUI (){
	if(selected)
	{
		GUI.skin = mySkin;
		GUIStyle style1= mySkin.customStyles[0];
		GUI.Label ( new Rect(Screen.width - 200,Screen.height-35,200,80),"Ammo : ");
		GUI.Label ( new Rect(Screen.width - 110,Screen.height-35,200,80),"" + ammoLeft, style1);
		
	}	
}






}