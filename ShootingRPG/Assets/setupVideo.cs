﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class setupVideo : MonoBehaviour
{

    public Slider QualitySlider;
    public Slider RenderSlider;
    public Toggle AAToggle;

    // Start is called before the first frame update
    void OnEnable()
    {
        QualitySlider.value = PlayerPrefs.GetInt("QualiyLevel");
     
        AAToggle.isOn= Boolean.Parse(PlayerPrefs.GetString("isAA"));
      
        RenderSlider.value = PlayerPrefs.GetFloat("Render");
      
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
