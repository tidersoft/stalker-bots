package com.plugin.reset;

import android.app.Activity;
import android.app.Instrumentation;
import android.view.KeyEvent ;
import java.lang.Thread;
import java.lang.Runnable;
import java.lang.InterruptedException;


public class Reset {



private Activity activity;
	
	public Reset(Activity activity) 
	{
		
		this.activity = activity;
	}


    public void ResetApp(){

	activity.runOnUiThread(new Runnable() {
			@Override
			public void run() 
			{        try {
            Instrumentation inst = new Instrumentation();
                inst.sendKeySync(new KeyEvent(KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_HOME));
                Thread.sleep(5000);
								inst.sendKeySync(new KeyEvent(KeyEvent.ACTION_UP,KeyEvent.KEYCODE_HOME));
								Thread.sleep(1000);
              
                inst.sendKeyDownUpSync(KeyEvent.KEYCODE_BACK);
                Thread.sleep(2000);
            }
            catch(InterruptedException e){
            }
        }   
    });
         
    }
}