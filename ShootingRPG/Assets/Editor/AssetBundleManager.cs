﻿using System.Collections;
using System.IO;
using UnityEditor;
using UnityEngine;

public class AssetBundleManager : MonoBehaviour {

    static WWW objSERVER;
    static string pathURL;
    static string pathLOCAL;

    //IEnumerator allows yield so the information is not accessed
    //before it finished downloading
    IEnumerator Start()
    {
        pathURL = "www.mywebsite.net/file"; //location of the file on the server
        pathLOCAL = Application.dataPath + "/assetbundles/" + "test" + ".unity3d"; //location of the file on the device

        objSERVER = new WWW(pathURL);

        // Wait for download to finish
        yield return objSERVER;

        // Save it to disk
        SaveDownloadedAsset(objSERVER);
        Debug.Log("------------------- HERE THE PATH -------------------" + "\n" + Application.dataPath);
        AssetBundle objLOCAL = AssetBundle.LoadFromFile(pathLOCAL);
        yield return objLOCAL;

      
    }

    public void SaveDownloadedAsset(WWW objSERVER)
    {
        // Create the directory if it doesn't already exist
        if (!Directory.Exists(Application.dataPath + "/assetbundles/"))
        {
            Directory.CreateDirectory(Application.dataPath + "/assetbundles/");
        }

        // Initialize the byte string
        byte[] bytes = objSERVER.bytes;

        // Creates a new file, writes the specified byte array to the file, and then closes the file. 
        // If the target file already exists, it is overwritten.
        File.WriteAllBytes(pathLOCAL, bytes);
    }

    [MenuItem("Assets/Build AssetBundles")]
    static void BuildAllAssetBundles()
    {
        string assetBundleDirectory = "Assets/AssetBundles";
        if (!Directory.Exists(assetBundleDirectory))
        {
            Directory.CreateDirectory(assetBundleDirectory);
        }
        BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
    }
}

