﻿/*using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class mainMenu : MonoBehaviour
{

    public string gameVersion = "0.1 Alpha";
    public string roomName = "Room 01";
    public Texture2D profilePic;


    public loadOutUIBlock[] loadOutUI;


    public rankItem[] ranks;

    [HideInInspector]
    public rankItem curRank;

    //int curRankIndex = 0    ;

    public GUISkin skin;

    public float scoreProg = 0f;

    public mapItem[] maps;


    public GameObject[] classPrefabs;

    public wepItem[] guns;

    public cosmeticItem[] cosmetics;

    public bool isLoading = false;



    public GameObject[] botAi;

    Vector2 scroll = Vector2.zero;
    Vector2 mapScroll = Vector2.zero;

    [HideInInspector]
    public string playerName = "";

    public Texture loading;

    public AudioClip buyItem;
    public AudioClip applySpray;
    public AudioClip equipWep;
    public AudioClip cancel;
    public AudioClip blip;

    public AudioClip menuMusic;
    public AudioClip loadingMusic;
    public AudioClip gameMusic;

    //bool changeLoadout = false;

    mapItem mapToLoad;

    public byte[] maxPlayerOptions;
    public int[] maxBotOptions;
    public int[] maxZombieOptions;

    public string profilePicURL = "http://i.imgur.com/0yFl6qI.png";

    public bool showServers = false;

    public AudioSource aS;

    bool createRoom = false;

    public bool legacyGUI = false;

    public Vector2[] screenRes;

    public Text nameText;
    public Text rankAbbreviationText;
    public Text rankText;
    public Text rankProgressText;
    public Text xpText;
    public Text creditText;
    public Text killDeathText;
    public Text headshotRoundsWonText;
    public Text resolutionLabel;
    public Image rankImage;

    public InputField userNameField;
    public InputField profilePicField;
    public Image profilePictureUI;
    public Slider volumeSlider;
    public Slider mouseSlider;
    public Toggle musicToggle;
    public Toggle fullScreenToggle;

    public bool fullScreen = false;

    //GameObject curClass;

    void Awake()
    {
        if (!PhotonNetwork.connected && !PhotonNetwork.connecting)
        {
            PhotonNetwork.ConnectUsingSettings(gameVersion);
        }

        if (!PlayerPrefs.HasKey("name"))
        {
            playerName = "Player " + Random.Range(0, 999);
            PlayerPrefs.SetString("name", playerName);
        }
        else
        {
            playerName = PlayerPrefs.GetString("name");
        }

        if (PlayerPrefs.HasKey("vol"))
        {
            volumeSlider.value = PlayerPrefs.GetFloat("vol");
        }

        if (PlayerPrefs.HasKey("mouse"))
        {
            mouseSlider.value = PlayerPrefs.GetFloat("mouse");
        }
        AudioListener.volume = volumeSlider.value = PlayerPrefs.GetFloat("vol");

        if (aS == null)
        {
            aS = GetComponent<AudioSource>();
        }

        updateRank();

        if (PlayerPrefs.HasKey("pfp") && PlayerPrefs.GetString("pfp") != "")
        {
            profilePicURL = PlayerPrefs.GetString("pfp");
            StartCoroutine(updatePFP());
        }

        mapToLoad = maps[0];

        profilePicField.text = profilePicURL;
        if (!PlayerPrefs.HasKey("class"))
        {
            setDefault();
        }

        PlayerPrefs.SetString("class", classPrefabs[0].name);

        //curClass = classPrefabs[0];

        updateSettingsUI();

        if(PlayerPrefs.GetInt("music") == 0)
        {
            musicToggle.isOn = true;
        } else
        {
            musicToggle.isOn = false;
        }

        if (PlayerPrefs.GetInt("fullscreen") == 0)
        {
            fullScreenToggle.isOn = false;
        }
        else
        {
            fullScreenToggle.isOn = true;
        }

        if (PlayerPrefs.HasKey("resolution"))
        {
            curRes = screenRes[PlayerPrefs.GetInt("resolution")];           
        } else {
            curRes = screenRes[currentResolutionIndex];
        }
        resolutionLabel.text = curRes.x + " x " + curRes.y;
        applySettings();

    }

    void OnJoinedMaster()
    {
        PhotonNetwork.JoinLobby();
    }

    void OnJoinedLobby()
    {
        PlayerPrefs.SetInt("bots", 0);
        PlayerPrefs.SetInt("zombies", 0);

        if (aS != null && menuMusic != null)
        {
            aS.Stop();
            if (PlayerPrefs.GetInt("music") == 0)
            {
                aS.PlayOneShot(menuMusic);
            }
        }

        
        updateUIText();
    }

    mapItem curMap;

    public void hideServers()
    {
        showServers = false;
    }

    public void updateUIText()
    {
        if (nameText != null)
        {
            if (PlayerPrefs.HasKey("name"))
            {
                nameText.text = PlayerPrefs.GetString("name");
            }
            else
            {
                nameText.text = playerName;
            }
        }
        updateRank();
        if (curRank != null)
        {
            if (rankText != null)
            {
                rankText.text = curRank.rankName;
            }

            if(rankAbbreviationText != null)
            {
                rankAbbreviationText.text = curRank.abbreviation;
            }
            if(rankImage != null)
            {
                rankImage.sprite = curRank.iconSprite;
            }

            if(rankProgressText != null)
            {
                rankProgressText.text = PlayerPrefs.GetInt("xp") + " / " + curRank.xp;
            }
            
            foreach(loadOutUIBlock lo in loadOutUI)
            {
                lo.UpdateUI();
            }
        }

        if(xpText != null)
        {
            xpText.text = "XP: " + PlayerPrefs.GetInt("xp");
        }

        if(creditText != null)
        {
            creditText.text = "Credits: $" + PlayerPrefs.GetInt("cash");
        }

        if(killDeathText != null)
        {
            killDeathText.text = "Kills: " + PlayerPrefs.GetInt("kills") + " / " + "Deaths: " + PlayerPrefs.GetInt("deaths");
        }

        if(headshotRoundsWonText != null)
        {
            headshotRoundsWonText.text = "Headshots: " + PlayerPrefs.GetInt("headshots") + "\nRounds Won: " + PlayerPrefs.GetInt("won");
        }

        
    }

  
  


    wepItem curGun;
    void OnGUI()
    {

        GUI.skin = skin;
        GUIStyle title = new GUIStyle("Label");
        GUIStyle subTitle = new GUIStyle("Label");
        title.fontSize = 36;
        title.alignment = TextAnchor.LowerLeft;
        subTitle.fontSize = 24;
        subTitle.alignment = TextAnchor.LowerLeft;



        if (PhotonNetwork.connecting || isLoading)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), loading, ScaleMode.StretchToFill);
            if (!PhotonNetwork.connected)
            {
                GUI.Label(new Rect(0, 0, 200, 60), PhotonNetwork.connectionState.ToString());
            }
            GUI.Label(new Rect(0, Screen.height - 60, 200, 60), loadPerc + "%");
        }


        if (showServers)
        {
            GUILayout.BeginArea(new Rect(Screen.width / 2 - 250, Screen.height / 2 - 175, 500, 500));
            GUILayout.BeginVertical("Window");
            if (!createRoom)
            {
                GUILayout.BeginHorizontal();
                GUILayout.BeginVertical();
                GUILayout.BeginHorizontal();
                GUILayout.Label("Server Browser:", subTitle);
                if (GUILayout.Button("X", GUILayout.Width(45)))
                {
                    showServerBrowser();
                }
                GUILayout.EndHorizontal();
                scroll = GUILayout.BeginScrollView(scroll);
                if (PhotonNetwork.GetRoomList().Length > 0)
                {
                    foreach (RoomInfo room in PhotonNetwork.GetRoomList())
                    {
                        GUILayout.BeginHorizontal("Box");
                        GUILayout.Label(room.Name + " | " + room.PlayerCount + "/" + room.MaxPlayers.ToString());
                        if (GUILayout.Button("Join", GUILayout.Width(100)))
                        {
                            PhotonNetwork.playerName = "[" + curRank.abbreviation + "] " + playerName;
                            joinRoom(room.Name);
                        }
                        GUILayout.EndHorizontal();
                    }
                }
                else
                {
                    GUILayout.Label("No Rooms Found...");
                }
                GUILayout.EndScrollView();

                GUILayout.BeginHorizontal();
                roomName = GUILayout.TextField(roomName);
                if (GUILayout.Button("Create", GUILayout.Width(100)))
                {
                    createRoom = true;
                }
                GUILayout.EndHorizontal();
                GUILayout.EndVertical();
                GUILayout.EndHorizontal();


            }
            else
            {
                GUILayout.BeginVertical();
                RoomOptions ro = new RoomOptions();
                GUILayout.Label("Creating " + roomName);
                mapScroll = GUILayout.BeginScrollView(mapScroll, GUILayout.Height(275));
                GUILayout.BeginHorizontal();
                foreach (mapItem map in maps)
                {
                    GUILayout.BeginVertical("Window");
                    GUILayout.Label(map.levelToLoad);
                    GUILayout.Label(map.icon, GUILayout.Width(300), GUILayout.Height(150));
                    GUILayout.Label(map.size + " | Kill Limit: " + map.scoreLimit);
                    if (GUILayout.Button("Choose"))
                    {
                        mapToLoad = map;
                    }
                    GUILayout.EndVertical();
                }
                GUILayout.EndHorizontal();
                GUILayout.EndScrollView();
                //max player select
                GUILayout.BeginHorizontal();
                GUILayout.Label("Max Players:", GUILayout.Width(150));

                foreach (byte mxpO in maxPlayerOptions)
                {
                    if (GUILayout.Button(mxpO.ToString()))
                    {
                        ro.MaxPlayers = mxpO;
                        Debug.Log(ro.MaxPlayers.ToString());
                    }
                }
                GUILayout.EndHorizontal();

                //max bot select
                GUILayout.BeginHorizontal();
                GUILayout.Label("Bot Count:", GUILayout.Width(150));
                foreach (int opt in maxBotOptions)
                {
                    if (GUILayout.Button(opt.ToString()))
                    {
                        PlayerPrefs.SetInt("bots", opt);
                    }
                }
                GUILayout.EndHorizontal();

                //max zombie select
                GUILayout.BeginHorizontal();
                GUILayout.Label("Zombie Count:", GUILayout.Width(150));
                foreach (int opt in maxZombieOptions)
                {
                    if (GUILayout.Button(opt.ToString()))
                    {
                        PlayerPrefs.SetInt("zombies", opt);
                    }
                }
                GUILayout.EndHorizontal();


                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Start", GUILayout.Height(35)))
                {
                    if (ro.MaxPlayers == 0)
                    {
                        ro.MaxPlayers = 8;
                    }
                    PhotonNetwork.JoinOrCreateRoom(roomName + "|" + mapToLoad.levelToLoad, ro, null);
                    PhotonNetwork.playerName = "[" + curRank.abbreviation + "] " + playerName;
                    StartCoroutine(LoadSceneAsynchronously(mapToLoad.levelToLoad));
                }
                if (GUILayout.Button("Cancel", GUILayout.Height(35)))
                {
                    createRoom = false;
                }
                GUILayout.EndHorizontal();
                GUILayout.EndVertical();
            }
            GUILayout.EndVertical();
            GUILayout.EndArea();
        }


    }

  

    float loadPerc = 0;

    IEnumerator LoadSceneAsynchronously(string scene)
    {
        AsyncOperation op = SceneManager.LoadSceneAsync(scene);

        while (!op.isDone)
        {
            isLoading = true;
            float progress = Mathf.Clamp01(op.progress / .9f);
            loadPerc = progress * 100f;

            yield return null;
        }
        while (op.isDone)
        {
            if (isLoading)
            {
                isLoading = false;
            }
        }
    }
    


    public void quit()
    {
        PhotonNetwork.Disconnect();
        Application.Quit();
    }

    public void quickPlay()
    {
        if (PhotonNetwork.connectedAndReady)
        {
            if (PhotonNetwork.inRoom)
            {
                PhotonNetwork.player.SetScore(0);
            }
            if (PhotonNetwork.GetRoomList().Length > 0)
            {
                RoomInfo[] theRooms = PhotonNetwork.GetRoomList();
                joinRoom(theRooms[Random.Range(0, theRooms.Length)].Name);
            }
            else
            {
                if (PhotonNetwork.insideLobby || PhotonNetwork.inRoom)
                {
                    PlayerPrefs.SetInt("bots", Random.Range(0, 8));
                    PlayerPrefs.SetInt("zombies", Random.Range(0, 8));
                    RoomOptions ro = new RoomOptions();
                    ro.MaxPlayers = 8;
                    PhotonNetwork.playerName = "[" + curRank.abbreviation + "] " + playerName;
                    string nm1 = "Room " + Random.Range(0, 999) + "|" + maps[Random.Range(0, maps.Length)].levelToLoad;
                    if (PhotonNetwork.connectedAndReady)
                    {
                        PhotonNetwork.JoinOrCreateRoom(nm1, ro, null);
                    }
                    char[] splitChar = new char[1];
                    splitChar[0] = char.Parse("|");
                    string[] roomName = nm1.Split(splitChar[0]);
                    Debug.Log(roomName[1]);
                    if (PhotonNetwork.inRoom)
                    {
                        PhotonNetwork.player.SetScore(0);
                    }
                    StartCoroutine(LoadSceneAsynchronously(roomName[1]));
                }

            }
        }
    }

    public void joinRoom(string nm)
    {
        PhotonNetwork.playerName = "[" + curRank.abbreviation + "] " + playerName;
        char[] splitChar = new char[1];
        splitChar[0] = char.Parse("|");
        string[] roomName = nm.Split(splitChar[0]);
        Debug.Log(roomName[1]);
        string map = roomName[1];
        Debug.Log(map);
        if (PhotonNetwork.inRoom)
        {
            PhotonNetwork.player.SetScore(0);
        }
        foreach (mapItem mapI in maps)
        {
            if (mapI.levelToLoad == map)
            {
                mapToLoad = mapI;
                Debug.Log(mapI.levelToLoad + " / " + mapI.levelToLoad);
            }
        }
        StartCoroutine(LoadSceneAsynchronously(mapToLoad.levelToLoad));
        PhotonNetwork.JoinRoom(nm);
    }

    //Vector2 scroll2 = Vector2.zero;


    void FixedUpdate()
    {

        if (Input.GetKeyUp(KeyCode.F5))
        {
            PlayerPrefs.SetInt("cash", PlayerPrefs.GetInt("cash") + 1000);
        }

       

            if (PhotonNetwork.connecting)
            {
                if (aS != null && loadingMusic != null)
                {
                    if (PlayerPrefs.GetInt("music") == 0)
                    {
                        if (!aS.isPlaying)
                        {
                            aS.PlayOneShot(loadingMusic);
                        }
                    }
                }
            }

       

       
                
        if (curRank == null)
        {
            updateRank();
        }

    }

    void OnLeftRoom()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        updateRank();
        StartCoroutine(LoadSceneAsynchronously(mainMenuName));
    }

    IEnumerator updatePFP()
    {

        WWW www = new WWW(profilePicURL);
        yield return www;

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Failed Load / " + www.error);
        }
        else
        {
            Debug.Log("Load Successful");
            profilePic = www.texture;
            profilePictureUI.sprite = Sprite.Create(profilePic, new Rect(0.0f, 0.0f, profilePic.width, profilePic.height), new Vector2(0.5f, 0.5f), 100.0f);
            PlayerPrefs.SetString("pfp", profilePicURL);
        }

    }

   

    public void showServerBrowser()
    {
        if (!showServers)
        {
            showServers = true;
        } else
        {
            showServers = false;
        }
    }


    public void setDefault()
    {
        foreach (GameObject clss in classPrefabs)
        {
            characterControls cc = clss.GetComponent<characterControls>();
            wepManager wm = cc.fpsCam.GetComponentInChildren<wepManager>();
            if (wm != null)
            {
                PlayerPrefs.SetString(clss.name + "wep", wm.primaries[0].name);
            }
        }
    }

    public void updateRank()
    {
        foreach (rankItem rank in ranks)
        {
            if (PlayerPrefs.GetInt("xp") >= rank.xp)
            {
                curRank = rank;
                //curRankIndex = System.Array.IndexOf(ranks, curRank);
                PhotonNetwork.playerName = "[" + curRank.abbreviation + "] " + playerName;
            }
        }
    }

   
    




    public void hideAllAttach()
    {
        wepUIBlock[] blocks = GameObject.FindObjectsOfType<wepUIBlock>();

        foreach(wepUIBlock block in blocks)
        {
            block.showAttachments = false;
        }
    }

    public void updateSettingsUI()
    {
        userNameField.text = playerName;
        profilePicField.text = profilePicURL;
    }

    public void setName()
    {
        playerName = userNameField.text;
        PlayerPrefs.SetString("name", playerName);
        updateUIText();
    }

    public void setPFPUi()
    {
        profilePicURL = profilePicField.text;
        StartCoroutine(updatePFP());
    }

    Vector2 curRes = new Vector2(1280, 720);
    public int currentResolutionIndex = 4;

    public void raiseReso()
    {
        if (currentResolutionIndex < screenRes.Length - 1)
        {
            currentResolutionIndex += 1;
            curRes = screenRes[currentResolutionIndex];
            resolutionLabel.text = curRes.x + " x " + curRes.y;
        }
    }

    public void lowerReso()
    {
        if (currentResolutionIndex > 0)
        {
            currentResolutionIndex -= 1;
            curRes = screenRes[currentResolutionIndex];
            resolutionLabel.text = curRes.x + " x " + curRes.y;
        }
    }

    int qualityLevel = 3;

    public void changeQuality(int lvl)
    {
        qualityLevel = lvl;
    }

    public void boolSound(Toggle tog)
    {
        if (tog.isOn)
        {
            aS.PlayOneShot(blip);
        } else
        {
            aS.PlayOneShot(cancel);
        }
    }

    public void applySettings()
    {
        QualitySettings.SetQualityLevel(qualityLevel);
        Screen.SetResolution(int.Parse(curRes.x.ToString()), int.Parse(curRes.y.ToString()), fullScreenToggle.isOn);
        PlayerPrefs.SetFloat("mouse", mouseSlider.value);
        PlayerPrefs.SetFloat("vol", volumeSlider.value);
        AudioListener.volume = volumeSlider.value;

        if (musicToggle.isOn)
        {
            if (PlayerPrefs.GetInt("music") != 0)
            {
                PlayerPrefs.SetInt("music", 0);
                if (aS != null && menuMusic != null)
                {
                    aS.PlayOneShot(menuMusic);
                }
            }
        } else
        {
            PlayerPrefs.SetInt("music", 1);
            aS.Stop();
        }

        if (fullScreenToggle.isOn)
        {
            PlayerPrefs.SetInt("fullscreen", 1);
        } else
        {
            PlayerPrefs.SetInt("fullscreen", 0);
        }

        PlayerPrefs.SetInt("resolution", currentResolutionIndex);


    }

    public void playCancel()
    {
        aS.PlayOneShot(cancel);
    }

    public void playBlip()
    {
        aS.PlayOneShot(blip);
    }

    public void playEquip()
    {
        aS.PlayOneShot(equipWep);
    }

    public void playBuy()
    {
        aS.PlayOneShot(buyItem);
    }

    public void playSpray()
    {
        aS.PlayOneShot(applySpray);
    }

    public void resetStats()
    {
        PlayerPrefs.DeleteAll();
        PhotonNetwork.Disconnect();
        StartCoroutine(LoadSceneAsynchronously(mainMenuName));
    }

    public string mainMenuName = "Main Menu";
}
*/