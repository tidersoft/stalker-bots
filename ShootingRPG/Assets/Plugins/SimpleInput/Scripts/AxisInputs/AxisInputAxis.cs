﻿using UnityEngine;

namespace SimpleInputNamespace
{
	public class AxisInputAxis : MonoBehaviour
	{
		[SerializeField]
		private string BindAxis;
        public float multiplay = 1.0f;

		public SimpleInput.AxisInput axis = new SimpleInput.AxisInput();
		
		private void OnEnable()
		{
			axis.StartTracking();
			SimpleInput.OnUpdate += OnUpdate;
		}

		private void OnDisable()
		{
			axis.StopTracking();
			SimpleInput.OnUpdate -= OnUpdate;
		}

		private void OnUpdate()
		{
			axis.value = Input.GetAxis(BindAxis)*multiplay;
		}
	}
}