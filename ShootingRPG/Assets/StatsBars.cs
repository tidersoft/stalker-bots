﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsBars : MonoBehaviour {
    public GUIStyle statusFont;


    public Texture2D hpBar;
    public Texture2D mpBar;
    public Texture2D shieldBar;
    public Texture2D expBar;
    public Texture2D backGroundBar;

    public float hpTimer;
    public float mpTimer;
    public float shTimer;
    private float elapse = 0.75f;


    status stat;
    private void Awake()
    {
        if (stat == null)
            stat = GetComponent<status>();
    }
    private void OnGUI()
    {
        float middle = (Screen.width / 2) - 200;
        if (hpTimer + elapse > Time.time)
        {
            int hp = (int)(stat.health * 100 / stat.maxHealth * 4.0f);
            //    GUI.Label(new Rect(middle, Screen.height - 100, 200, 50), "HP : " + stat.health.ToString(), statusFont);
            GUI.DrawTexture(new Rect(middle, Screen.height - 100, 400, 10), backGroundBar);
            GUI.DrawTexture(new Rect(middle, Screen.height - 100, hp, 10), hpBar);
        }
        if (mpTimer + elapse > Time.time)
        {
            int mp = (int)(stat.mana * 100 / stat.maxMana * 4.0f);
            // GUI.Label(new Rect(middle, Screen.height - 60, 200, 50), "MP : " + stat.mana.ToString(), statusFont);
            GUI.DrawTexture(new Rect(middle, Screen.height - 90, 400, 10), backGroundBar);
            GUI.DrawTexture(new Rect(middle, Screen.height - 90, mp, 10), mpBar);
        }

        if (stat.maxShieldPlus > 0)
        {
            if (shTimer + elapse > Time.time)
            {
                int sh = (int)(stat.shield * 100 / stat.maxShieldPlus * 4.0f);
                //     GUI.Label(new Rect(middle, Screen.height - 140, 200, 50), "Shield : " + stat.shield.ToString(), statusFont);
                GUI.DrawTexture(new Rect(middle, Screen.height - 120, 400, 10), backGroundBar);
                GUI.DrawTexture(new Rect(middle, Screen.height - 120, sh, 10), shieldBar);
            }
        }

        int xp = (int)(stat.exp * 100 / stat.maxExp * 1.5f);
        GUI.Label(new Rect(50, 40, 200, 50), "Level : " + stat.level.ToString(), statusFont);
        GUI.DrawTexture(new Rect(50, 60, 150, 10), backGroundBar);
        GUI.DrawTexture(new Rect(50, 60, xp, 10), expBar);

    }
}
