﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class getSkill : MonoBehaviour
{
    public int index;
    // Start is called before the first frame update
    void Awake()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        if (GameMenager.Instance.getLocalPlayer() != null)
        {
            GunTrigger.SkilAtk id = GameMenager.Instance.getLocalPlayer().GetComponent<GunTrigger>().skill[index];
            if (id != null)
            {
                if (id.icon != null)
                {
                    GetComponent<Image>().sprite = Sprite.Create(id.icon, new Rect(0, 0, id.icon.width, id.icon.height), new Vector2(0.5f, 0.5f));
                }
                else
                {
                    gameObject.SetActive(false);
                }
            }
            else
            {
                gameObject.SetActive(false);
            }

        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
