﻿using Photon.Pun.Demo.PunBasics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using GooglePlayGames.BasicApi;

using GooglePlayGames;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    #region Private Serializable Fields

    [Tooltip("The Ui Panel to let the user enter name, connect and play")]
    [SerializeField]
    public GameObject controlPanel;

    [Tooltip("The Ui Text to inform the user about the connection progress")]
    [SerializeField]
    private Text feedbackText;

    [Tooltip("The maximum number of players per room")]
    [SerializeField]
    private byte maxPlayersPerRoom = 4;

    [Tooltip("The UI Loader Anime")]
    [SerializeField]
    private LoaderAnime loaderAnime;
    public Transform modelPosition;
    public GameObject showingModel;

    private System.Action<bool> mAuthCallback;


    private bool mAuthOnStart = true;
    private bool mSigningIn = false;


    private int page = 0;
    //private int presave = 0;

    private int saveSlot = 0;
    private string charName = "Serin";
 
    int charSelect = 0;
    int maxChar = 2;
    public bool show = false;
    #endregion

    #region Private Fields
    /// <summary>
    /// Keep track of the current process. Since connection is asynchronous and is based on several callbacks from Photon, 
    /// we need to keep track of this to properly adjust the behavior when we receive call back by Photon.
    /// Typically this is used for the OnConnectedToMaster() callback.
    /// </summary>
    bool isConnecting;

    CoroutineScheduler sh; 

    /// <summary>
    /// This client's version number. Users are separated from each other by gameVersion (which allows you to make breaking changes).
    /// </summary>
    string gameVersion = "1";

    #endregion

    #region MonoBehaviour CallBacks

    /// <summary>
    /// MonoBehaviour method called on GameObject by Unity during early initialization phase.
    /// </summary>
    void Awake()
    {
        if (loaderAnime == null)
        {
            Debug.LogError("<Color=Red><b>Missing</b></Color> loaderAnime Reference.", this);
        }
        mAuthCallback = (bool success) =>
        {

            Debug.Log("In Auth callback, success = " + success);

            mSigningIn = false;
            if (success)
            {
                Debug.Log("Auth succes");
              //  CharackterSelect();
              //  GameMenager.Instance.ShowSelectUI();
                 //    NavigationUtil.ShowMainMenu();
            }
            else
            {
                Debug.Log("Auth failed!!");
            }
        };

        // enable debug logs (note: we do this because this is a sample;
        // on your production app, you probably don't want this turned 
        // on by default, as it will fill the user's logs with debug info).
        var config = new PlayGamesClientConfiguration.Builder()
        .WithInvitationDelegate(InvitationManager.Instance.OnInvitationReceived)
        // requests the email address of the player be available.
        // Will bring up a prompt for consent.
       .Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
        Authorize(false);
        //    PlayGamesPlatform.Instance.RegisterInvitationDelegate(InvitationManager.Instance.OnInvitationReceived);
    }

    #endregion


    #region Public Methods

    /// <summary>
    /// Start the connection process. 
    /// - If already connected, we attempt joining a random room
    /// - if not yet connected, Connect this application instance to Photon Cloud Network
    /// </summary>
    public void Connect()
    {
        // we want to make sure the log is clear everytime we connect, we might have several failed attempted if connection failed.
        feedbackText.text = "";

        show = false;

        Destroy(showingModel);

        // keep track of the will to join a room, because when we come back from the game we will get a callback that we are connected, so we need to know what to do then
        isConnecting = true;

        // hide the Play button for visual consistency
       // controlPanel.SetActive(false);

        // start the loader animation for visual effect.
        if (loaderAnime != null)
        {
            loaderAnime.StartLoaderAnimation();
        }

        SceneManager.LoadScene("Base");
    //   Application.LoadLevel("Base");


    }

    void MenuSelect(int pa) {
        page = pa;
        if (page != 0)
        {
            show = true;
            if(page == 5)
            SwitchModel();
            controlPanel.SetActive(false);
        }
        else {
            show = false;
            Destroy(showingModel);
            controlPanel.SetActive(true);
        }
    }

    void Authorize(bool silent)
    {
        if (!mSigningIn)
        {
            Debug.Log("Starting sign-in...");
            Social.localUser.Authenticate(mAuthCallback);
           // CharackterSelect();
        }
        else
        {
            Debug.Log("Already started signing in");
        }
    }

    public void OnGUI()
    {
        float scala = (float)Screen.height/(float)600 * GameControler.instance.getRender();
        //   GUI.Label(new Rect(10, 10, 100, 50), Screen.height + " " + scala.ToString());
        //   GUI.Label(new Rect(10, 60, 100, 50), DisplayMetricsAndroid.DensityDPI.ToString());
        if (show)
        {

            if (page == 2)
            {
                //Create Character and Select Save Slot
                GUI.Box(new Rect(Screen.width / 2 - 250, 170, 500, 400), "Select your slot");
                if (GUI.Button(new Rect(Screen.width / 2 + 185, 175, 30, 30), "X"))
                {
                    MenuSelect(0);
                }
                //---------------Slot 1 [ID 0]------------------
                if (PlayerPrefs.GetInt("PreviousSave0") > 0)
                {
                    if (GUI.Button(new Rect(Screen.width / 2 - 200, 205, 400, 100), PlayerPrefs.GetString("Name0") + "\n" + "Level " + PlayerPrefs.GetInt("PlayerLevel0").ToString()))
                    {
                        //When Slot 1 already used
                        saveSlot = 0;
                       MenuSelect(4);
                    }
                }
                else
                {
                    if (GUI.Button(new Rect(Screen.width / 2 - 200, 205, 400, 100), "- Empty Slot -"))
                    {
                        //Empty Slot 1
                        saveSlot = 0;
                        MenuSelect(5);
                        SwitchModel();
                    }
                }
                //---------------Slot 2 [ID 1]------------------
                if (PlayerPrefs.GetInt("PreviousSave1") > 0)
                {
                    if (GUI.Button(new Rect(Screen.width / 2 - 200, 315, 400, 100), PlayerPrefs.GetString("Name1") + "\n" + "Level " + PlayerPrefs.GetInt("PlayerLevel1").ToString()))
                    {
                        //When Slot 2 already used
                        saveSlot = 1;
                        MenuSelect(4);
                    }
                }
                else
                {
                    if (GUI.Button(new Rect(Screen.width / 2 - 200, 315, 400, 100), "- Empty Slot -"))
                    {
                        //Empty Slot 2
                        saveSlot = 1;
                       MenuSelect(5);
                        SwitchModel();
                    }
                }
                //---------------Slot 3 [ID 2]------------------
                if (PlayerPrefs.GetInt("PreviousSave2") > 0)
                {
                    if (GUI.Button(new Rect(Screen.width / 2 - 200, 425, 400, 100), PlayerPrefs.GetString("Name2") + "\n" + "Level " + PlayerPrefs.GetInt("PlayerLevel2").ToString()))
                    {
                        //When Slot 3 already used
                        saveSlot = 2;
                        MenuSelect(4);
                    }
                }
                else
                {
                    if (GUI.Button(new Rect(Screen.width / 2 - 200, 425, 400, 100), "- Empty Slot -"))
                    {
                        //Empty Slot 3
                        saveSlot = 2;
                        MenuSelect(5);
                        SwitchModel();
                    }
                }
            }

            if (page == 3)
            {
                //Load Save Slot
                GUI.Box(new Rect(Screen.width / 2 - 250, 170, 500, 400), "Menu");
                if (GUI.Button(new Rect(Screen.width / 2 + 185, 175, 30, 30), "X"))
                {
                   MenuSelect(0);
                }
                //---------------Slot 1 [ID 0]------------------
                if (PlayerPrefs.GetInt("PreviousSave0") > 0)
                {
                    if (GUI.Button(new Rect(Screen.width / 2 - 200, 205, 400, 100), PlayerPrefs.GetString("Name0") + "\n" + "Level " + PlayerPrefs.GetInt("PlayerLevel0").ToString()))
                    {
                        //When Slot 1 already used
                        saveSlot = 0;
                        LoadData();
                    }
                }
                else
                {
                    if (GUI.Button(new Rect(Screen.width / 2 - 200, 205, 400, 100), "- Empty Slot -"))
                    {
                        //Empty Slot 1
                    }
                }
                //---------------Slot 2 [ID 1]------------------
                if (PlayerPrefs.GetInt("PreviousSave1") > 0)
                {
                    if (GUI.Button(new Rect(Screen.width / 2 - 200, 315, 400, 100), PlayerPrefs.GetString("Name1") + "\n" + "Level " + PlayerPrefs.GetInt("PlayerLevel1").ToString()))
                    {
                        //When Slot 2 already used
                        saveSlot = 1;
                        LoadData();
                    }
                }
                else
                {
                    if (GUI.Button(new Rect(Screen.width / 2 - 200, 315, 400, 100), "- Empty Slot -"))
                    {
                        //Empty Slot 2
                    }
                }
                //---------------Slot 3 [ID 2]------------------
                if (PlayerPrefs.GetInt("PreviousSave2") > 0)
                {
                    if (GUI.Button(new Rect(Screen.width / 2 - 200, 425, 400, 100), PlayerPrefs.GetString("Name2") + "\n" + "Level " + PlayerPrefs.GetInt("PlayerLevel2").ToString()))
                    {
                        //When Slot 3 already used
                       saveSlot = 2;
                        LoadData();
                    }
                }
                else
                {
                    if (GUI.Button(new Rect(Screen.width / 2 - 200, 425, 400, 100), "- Empty Slot -"))
                    {
                        //Empty Slot 3
                    }
                }

            }

            if (page == 4)
            {
                //Overwrite Confirm
                GUI.Box(new Rect(Screen.width / 2 - 150, 200, 300, 180), "Are you sure to overwrite this slot?");
                if (GUI.Button(new Rect(Screen.width / 2 - 110, 260, 100, 40), "Yes"))
                {
                    MenuSelect(5);
                    SwitchModel();
                }
                if (GUI.Button(new Rect(Screen.width / 2 + 20, 260, 100, 40), "No"))
                {
                    MenuSelect(0);
                }
            }
            if (page == 5)
            {
                GUI.Label(new Rect(100 * scala, 200 * scala, 300 * scala, 40 * scala), CharacterData.instance.player[charSelect].description.textLine1);
                GUI.Label(new Rect(100 * scala, 230 * scala, 300 * scala, 40 * scala), CharacterData.instance.player[charSelect].description.textLine2);
                GUI.Label(new Rect(100 * scala, 260 * scala, 300 * scala, 40 * scala), CharacterData.instance.player[charSelect].description.textLine3);
                GUI.Label(new Rect(100 * scala, 290 * scala, 300 * scala, 40 * scala), CharacterData.instance.player[charSelect].description.textLine4);
                GUI.Box(new Rect(80 * scala, 100 * scala, 300 * scala, 360 * scala), "Enter Your Name");

                charName = GUI.TextField(new Rect(120 * scala, 140 * scala, 220 * scala, 40 * scala), charName, 25);


                //charName = GUI.TextField(new Rect(120, 140, 220, 40), charName, 25);
                if (GUI.Button(new Rect(180 * scala, 400 * scala, 100 * scala, 40 * scala), "Done"))
                {
                     NewGame();

                    if (charSelect == 0)
                    {
                        ItemData.instance.infloud = new Infrontery[1];
                        ItemData.instance.infloud[0] = new Infrontery();
                        ItemData.instance.weapons[1].have = true;
                        ItemData.instance.weapons[2].have = true;
                        ItemData.instance.weapons[3].have = true;
                        // ItemData.instance.weapons[7].have = true;
                        ItemData.instance.equipment[1].have = true;
                        SkillData.instance.skill[1].have = true;
                        SkillData.instance.skill[3].have = true;

                        ItemData.instance.infloud[0].name = "Vanguard";
                        ItemData.instance.infloud[0].primeryWeapon = ItemData.instance.weapons[1];
                        ItemData.instance.infloud[0].seconderyWeapon = ItemData.instance.weapons[3];
                        ItemData.instance.infloud[0].meleWeapon = ItemData.instance.weapons[2];
                        ItemData.instance.infloud[0].armor = ItemData.instance.equipment[1];
                        ItemData.instance.infloud[0].charakter = CharacterData.instance.player[charSelect].playerPrefab;
                        ItemData.instance.infloud[0].charId = charSelect;
                        /*   ItemData.instance.infloud[1] = new Infrontery();

                           ItemData.instance.infloud[1].name = "Vanguard assalt";
                           ItemData.instance.infloud[1].primeryWeapon = ItemData.instance.weapons[7];
                           ItemData.instance.infloud[1].seconderyWeapon = ItemData.instance.weapons[3];
                           ItemData.instance.infloud[1].meleWeapon = ItemData.instance.weapons[2];
                           ItemData.instance.infloud[1].armor = ItemData.instance.equipment[1];
                           ItemData.instance.infloud[1].charakter = CharacterData.instance.player[charSelect].playerPrefab;
                           ItemData.instance.infloud[1].charId = charSelect;
                           */
                    }
                    else if (charSelect == 1)
                    {


                        ItemData.instance.infloud = new Infrontery[3];
                        ItemData.instance.infloud[0] = new Infrontery();

                        ItemData.instance.weapons[1].have = true;
                        ItemData.instance.weapons[6].have = true;
                        ItemData.instance.weapons[3].have = true;
                        ItemData.instance.weapons[4].have = true;
                        ItemData.instance.weapons[5].have = true;
                        ItemData.instance.equipment[1].have = true;
                        SkillData.instance.skill[2].have = true;


                        ItemData.instance.infloud[0].name = "Anya";
                        ItemData.instance.infloud[0].primeryWeapon = ItemData.instance.weapons[1];
                        ItemData.instance.infloud[0].seconderyWeapon = ItemData.instance.weapons[3];
                        ItemData.instance.infloud[0].meleWeapon = ItemData.instance.weapons[6];
                        ItemData.instance.infloud[0].armor = ItemData.instance.equipment[1];
                        ItemData.instance.infloud[0].charakter = CharacterData.instance.player[charSelect].playerPrefab;
                        ItemData.instance.infloud[0].charId = charSelect;

                        ItemData.instance.infloud[1] = new Infrontery();

                        ItemData.instance.infloud[1].name = "Anya sniper";
                        ItemData.instance.infloud[1].primeryWeapon = ItemData.instance.weapons[4];
                        ItemData.instance.infloud[1].seconderyWeapon = ItemData.instance.weapons[3];
                        ItemData.instance.infloud[1].meleWeapon = ItemData.instance.weapons[6];
                        ItemData.instance.infloud[1].armor = ItemData.instance.equipment[1];
                        ItemData.instance.infloud[1].charakter = CharacterData.instance.player[charSelect].playerPrefab;
                        ItemData.instance.infloud[1].charId = charSelect;

                        ItemData.instance.infloud[2] = new Infrontery();

                        ItemData.instance.infloud[2].name = "Anya shotgun";
                        ItemData.instance.infloud[2].primeryWeapon = ItemData.instance.weapons[5];
                        ItemData.instance.infloud[2].seconderyWeapon = ItemData.instance.weapons[3];
                        ItemData.instance.infloud[2].meleWeapon = ItemData.instance.weapons[6];
                        ItemData.instance.infloud[2].armor = ItemData.instance.equipment[1];
                        ItemData.instance.infloud[2].charakter = CharacterData.instance.player[charSelect].playerPrefab;
                        ItemData.instance.infloud[2].charId = charSelect;

                    }



                    Connect();
                }

                //Previous Character
                if (GUI.Button(new Rect(Screen.width / 2 - (110 * scala), 380 * scala, 50 * scala, 150 * scala), "<"))
                {
                    if (charSelect > 0)
                    {
                        charSelect--;
                        SwitchModel();
                    }
                }
                //Next Character
                if (GUI.Button(new Rect(Screen.width / 2 + (190 * scala), 380 * scala, 50 * scala, 150 * scala), ">"))
                {
                    if (charSelect < maxChar - 1)
                    {
                        charSelect++;
                        SwitchModel();
                    }
                }
            }
        }
    }

    public void Login() {
        MenuSelect(2);
    }

    public void Load()
    {
        MenuSelect(3);
    }


    void NewGame()
    {
        PlayerPrefs.SetInt("SaveSlot", saveSlot);
        PlayerPrefs.SetString("Name" + saveSlot.ToString(), charName);
        PlayerPrefs.SetInt("PlayerID" + saveSlot.ToString(), charSelect);
      //  PlayerPrefs.SetInt("Loadgame", 0);
       // Instantiate(charData.player[charSelect].playerPrefab, transform.position, transform.rotation);

    }

    void LoadData()
    {
        PlayerPrefs.SetInt("SaveSlot", saveSlot);
        //if(presave == 10){
       // PlayerPrefs.SetInt("Loadgame", 10);
        SaveUtil.LoadGame(saveSlot);
        int playerId = PlayerPrefs.GetInt("PlayerID" + saveSlot.ToString());
        Connect();
      //  Instantiate(charData.player[playerId].playerPrefab, transform.position, transform.rotation);

        //}
    }

    public void Logout() {

        PlayGamesPlatform.Instance.SignOut();
    }

    public void Back() {
        //controlPanel.SetActive(false);

    }

    public void SwitchModel()
    {
        show = true;
        if (showingModel!=null)
        {
            Destroy(showingModel);
        }
        //Spawn Showing Model from Character Database
        if (CharacterData.instance.player[charSelect].characterSelectModel)
        {
            showingModel = Instantiate(CharacterData.instance.player[charSelect].characterSelectModel, modelPosition.position, modelPosition.rotation);
        }
    }

    /// <summary>
    /// Logs the feedback in the UI view for the player, as opposed to inside the Unity Editor for the developer.
    /// </summary>
    /// <param name="message">Message.</param>
    void LogFeedback(string message)
    {
        // we do not assume there is a feedbackText defined.
        if (feedbackText == null)
        {
            return;
        }

        // add new messages as a new line and at the bottom of the log.
        feedbackText.text += System.Environment.NewLine + message;
    }

    #endregion


  

}
