﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class Scale : MonoBehaviour {

    public float scalaX = 1.0f;
    public float scalaY=1.0f;
    public float X;
    public float Y;
     private Vector2 vec2;
   
	// Use this for initialization
	void Start () {
       
    }
	
	// Update is called once per frame
	void Update () {
        float scalX = scalaX * ((float)Screen.height / 768.0f);
        float scalY = scalaY * ((float)Screen.height / 768.0f);
        Vector3 vec = new Vector3(scalX, scalY, 1);
        gameObject.transform.localScale =vec;
     
        vec2 = new Vector2(X * scalX, Y * scalY);
        GetComponent<RectTransform>().anchoredPosition = vec2;
	}
}
