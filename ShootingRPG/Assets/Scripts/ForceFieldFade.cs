﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceFieldFade : MonoBehaviour {

  
        [SerializeField] private float fadePerSecond = 2.5f;

        private void Update()
        {
            var material = GetComponent<Renderer>().material;
            var color = material.GetColor("_TintColor");

        material.SetColor("_TintColor", new Color(color.r, color.g, color.b, color.a - (fadePerSecond * Time.deltaTime)));
        if (material.GetColor("_TintColor").a <= 0) {
            Destroy(gameObject);
        }
        }
    }

