﻿using UnityEngine;
using System.Collections;
using System.IO;

public class LoadFromFileExample : MonoBehaviour
{
    GameObject map;
    void Start()
    {
        map = GameObject.Find("Map");
        if (map == null)
        {
            var myLoadedAssetBundle = AssetBundle.LoadFromFile(Application.dataPath + "/AssetBundles/map1");
            if (myLoadedAssetBundle == null)
            {
                Debug.Log("Failed to load AssetBundle!");
                return;
            }

            var prefab = myLoadedAssetBundle.LoadAsset<GameObject>("map1");
            map = Instantiate(prefab);
            map.name = "Map";
           // myLoadedAssetBundle.Unload(false);
        }
    }
}