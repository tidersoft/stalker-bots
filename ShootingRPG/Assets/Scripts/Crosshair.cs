﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour {


    [SerializeField] Texture2D image;
    [SerializeField] int size;
  public Camera cam;
    public Weapon parent;

   

    private void OnGUI()
    {
       if (!GetComponentInParent<CharakterSync>().isMine() )
            return;
       if(parent!=null)
        if (!parent.selected)
            return;
        if (cam == null)
            return;
        
            RaycastHit hit;

        if (Physics.Raycast(transform.parent.transform.position, transform.parent.transform.forward, out hit, 100000.0f, LayerMask.GetMask("Default", "Wall")))
        {
            transform.position = hit.point;

        }
        else {
            transform.localPosition = new Vector3(0, 0, 25);
        }
       

            Vector3 screenPosition = cam.WorldToScreenPoint(transform.position);
            screenPosition.y = Screen.height - screenPosition.y;
            GUI.DrawTexture(new Rect(screenPosition.x - ((float)size / 2f), screenPosition.y - ((float)size / 2f), size, size), image);
         
      

    }
}
