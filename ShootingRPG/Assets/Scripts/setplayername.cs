﻿using GooglePlayGames;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class setplayername : MonoBehaviour {


    public TextMesh text;
    // Use this for initialization
    void Awake() {
       // activeText();
    }


    public void activeText() {
        if (GetComponent<CharakterSync>().isMine())
        {
            //  GetComponent<PhotonView>().RPC( "setText", RpcTarget.All, PhotonNetwork.LocalPlayer.NickName);
            
            transform.name = PlayGamesPlatform.Instance.GetUserDisplayName();
            if(text!= null)
            text.gameObject.SetActive(false);
        }
    }

   
    void setText(string name) {

        text.text = name;
    }
	
}
