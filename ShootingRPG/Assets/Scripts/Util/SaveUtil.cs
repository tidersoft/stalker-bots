﻿using CI.QuickSave;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveUtil : MonoBehaviour
{
    string path = "";

    [System.Serializable]
    public class Node {
        public string name;
        public List<Values> values;
    }
   
    [System.Serializable]
    public class Values {
        public string name;
        public string value;

    }

    [System.Serializable]
    public class SaveFile {
        public string name;
        public List<Node> nodes;
        public List<Values> values;
    }

    public static void SaveGame() {
        SaveFile file = new SaveFile();
        file.nodes = new List<Node>();
        file.values = new List<Values>();

        Values val = new Values();
        val.name = "Inf:Loudout:size";
        val.value = ItemData.instance.infloud.Length.ToString();
        file.values.Add(val);

        int index = 0;
        for (int i = 0; i < ItemData.instance.infloud.Length; i++) {
            Node node = new Node();
            node.name = "Inf:Loudout";
            node.values = new List<Values>();

            val = new Values();
            val.name = "Armor:Id";
            val.value = ItemData.instance.infloud[i].armor.id.ToString();
            node.values.Add(val);
            val = new Values();
            val.name = "SecWep:Id";
            val.value = ItemData.instance.infloud[i].seconderyWeapon.id.ToString();
            node.values.Add(val);
            val = new Values();
            val.name = "PriWep:Id";
            val.value = ItemData.instance.infloud[i].primeryWeapon.id.ToString();
            node.values.Add(val);
            val = new Values();
            val.name = "MelWep:Id";
            val.value = ItemData.instance.infloud[i].meleWeapon.id.ToString();
            node.values.Add(val);
            val = new Values();
            val.name = "Name";
            val.value = ItemData.instance.infloud[i].name;
            node.values.Add(val);
            val = new Values();
            val.name = "Charakter";
            val.value = ItemData.instance.infloud[i].charId.ToString();
            node.values.Add(val);
            val = new Values();
            val.name = "Skill:1";
            val.value = ItemData.instance.infloud[i].sklis[0].id.ToString();
            node.values.Add(val);
            val = new Values();
            val.name = "Skill:2";
            val.value = ItemData.instance.infloud[i].sklis[1].id.ToString();
            node.values.Add(val);
            val = new Values();
            val.name = "Skill:3";
            val.value = ItemData.instance.infloud[i].sklis[2].id.ToString();
            node.values.Add(val);
            val = new Values();
            val.name = "Skill:4";
            val.value = ItemData.instance.infloud[i].sklis[3].id.ToString();
            node.values.Add(val);

            file.nodes.Add(node);
            index++;
        }

        Node hasWep = new Node();
        hasWep.name = "Have:Weapon";
        hasWep.values = new List<Values>();
        ItemData.Weapon[] weps= ItemData.instance.weapons;
        for (int i = 0; i < weps.Length; i++) {
            if (weps[i].have) {
                val = new Values();
                val.name = i.ToString();
                val.value = Boolean.TrueString;
                hasWep.values.Add(val);
            }
        }
        file.nodes.Add(hasWep);

        Node hasSkill = new Node();
        hasSkill.name = "Have:Skill";
        hasSkill.values = new List<Values>();
        SkillData.Skil[] skils = SkillData.instance.skill;
        for (int i = 0; i < skils.Length; i++)
        {
            if (skils[i].have)
            {
                val = new Values();
                val.name = i.ToString();
                val.value = Boolean.TrueString;
                hasSkill.values.Add(val);
            }
        }
        file.nodes.Add(hasSkill);

        Node hasEqu = new Node();
        hasEqu.name = "Have:Equp";
        hasEqu.values = new List<Values>();
        ItemData.Equip[] equ = ItemData.instance.equipment;
        for (int i = 0; i < equ.Length; i++)
        {
            if (equ[i].have)
            {
                val = new Values();
                val.name = i.ToString();
                val.value = Boolean.TrueString;
                hasEqu.values.Add(val);
            }
        }
        file.nodes.Add(hasEqu);



        string save = JsonUtility.ToJson(file);
        Debug.Log(save);
        int slot = PlayerPrefs.GetInt("SaveSlot");
        QuickSaveWriter.Create("Save" + slot.ToString()).Write("SaveData", save).Commit();
        PlayerPrefs.SetString("Save" + slot, save);
        PlayerPrefs.SetInt("PreviousSave" + slot, 10);
       // PlayerPrefs.SetInt("PlayerLevel"+slot,);

        PlayerPrefs.Save();

    }

    public static void LoadGame(int slot) {
        string save = "";
        QuickSaveReader.Create("Save" + slot.ToString()).Read<string>("SaveData", (r) => { save = r; });
       // PlayerPrefs.GetString("Save" + slot);
        Debug.Log(save);
        SaveFile file = JsonUtility.FromJson<SaveFile>(save);
        int Inf = Int32.Parse(file.values[0].value);
        ItemData.instance.infloud = new Infrontery[Inf];
        int index = 0;
        file.nodes.ForEach(node =>
        {
            if (node.name.Equals("Inf:Loudout"))
            {
                ItemData.instance.infloud[index] = new Infrontery();
                node.values.ForEach(value =>
                {
                    if (value.name.Equals("Name"))
                        ItemData.instance.infloud[index].name = value.value;

                    if (value.name.Equals("PriWep:Id"))
                        ItemData.instance.infloud[index].primeryWeapon = ItemData.instance.weapons[Int32.Parse(value.value)];

                    if (value.name.Equals("SecWep:Id"))
                        ItemData.instance.infloud[index].seconderyWeapon = ItemData.instance.weapons[Int32.Parse(value.value)];

                    if (value.name.Equals("MelWep:Id"))
                        ItemData.instance.infloud[index].meleWeapon = ItemData.instance.weapons[Int32.Parse(value.value)];

                    if (value.name.Equals("Armor:Id"))
                        ItemData.instance.infloud[index].armor = ItemData.instance.equipment[Int32.Parse(value.value)];

                    if (value.name.Equals("Charakter"))
                    {
                        ItemData.instance.infloud[index].charId = Int32.Parse(value.value);
                        ItemData.instance.infloud[index].charakter = CharacterData.instance.player[Int32.Parse(value.value)].playerPrefab;
                    }
                    if (value.name.Equals("Skill:1"))
                        ItemData.instance.infloud[index].sklis[0] = SkillData.instance.skill[Int32.Parse(value.value)];
                    if (value.name.Equals("Skill:2"))
                        ItemData.instance.infloud[index].sklis[1] = SkillData.instance.skill[Int32.Parse(value.value)];
                    if (value.name.Equals("Skill:3"))
                        ItemData.instance.infloud[index].sklis[2] = SkillData.instance.skill[Int32.Parse(value.value)];
                    if (value.name.Equals("Skill:4"))
                        ItemData.instance.infloud[index].sklis[3] = SkillData.instance.skill[Int32.Parse(value.value)];


                });
                index++;
            }
            if (node.name.Equals("Have:Weapon"))
            {
                node.values.ForEach(value => {
                    ItemData.instance.weapons[Int32.Parse(value.name)].have = Boolean.Parse(value.value);
                });
            }
            if (node.name.Equals("Have:Skill"))
            {
                node.values.ForEach(value => {
                    SkillData.instance.skill[Int32.Parse(value.name)].have = Boolean.Parse(value.value);
                });
            }
            if (node.name.Equals("Have:Equp"))
            {
                node.values.ForEach(value => {
                    ItemData.instance.equipment[Int32.Parse(value.name)].have = Boolean.Parse(value.value);
                });
            }
        });
    }

  

}
