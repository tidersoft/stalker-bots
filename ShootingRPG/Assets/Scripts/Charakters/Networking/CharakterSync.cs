﻿using GooglePlayGames;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharakterSync : MonoBehaviour {

    public string mParticipantId = null;
    public Vector3 curentpos;
    public Vector3 lastpos;
    // Use this for initialization
    void Start () {
		
	}

    public void setParticipantId(string id) {
        mParticipantId = id;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!isMine()) 
            return;
     


        curentpos = transform.position;
        if (Vector3.Distance(curentpos,lastpos)>0.1f) {
            sendPos();
            lastpos = curentpos;
        }
	}

    public void sendPos() {
        GameMenager.Instance.SendVector3("Upos",transform.name,curentpos);
    }
    public void setPos(Vector3 vec) {
        transform.position = vec;
    }

    public bool isMine() {

     //   Debug.Log(mParticipantId + " -> " + PlayGamesPlatform.Instance.GetUserId());
      //  Debug.Log(mParticipantId.Equals(PlayGamesPlatform.Instance.GetUserId()).ToString());
        return mParticipantId.Equals(PlayGamesPlatform.Instance.GetUserId());
    }
}
