﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoudoutMenu : MonoBehaviour {



    public Rect windowRect;
    private int window = 0;
    public bool show = false;
    public int selected = 0;
    public float width, height;
    int hover;
    bool onhover = false;
    int skilIndex = 0;

    private bool enter = false;
    public EqType sortTyp;
    public string lastTooltip = " ";
    public string Tooltip = "";
    private GameObject showingModel;
    public Transform modelPosition;

    public Texture button;
    public RenderTexture text;

    public Texture2D backGroundBar;
    public Texture2D Bar;
    public Texture2D Bar2;
    public GUIStyle statusFont;

   public int charid ;
   public CharacterData.PlayerData charackter ;


    // Use this for initialization
    void Start () {
        modelPosition = GameObject.FindGameObjectWithTag("ModelPos").transform;
        charid = ItemData.instance.infloud[selected].charId;
        charackter = CharacterData.instance.player[charid];
    }

    void Update()
    {
        if (Input.GetKeyDown("e") && enter)
        {
             OnOffMenu();

        }

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && other.GetComponent<CharakterSync>().isMine())
        {
                enter = true;
           
        }

    }
    void OnTriggerExit(Collider other)
    {
      if (other.gameObject.tag == "Player") 
     //   if (other.gameObject == player)
        {
            enter = false;
        }
    }

    private void OnGUI()
    {
        float centerx = Screen.width / 2;
        float centery = Screen.height / 2;

        if (!show) {
            if (enter)
            {
                if (GUI.Button(new Rect(Screen.width / 2 - 130, Screen.height - 180, 260, 80), button))
                {
                    OnOffMenu();

                }
            }
                return;

        }

          if (window == 0)
        {
            windowRect = GUI.Window(2, new Rect(Screen.width / 2 - (Screen.width - width) / 2, (Screen.height / 2) - height/2, width, height), LoudoutWin, "Loudout");
            ShowModel();
        }
        if (window == 1)
        {
            windowRect = GUI.Window(2, new Rect(Screen.width / 2 - (Screen.width - width) / 2, (Screen.height / 2) - height / 2, width, height), LoudoutInfWin, "Loudout");
            CountPoints();
            HideModel();
        }
        if (window == 2)
        {
            windowRect = GUI.Window(2, new Rect(Screen.width / 2 - (Screen.width - width) / 2, (Screen.height / 2) - height / 2, width, height), SelectWindow, "Loudout");
            HideModel();
        }
        if (window == 3)
        {
            windowRect = GUI.Window(2, new Rect(Screen.width / 2 - (Screen.width - width) / 2, (Screen.height / 2) - height / 2, width, height), SelectSkillWindow, "Loudout");
            HideModel();
        }
    }


    void LoudoutWin(int windowID) {
        if (GUI.Button(new Rect(width-50,height- 50, 40, 40), "X"))
        {
            OnOffMenu();
        }

        Infrontery[] inf = ItemData.instance.infloud;
        for (int i = 0; i < inf.Length; i++) {

            if (GUI.Button(new Rect(25, 50 + (i * 60), 150, 50),new GUIContent( inf[i].name,i.ToString()))) {
                window = 1;
                selected = i;
            }
            if (inf.Length > 1) {
                if (GUI.Button(new Rect(195, 50 + (i * 60), 50, 50), "X")) {
                    DeleteLoudout(i);
                }
            }
        }

        if (inf.Length < 6) {
             if (GUI.Button(new Rect(75, 50 + (inf.Length * 60), 50, 50),"+")) {
                AddLoudout();
            }
        }

        if (onhover)
        {
            GUI.Box(new Rect(255, 50, 75, 75), inf[hover].primeryWeapon.icon);
            GUI.Box(new Rect(255, 135, 75, 75), inf[hover].seconderyWeapon.icon);
            GUI.Box(new Rect(255, 220, 75, 75), inf[hover].meleWeapon.icon);
            GUI.Box(new Rect(255, 305, 75, 75), inf[hover].armor.icon);

        }
        if (Event.current.type == EventType.Repaint && GUI.tooltip != lastTooltip)
        {
            if (lastTooltip != "")
            {
                SendMessage("LoudOnMouseOut", SendMessageOptions.DontRequireReceiver);
            }

            if (GUI.tooltip != "")
            {
                SendMessage("LoudOnMouseOver", SendMessageOptions.DontRequireReceiver);
            }

            lastTooltip = GUI.tooltip;
        }


        
        GUI.DrawTexture(new Rect(width - 225, 50, 175, 300), text);


    }

    void DeleteLoudout(int index) {

        ItemData.instance.infloud[index] = null;
        int leng = ItemData.instance.infloud.Length - 1;
        Infrontery[] inf = new Infrontery[leng];
        int ind = 0;
        for (int i = 0; i < ItemData.instance.infloud.Length; i++) {
            if (ItemData.instance.infloud[i] != null) {
                inf[ind] = ItemData.instance.infloud[i];
                ind++;
            }
        }
        ItemData.instance.infloud = inf;
    }

    void AddLoudout() {
        int leng = ItemData.instance.infloud.Length + 1;
        Infrontery[] inf = new Infrontery[leng];
        int ind = 0;
        for (int i = 0; i < ItemData.instance.infloud.Length; i++)
        {
            if (ItemData.instance.infloud[i] != null)
            {
                inf[ind] = ItemData.instance.infloud[i];
                ind++;
            }
        }
        inf[ItemData.instance.infloud.Length] = new Infrontery();

        inf[ItemData.instance.infloud.Length].charakter =  CharacterData.instance.player[PlayerHelper.getLocalPlayer().GetComponent<status>().charakterId].playerPrefab;
        inf[ItemData.instance.infloud.Length].name = PlayerHelper.getLocalPlayer().name.Replace("(Clone)","");
        inf[ItemData.instance.infloud.Length].primeryWeapon = new ItemData.Weapon();
        inf[ItemData.instance.infloud.Length].seconderyWeapon = new ItemData.Weapon();
        inf[ItemData.instance.infloud.Length].meleWeapon = new ItemData.Weapon();
        inf[ItemData.instance.infloud.Length].armor = new ItemData.Equip();
        inf[ItemData.instance.infloud.Length].charId = PlayerHelper.getLocalPlayer().GetComponent<status>().charakterId;
        inf[ItemData.instance.infloud.Length].sklis = new SkillData.Skil[4];
        for (int i = 0; i < 4; i++) {
            inf[ItemData.instance.infloud.Length].sklis[i] = new SkillData.Skil();
        }
        ItemData.instance.infloud = inf;

    }

    void LoudOnMouseOut() {
        hover = 0;
        onhover = false;
    }

    void LoudOnMouseOver() {
        
        onhover = true;
        hover = Int32.Parse(GUI.tooltip);
    }

    void ShowModel() {
        if (showingModel==null)
        {
            showingModel = Instantiate(CharacterData.instance.player[charid].characterSelectModel, modelPosition.position, modelPosition.rotation);
           
        }
        if (!text.IsCreated())
            text.Create();
    }
    void HideModel() {
        if (showingModel!=null)
        {
            Destroy(showingModel);
            showingModel = null;
        }
        if (text.IsCreated())
            text.Release();
    }


    void SelectSkillWindow(int WindowID) {
        int x = 0;
        int y = 0;
        int row = 2;

        
            SkillData.Skil[] wep = SkillData.instance.skill;
            for (int i = 0; i < wep.Length; i++)
            {
                if (wep[i] != null)
                {
                if (wep[i] != ItemData.instance.infloud[selected].sklis[skilIndex])
                {
                    
                    if (wep[i].have)
                    {
                        if (GUI.Button(new Rect(25 + (x * 110), 25 + (y * 110), 100, 100), new GUIContent(wep[i].icon, i.ToString())))
                        {
                            SelectSkill(wep[i]);
                        }
                        x++;
                        if (x > row)
                        {
                            x = 0;
                            y++;
                        }
                    }
                }
                }

            
        }

        if (Event.current.type == EventType.Repaint && GUI.tooltip != lastTooltip)
        {
            if (lastTooltip != "")
            {
                SendMessage("SelectSkillOnMouseOut", SendMessageOptions.DontRequireReceiver);
            }

            if (GUI.tooltip != "")
            {
                SendMessage("SelectSkillOnMouseOver", SendMessageOptions.DontRequireReceiver);
            }

            lastTooltip = GUI.tooltip;
        }

        GUIStyle styl = GUI.skin.box;
        styl.alignment = TextAnchor.UpperLeft;
        GUI.Box(new Rect(width - 200, 25, 185, height - 85), Tooltip, styl);
        //  GUI.Label(new Rect(width-200, 50, 175, 500), Tooltip);


    }

    void SelectWindow(int WindowID) {

        int x = 0;
        int y = 0;
        int row = 2;
        if (sortTyp == EqType.PrimaryWeapon || sortTyp == EqType.SecondaryWeapon)
        {
            ItemData.Weapon[] wep = ItemData.instance.weapons;
            for (int i = 0; i < wep.Length; i++)
            {
                if (wep[i] != null)
                {
                    if (wep[i].equipmentType == sortTyp)
                    {
                        if (charackter.weponFilter.Contains(wep[i].useAmmo)) {
                            if (wep[i].have)
                            {
                                if (GUI.Button(new Rect(25 + (x * 110), 25 + (y * 110), 100, 100), new GUIContent(wep[i].icon, i.ToString())))
                                {
                                    SelectWeapon(wep[i]);
                                }
                                x++;
                                if (x > row)
                                {
                                    x = 0;
                                    y++;
                                }
                            }
                        }
                    }
                }

            }
        }

        if (sortTyp == EqType.MeleeWeapon)
        {
            ItemData.Weapon[] wep = ItemData.instance.weapons;
            for (int i = 0; i < wep.Length; i++)
            {
                if (wep[i] != null)
                {
                    if (wep[i].equipmentType == sortTyp)
                    {
                        if (charackter.meleFilter.Contains(wep[i].meleType))
                        {
                            if (wep[i].have)
                            {
                                if (GUI.Button(new Rect(25 + (x * 110), 25 + (y * 110), 100, 100), new GUIContent(wep[i].icon, i.ToString())))
                                {
                                    SelectWeapon(wep[i]);
                                }
                                x++;
                                if (x > row)
                                {
                                    x = 0;
                                    y++;
                                }
                            }
                        }
                    }
                }

            }
        }

        if (sortTyp == EqType.Armor)
        {
            ItemData.Equip[] wep = ItemData.instance.equipment;
            for (int i = 0; i < wep.Length; i++)
            {
                if (wep[i] != null)
                {
                    if (wep[i].equipmentType == sortTyp)
                    {
                        if (charackter.armorFilter.Contains(wep[i].armorType))
                        {
                            if (wep[i].have)
                            {
                                if (GUI.Button(new Rect(25 + (x * 110), 25 + (y * 110), 100, 100), new GUIContent(wep[i].icon, i.ToString())))
                                {
                                    SelectEquip(wep[i]);
                                }
                                x++;
                                if (x > row)
                                {
                                    x = 0;
                                    y++;
                                }
                            }
                        }
                    }
                }

            }
        }


        if (Event.current.type == EventType.Repaint && GUI.tooltip != lastTooltip)
        {
            if (lastTooltip != "")
            {
                SendMessage("SelectOnMouseOut", SendMessageOptions.DontRequireReceiver);
            }

            if (GUI.tooltip != "")
            {
                SendMessage("SelectOnMouseOver", SendMessageOptions.DontRequireReceiver);
            }

            lastTooltip = GUI.tooltip;
        }

        GUIStyle styl = GUI.skin.box;
        styl.alignment = TextAnchor.UpperLeft;
        GUI.Box(new Rect(width - 200, 25, 185, height - 85), Tooltip,styl);
        //  GUI.Label(new Rect(width-200, 50, 175, 500), Tooltip);



    }

    void SelectOnMouseOut() {
        Tooltip = "";

    }

    void SelectOnMouseOver() {
      
            if (sortTyp == EqType.PrimaryWeapon)
        {
            ItemData.Weapon[] wep = ItemData.instance.weapons;
            int index = Int32.Parse(GUI.tooltip);
            ItemData.Weapon ite = wep[index];

            Tooltip = ite.description;
                Tooltip += "\n";
                Tooltip += "Damage: " + ite.attack;
                Tooltip += "\n";
                Tooltip += "Attack speed: " + ite.attackDelay;
                Tooltip += "\n";
                Tooltip += "Ammo clip: " + ite.maxAmmo;
                Tooltip += "\n";
                Tooltip += "Ammo type: " + Enum.GetName(typeof(AmmoType),ite.useAmmo);
        }
            if (sortTyp == EqType.SecondaryWeapon)
        {
            ItemData.Weapon[] wep = ItemData.instance.weapons;
            int index = Int32.Parse(GUI.tooltip);
            ItemData.Weapon ite = wep[index];

            Tooltip = ite.description;
                Tooltip += "\n";
                Tooltip += "Damage: " + ite.attack;
                Tooltip += "\n";
                Tooltip += "Attack speed: " + ite.attackDelay;
                Tooltip += "\n";
                Tooltip += "Ammo clip: " + ite.maxAmmo;
                Tooltip += "\n";
                Tooltip += "Ammo type: " + Enum.GetName(typeof(AmmoType), ite.useAmmo);
        }
            if (sortTyp == EqType.MeleeWeapon)
        {
            ItemData.Weapon[] wep = ItemData.instance.weapons;
            int index = Int32.Parse(GUI.tooltip);
            ItemData.Weapon ite = wep[index];

            Tooltip = ite.description;
                Tooltip += "\n";
                Tooltip += "Damage: " + ite.meleeDamage;
                Tooltip += "\n";
                Tooltip += "Attack speed: " + ite.attackDelay;
                Tooltip += "\n";
                Tooltip += "Weapon type: " + Enum.GetName(typeof(MeleType),  ite.meleType);
                Tooltip += "\n";
            //Tooltip += "Ammo clip: " + ite.maxAmmo;
        }
            if (sortTyp == EqType.Armor)
        {
            ItemData.Equip[] wep = ItemData.instance.equipment;
            int index = Int32.Parse(GUI.tooltip);
            ItemData.Equip ite = wep[index];

            Tooltip = ite.description;
                Tooltip += "\n";
                Tooltip += "Armor: " + ite.defense;
                Tooltip += "\n";
                Tooltip += "Magic armor: " + ite.magicDefense;
                Tooltip += "\n";
                Tooltip += "Armor type: " + Enum.GetName(typeof(ArmorType), ite.armorType);
                Tooltip += "\n";
            //Tooltip += "Ammo clip: " + ite.maxAmmo;
        }
       
    }
    void SelectSkillOnMouseOut()
    {
        Tooltip = "";

    }

    void SelectSkillOnMouseOver()
    {

        
            SkillData.Skil[] wep = SkillData.instance.skill;
            int index = Int32.Parse(GUI.tooltip);
            SkillData.Skil ite = wep[index];

            if (!ite.description.Equals(string.Empty))
            {
                Tooltip = ite.description;
                Tooltip += "\n";
                Tooltip += "Mana cost: " + ite.manaCost;
                Tooltip += "\n";
                Tooltip += "Cast time: " + ite.castTime;
                Tooltip += "\n";
                Tooltip += "Delay: " + ite.skillDelay;
            }
        
     

    }

    void SelectWeapon(ItemData.Weapon wep) {
        Infrontery inf= ItemData.instance.infloud[selected];
        if (sortTyp == EqType.PrimaryWeapon) {
            inf.primeryWeapon = wep;
            window = 1;
        }
        if (sortTyp == EqType.SecondaryWeapon)
        {
            inf.seconderyWeapon = wep;
            window = 1;
        }
        if (sortTyp == EqType.MeleeWeapon)
        {
            inf.meleWeapon = wep;
            window = 1;
        }
        CountPoints();
    }


    void CountPoints() {

        int temp = 0;
        Infrontery inf = ItemData.instance.infloud[selected];
        temp += inf.primeryWeapon.pointCost;
        temp += inf.seconderyWeapon.pointCost;
        temp += inf.meleWeapon.pointCost;
        temp += inf.armor.pointCost;

        for (int i = 0; i < 4; i++) {
            temp += inf.sklis[i].pointCost;
        }

        inf.points = temp;
    }

    void SelectSkill(SkillData.Skil wep)
    {
        Infrontery inf = ItemData.instance.infloud[selected];
       
            inf.sklis[skilIndex] = wep;
            window = 1;
        CountPoints();
    }

    void SelectEquip(ItemData.Equip wep)
    {
        Infrontery inf = ItemData.instance.infloud[selected];
        if (sortTyp == EqType.Armor)
        {
            inf.armor = wep;
            window = 1;
        }
        /*if (sortTyp == EqType.SecondaryWeapon)
        {
            inf.seconderyWeapon = wep;
            window = 1;
        }
        if (sortTyp == EqType.MeleeWeapon)
        {
            inf.meleWeapon = wep;
            window = 1;
        }*/
    }

    void LoudoutInfWin(int windowID)
    {

        Infrontery inf = ItemData.instance.infloud[selected];

        //Close Window Button
        if (GUI.Button(new Rect(width-50,height-50, 40, 40), "X"))
        {
            if (inf.points <= 1000)
                OnOffMenu();
           
        }

       inf.name = GUI.TextField(new Rect(25, 40, 150, 25), inf.name);

        if (GUI.Button(new Rect(25, 75, 200, 100),new GUIContent(inf.primeryWeapon.icon,"Primery"))) {
            sortTyp = EqType.PrimaryWeapon;
            window = 2;
        }
        if (GUI.Button(new Rect(25, height-240, 100, 100),new GUIContent(inf.seconderyWeapon.icon,"Secondery")))
        {
            sortTyp = EqType.SecondaryWeapon;
            window = 2;
        }
        if (GUI.Button(new Rect(25, height-125, 100, 100),new GUIContent(inf.meleWeapon.icon,"Mele")))
        {
            sortTyp = EqType.MeleeWeapon;
            window = 2;
        }
        if (GUI.Button(new Rect(275, height - 125, 100, 100), new GUIContent(inf.armor.icon, "Armor")))
        {
            sortTyp = EqType.Armor;
            window = 2;
        }

        if (GUI.Button(new Rect(275, 72, 50, 50), new GUIContent(inf.sklis[0].icon, "Skill1")))
        {
            skilIndex = 0;
            window = 3;
        }
        if (GUI.Button(new Rect(330, 72, 50, 50), new GUIContent(inf.sklis[1].icon, "Skill2")))
        {
            skilIndex = 1;
            window = 3;
        }
        if (GUI.Button(new Rect(275, 125, 50, 50), new GUIContent(inf.sklis[2].icon, "Skill3")))
        {
            skilIndex = 2;
            window = 3;
        }
        if (GUI.Button(new Rect(330, 125, 50, 50), new GUIContent(inf.sklis[3].icon, "Skill4")))
        {
            skilIndex = 3;
            window = 3;
        }

        if (Event.current.type == EventType.Repaint && GUI.tooltip != lastTooltip)
        {
            if (lastTooltip != "")
            {
                SendMessage(lastTooltip + "OnMouseOut", SendMessageOptions.DontRequireReceiver);
            }

            if (GUI.tooltip != "")
            {
                SendMessage(GUI.tooltip + "OnMouseOver", SendMessageOptions.DontRequireReceiver);
            }

            lastTooltip = GUI.tooltip;
        }

        GUIStyle styl = GUI.skin.box;
        styl.alignment = TextAnchor.UpperLeft;

        GUI.Box(new Rect(width - 200, 60, 185, height - 125), Tooltip,styl);


        int hp = (int)(inf.points * 100 / 1000 * 1.95f);
        
        GUI.DrawTexture(new Rect(185, 40, 195, 25), backGroundBar);
        if (inf.points < 1001)
            GUI.DrawTexture(new Rect(185, 40, hp, 25), Bar);
        else
           GUI.DrawTexture(new Rect(185, 40, 195, 25), Bar2);

        GUI.Label(new Rect(200, 40, 200, 25), "Points : " + inf.points.ToString(), statusFont);




        // GUI.Label(new Rect(width-200, 50, 175, 500), Tooltip);




        /*
         if (GUI.Button(new Rect(30, 30, 75, 75), dataItem.usableItem[itemShopSlot[0 + page]].icon))
         {
             select = 0 + page;
             buywindow = true;
         }
         GUI.Label(new Rect(125, 40, 320, 75), dataItem.usableItem[itemShopSlot[0 + page]].itemName.ToString(), itemNameText); //Item Name
         GUI.Label(new Rect(125, 65, 320, 75), dataItem.usableItem[itemShopSlot[0 + page]].description.ToString(), itemDescriptionText); //Item Description
         GUI.Label(new Rect(340, 55, 140, 40), "$ : " + dataItem.usableItem[itemShopSlot[0 + page]].price, itemDescriptionText); //Show Item's Price
                                                                                                                                 //----------------------------------------------
         if (GUI.Button(new Rect(30, 120, 75, 75), dataItem.usableItem[itemShopSlot[1 + page]].icon))
         {
             select = 1 + page;
             buywindow = true;
         }
         GUI.Label(new Rect(125, 130, 320, 75), dataItem.usableItem[itemShopSlot[1 + page]].itemName.ToString(), itemNameText); //Item Name
         GUI.Label(new Rect(125, 155, 320, 75), dataItem.usableItem[itemShopSlot[1 + page]].description.ToString(), itemDescriptionText); //Item Description
         GUI.Label(new Rect(340, 145, 140, 40), "$ : " + dataItem.usableItem[itemShopSlot[1 + page]].price, itemDescriptionText); //Show Item's Price
                                                                                                                                  //----------------------------------------------
         if (GUI.Button(new Rect(30, 210, 75, 75), dataItem.usableItem[itemShopSlot[2 + page]].icon))
         {
             select = 2 + page;
             buywindow = true;
         }
         GUI.Label(new Rect(125, 220, 320, 75), dataItem.usableItem[itemShopSlot[2 + page]].itemName.ToString(), itemNameText); //Item Name
         GUI.Label(new Rect(125, 245, 320, 75), dataItem.usableItem[itemShopSlot[2 + page]].description.ToString(), itemDescriptionText); //Item Description
         GUI.Label(new Rect(340, 235, 140, 40), "$ : " + dataItem.usableItem[itemShopSlot[2 + page]].price, itemDescriptionText); //Show Item's Price
                                                                                                                                  //----------------------------------------------
         if (GUI.Button(new Rect(30, 300, 75, 75), dataItem.usableItem[itemShopSlot[3 + page]].icon))
         {
             select = 3 + page;
             buywindow = true;
         }
         GUI.Label(new Rect(125, 310, 320, 75), dataItem.usableItem[itemShopSlot[3 + page]].itemName.ToString(), itemNameText); //Item Name
         GUI.Label(new Rect(125, 335, 320, 75), dataItem.usableItem[itemShopSlot[3 + page]].description.ToString(), itemDescriptionText); //Item Description
         GUI.Label(new Rect(340, 325, 140, 40), "$ : " + dataItem.usableItem[itemShopSlot[3 + page]].price, itemDescriptionText); //Show Item's Price
                                                                                                                                  //----------------------------------------------
         if (GUI.Button(new Rect(30, 390, 75, 75), dataItem.usableItem[itemShopSlot[4 + page]].icon))
         {
             select = 4 + page;
             buywindow = true;
         }
         GUI.Label(new Rect(125, 400, 320, 75), dataItem.usableItem[itemShopSlot[4 + page]].itemName.ToString(), itemNameText); //Item Name
         GUI.Label(new Rect(125, 425, 320, 75), dataItem.usableItem[itemShopSlot[4 + page]].description.ToString(), itemDescriptionText); //Item Description
         GUI.Label(new Rect(340, 415, 140, 40), "$ : " + dataItem.usableItem[itemShopSlot[4 + page]].price, itemDescriptionText); //Show Item's Price
                                                                                                                                  //----------------------------------------------
         if (GUI.Button(new Rect(220, 485, 50, 52), "1"))
         {
             page = 0;
         }
         if (GUI.Button(new Rect(290, 485, 50, 52), "2"))
         {
             page = pageMultiply;
         }
         GUI.Label(new Rect(20, 505, 150, 50), "$ " + cash.ToString(), itemDescriptionText);
         GUI.DragWindow(new Rect(0, 0, 10000, 10000));*/
    }

    public void OnOffMenu() {
        if (!show) {
            show = true;
            PlayerHelper.getLocalPlayer().GetComponent<status>().setEnable(false);

        }
        else
        if (window == 1)
        {
            window = 0;
        }else
        if (window == 0) {
            PlayerHelper.getLocalPlayer().GetComponent<status>().setEnable(true);

            SaveUtil.SaveGame();
            show = false;
        }
    }

    void PrimeryOnMouseOver()
    {
        Infrontery inf = ItemData.instance.infloud[selected];
        if (!inf.primeryWeapon.description.Equals(string.Empty))
        {
            Tooltip = inf.primeryWeapon.description;
            Tooltip += "\n";
            Tooltip += "Damage: " + inf.primeryWeapon.attack;
            Tooltip += "\n";
            Tooltip += "Attack speed: " + inf.primeryWeapon.attackDelay;
            Tooltip += "\n";
            Tooltip += "Ammo clip: " + inf.primeryWeapon.maxAmmo;
        }
     //   Debug.Log("Play game got focus");
    }

    void PrimeryOnMouseOut()
    {
        Tooltip = "";
       // Debug.Log("Quit lost focus");
    }

    void ArmorOnMouseOver()
    {
        Infrontery inf = ItemData.instance.infloud[selected];
        if (!inf.armor.description.Equals(string.Empty))
        {
            Tooltip = inf.armor.description;
            Tooltip += "\n";
            Tooltip += "Armor: " + inf.armor.defense;
            Tooltip += "\n";
            Tooltip += "Magic armor " + inf.armor.magicDefense;
          //  Tooltip += "\n";
            //Tooltip += "Ammo clip: " + inf.primeryWeapon.maxAmmo;
        }
        //   Debug.Log("Play game got focus");
    }

    void ArmorOnMouseOut()
    {
        Tooltip = "";
        // Debug.Log("Quit lost focus");
    }
    void SeconderyOnMouseOver()
    {
        Infrontery inf = ItemData.instance.infloud[selected];

        if (!inf.seconderyWeapon.description.Equals(string.Empty))
        {
            Tooltip = inf.seconderyWeapon.description;
            Tooltip += "\n";
            Tooltip += "Damage: " + inf.seconderyWeapon.attack;
            Tooltip += "\n";
            Tooltip += "Attack speed: " + inf.seconderyWeapon.attackDelay;
            Tooltip += "\n";
            Tooltip += "Ammo clip: " + inf.seconderyWeapon.maxAmmo;
        }
        // Debug.Log("Play game got focus");
    }

    void SeconderyOnMouseOut()
    {
        Tooltip = "";
      //  Debug.Log("Quit lost focus");
    }
    void MeleOnMouseOver()
    {
        Infrontery inf = ItemData.instance.infloud[selected];

        if (!inf.meleWeapon.description.Equals(string.Empty))
        {
            Tooltip = inf.meleWeapon.description;
            Tooltip += "\n";
            Tooltip += "Damage: " + inf.meleWeapon.meleeDamage;
            Tooltip += "\n";
            Tooltip += "Attack speed: " + inf.meleWeapon.attackDelay;
            Tooltip += "\n";
          //  Tooltip += "Ammo clip: " + inf.primeryWeapon.maxAmmo;
        }
        // Debug.Log("Play game got focus");
    }

    void MeleOnMouseOut()
    {
        Tooltip = "";
       // Debug.Log("Quit lost focus");
    }

  

    void Skill1OnMouseOut()
    {
        Tooltip = "";
        // Debug.Log("Quit lost focus");
    }

    void Skill1OnMouseOver()
    {
        Infrontery inf = ItemData.instance.infloud[selected];
        if (!inf.sklis[0].description.Equals(string.Empty))
        {
            Tooltip = inf.sklis[0].description;
            Tooltip += "\n";
            Tooltip += "Mana cost: " + inf.sklis[0].manaCost;
            Tooltip += "\n";
            Tooltip += "Cast time: " + inf.sklis[0].castTime;
            Tooltip += "\n";
            Tooltip += "Delay: " + inf.sklis[0].skillDelay;
        }
        //   Debug.Log("Play game got focus");
    }
    void Skill2OnMouseOut()
    {
        Tooltip = "";
        // Debug.Log("Quit lost focus");
    }

    void Skill2OnMouseOver()
    {
        Infrontery inf = ItemData.instance.infloud[selected];
        if (!inf.sklis[1].description.Equals(string.Empty))
        {
            Tooltip = inf.sklis[1].description;
            Tooltip += "\n";
            Tooltip += "Mana cost: " + inf.sklis[1].manaCost;
            Tooltip += "\n";
            Tooltip += "Cast time: " + inf.sklis[1].castTime;
            Tooltip += "\n";
            Tooltip += "Delay: " + inf.sklis[1].skillDelay;
        }
        //   Debug.Log("Play game got focus");
    }
    void Skill3OnMouseOut()
    {
        Tooltip = "";
        // Debug.Log("Quit lost focus");
    }

    void Skill3OnMouseOver()
    {
        Infrontery inf = ItemData.instance.infloud[selected];
        if (!inf.sklis[2].description.Equals(string.Empty))
        {
            Tooltip = inf.sklis[2].description;
            Tooltip += "\n";
            Tooltip += "Mana cost: " + inf.sklis[2].manaCost;
            Tooltip += "\n";
            Tooltip += "Cast time: " + inf.sklis[2].castTime;
            Tooltip += "\n";
            Tooltip += "Delay: " + inf.sklis[2].skillDelay;
        }
        //   Debug.Log("Play game got focus");
    }
    void Skill4OnMouseOut()
    {
        Tooltip = "";
        // Debug.Log("Quit lost focus");
    }

    void Skill4OnMouseOver()
    {
        Infrontery inf = ItemData.instance.infloud[selected];
        if (!inf.sklis[3].description.Equals(string.Empty))
        {
            Tooltip = inf.sklis[3].description;
            Tooltip += "\n";
            Tooltip += "Mana cost: " + inf.sklis[3].manaCost;
            Tooltip += "\n";
            Tooltip += "Cast time: " + inf.sklis[3].castTime;
            Tooltip += "\n";
            Tooltip += "Delay: " + inf.sklis[3].skillDelay;
        }
        //   Debug.Log("Play game got focus");
    }
}
