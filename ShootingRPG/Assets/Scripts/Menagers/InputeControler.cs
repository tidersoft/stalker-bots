﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputeControler : MonoBehaviour {


    public class InputeState {
        public float Vertical;
        public float Horizontal;
        public bool Fire1;
        public bool Fire2;
        public bool Mele;
        public bool Reload;
        public bool isSprinting;
        public bool A;
        public bool B;
        public bool Use;
        public bool Scope;
         public bool changeWeapon;

        public bool isCrouch;
        public bool jump;

        public bool Skill1;
        public bool Skill2;
        public bool Skill3;
        public bool Skill4;
    }

    public float Vertical{get { return state.Vertical; }set { state.Vertical = value; } }
    public float Horizontal { get { return state.Horizontal; } set { state.Horizontal = value; } }
    public bool Jump { get { return state.jump; } set { state.jump = value; } }


    public bool Fire1 { get { return state.Fire1; } set { state.Fire1 = value; } }
    public bool Fire2 { get { return state.Fire2; } set { state.Fire2 = value; } }
    public bool Mele { get { return state.Mele; } set { state.Mele = value; } }


    public bool Reload { get { return state.Reload; } set { state.Reload = value; } }
    public bool isSprinting { get { return state.isSprinting; } set { state.isSprinting = value; } }
    public bool isCrouch { get { return state.isCrouch; } set { state.isCrouch = value; } }
    public bool Use { get { return state.Use; } set { state.Use = value; } }
    public bool Scope { get { return state.Scope; } set { state.Scope = value; } }
    public bool changeWeapon { get { return state.changeWeapon; } set { state.changeWeapon = value; } }

    public bool A { get { return state.A; } set { state.A = value; } }
    public bool B { get { return state.B; } set { state.B = value; } }

    public bool Skill1 { get { return state.Skill1; } set { state.Skill1 = value; } }
    public bool Skill2 { get { return state.Skill2; } set { state.Skill2 = value; } }
    public bool Skill3 { get { return state.Skill3; } set { state.Skill3 = value; } }
    public bool Skill4 { get { return state.Skill4; } set { state.Skill4 = value; } }

    public Vector2 MouseInput;
    public float MouseWheel;

    public InputeState state;
    public BindingKey binds;


    public class BindingKey {

        public string Vertical = "Vertical";
        public string Horizontal = "Horizontal";
        public string MouseX = "MouseX";
        public string MouseY = "MouseY";

        public string Fire1 = "R1";
        public string Fire2;
        public string Mele = "L2";
        public string Reload = "Reload";
        public string isSprinting;
        public string A;
        public string B;
        public string Use;
        public string Scope = "L1";
        public string changeWeapon = "Change Weapon";

        public string isCrouch = "Crouch";
        public string jump = "R2";

        public string skill1 = "Skill1";
        public string skill2 = "Skill2";
        public string skill3 = "Skill3";
        public string skill4 = "Skill4";

    }

    public bool IsMouse = false;
    

    private void Awake()
    {
        state = new InputeState();
        binds = new BindingKey();
    }
    private void Update()
    {
      
        Vertical = SimpleInput.GetAxis(binds.Vertical);
        Horizontal = SimpleInput.GetAxis(binds.Horizontal);
        if (SimpleInput.GetAxis(binds.MouseX) != 0 || SimpleInput.GetAxis(binds.MouseY) != 0)
            IsMouse = false;
        else
            IsMouse = true;

        


        MouseInput = new Vector2(SimpleInput.GetAxis(binds.MouseX), SimpleInput.GetAxis(binds.MouseY));
        Fire1 = SimpleInput.GetButton(binds.Fire1);
        Fire2 = Input.GetKey(KeyCode.T);
        Mele = SimpleInput.GetButton(binds.Mele);
        Reload = SimpleInput.GetButtonUp(binds.Reload);
        Scope =  SimpleInput.GetButton(binds.Scope);
        Jump = SimpleInput.GetButton(binds.jump);
        changeWeapon = SimpleInput.GetButtonUp(binds.changeWeapon);

        Skill1 = SimpleInput.GetButton(binds.skill1);
        Skill2 = SimpleInput.GetButton(binds.skill2);
        Skill3 = SimpleInput.GetButton(binds.skill3);
        Skill4 = SimpleInput.GetButton(binds.skill4);

        isSprinting = Input.GetKey(KeyCode.LeftShift);
        Use = Input.GetKey(KeyCode.E); 
        MouseWheel = Input.GetAxis("Mouse ScrollWheel");
        A = Input.GetKey(KeyCode.Z);
        isCrouch = Input.GetKey(KeyCode.C);
        B = Input.GetKey(KeyCode.X);
    }
}
