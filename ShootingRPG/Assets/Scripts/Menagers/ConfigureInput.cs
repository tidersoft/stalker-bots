﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ConfigureInput : MonoBehaviour
{
    GameControler.ConrollerSetting config = new GameControler.ConrollerSetting();
    // Start is called before the first frame update
    void Awake()
    {
        config.pos = GetComponent<RectTransform>().position;
        config.size = GetComponent<RectTransform>().localScale.x;
    }

   


    
    public void Selected()
    {
        GameControler.instance.setActive(gameObject);
    }

    public void OnDrag()
    {
        if (!GetComponent<Button>().enabled)
        {
            transform.position = Input.mousePosition;
            config.pos = Input.mousePosition;
        }
        
    }
}
