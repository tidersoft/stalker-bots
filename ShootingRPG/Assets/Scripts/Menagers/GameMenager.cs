﻿
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Multiplayer;
using GooglePlayGames.BasicApi.SavedGame;

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMenager : MonoBehaviour, RealTimeMultiplayerListener 
{

    const int QuickGameOpponents = 1;
    const int GameVariant = 0;

    private string mAutoSaveName;

    public Texture2D mScreenImage;

    private bool mSaving;

    // private GameProgress mProgress;


    bool invite = false;

    const int MinOpponents = 1;
    const int MaxOpponents = 3;
    public bool isLocalGame = true;

    public string LocalId;

    //  public event System.Action<Player> OnLocalPlayerJoined;
    #region Instance
    private static GameMenager m_istance;
    [SerializeField]
    public static GameMenager Instance
    {
        get
        {

            return m_istance;
        }

    }



    public bool loged;

    void Start()
    {
        m_istance = this;



        //  m_istance.LocalId = PlayGamesPlatform.Instance.RealTime.GetSelf().ParticipantId;
        //   m_istance.mProgress = GameProgress.LoadFromDisk();

        //m_istance.gameObject = GameObject.Find("_gameMenager");
        //   m_istance.gameObject.AddComponent<Timer>();
        //     m_istance.gameObject.AddComponent<Respawner>();

        // sh = gameObject.AddComponent<CoroutineScheduler>();
    }

    private bool m_IsNetworkGame;
    private bool m_IsNetworkGameCheck;
    [SerializeField]
    public bool IsNetworkGamen {
        get
        {
            if (m_IsNetworkGameCheck) {
                m_IsNetworkGameCheck = true;
                m_IsNetworkGame = GameObject.Find("NetworkManager") != null;

            }
            return m_IsNetworkGame;
        }
    }

    /* private Timer m_Timer;
     public Timer Timer {
         get {
             if (m_Timer == null) {
                 m_Timer = gameObject.GetComponent<Timer>();
             }
             return m_Timer;
         }
     }

     private Respawner m_Respawner;
     public Respawner Respawner
     {
         get
         {
             if (m_Respawner == null)
             {
                 m_Respawner = gameObject.GetComponent<Respawner>();
             }
             return m_Respawner;
         }
     }*/

    private InputeControler m_InputControler;
    public InputeControler InputControler {
        get {

            m_InputControler = gameObject.GetComponent<InputeControler>();

            return m_InputControler;

        }
    }

    public GameObject getLocalPlayer() {
        
            GameObject[] trans = GameObject.FindGameObjectsWithTag("Player");
            for (int i = 0; i < trans.Length; i++)
                if (trans[i].GetComponent<CharakterSync>().isMine())
                {
                    return trans[i];
                }
        return null;
        
    }

    #endregion

    #region Basic handler
    public void CreateQuickGame()
    {

        PlayGamesPlatform.Instance.RealTime.CreateQuickGame(QuickGameOpponents, QuickGameOpponents,
            GameVariant, GameMenager.Instance);
    }


    public void CreateWithInvitationScreen(uint MinOpponents, uint MaxOpponents)
    {
        // Debug.Log("Game varient:" + GameVariant.ToString() + " Map:" + getMap(GameVariant).ToString());
        invite = true;
        PlayGamesPlatform.Instance.RealTime.CreateWithInvitationScreen(1, 4,

    0, GameMenager.Instance);

    }

    public void AcceptFromInbox()
    {
        PlayGamesPlatform.Instance.RealTime.AcceptFromInbox(GameMenager.Instance);
    }

    public void AcceptInvitation(string invitationId)
    {
        invite = false;
        PlayGamesPlatform.Instance.RealTime.AcceptInvitation(invitationId, GameMenager.Instance);
    }

    public void LeaveRoom(string level)
    {
        PlayGamesPlatform.Instance.RealTime.LeaveRoom();
        SceneManager.LoadScene(level);
    }

    public void EnterRoom(string level) {
        SceneManager.LoadScene(level);
    }

    #endregion

    #region Listener interface
    public void OnRoomSetupProgress(float percent)
    {
        // throw new System.NotImplementedException();
    }

    public void OnRoomConnected(bool success)
    {
        if (success)
        {
            LocalId = PlayGamesPlatform.Instance.RealTime.GetSelf().ParticipantId;

        }
        else
        {

        }
    }

    public void OnLeftRoom()
    {
        throw new System.NotImplementedException();
    }

    public void OnParticipantLeft(Participant participant)
    {
        throw new System.NotImplementedException();
    }

    public void OnPeersConnected(string[] participantIds)
    {
        throw new System.NotImplementedException();
    }

    public void OnPeersDisconnected(string[] participantIds)
    {
        throw new System.NotImplementedException();
    }

    public void OnRealTimeMessageReceived(bool isReliable, string senderId, byte[] data)
    {
        if (!senderId.Equals(LocalId)) {
            // From byte array to string
            string s = System.Text.Encoding.UTF8.GetString(data, 0, data.Length);
            Debug.Log("Receiving Packet -> " + s);
            Packet p = JsonUtility.FromJson<Packet>(s);
            if (p.cmd.Equals("Upos")) {
                GameObject obj = GameObject.Find(p.own);
                if (obj != null) {
                    obj.SendMessage("setPos", JsonUtility.FromJson<Vector3>(p.msg));
                }
            }
        }
    }
    #endregion


    public void DeclineInvitation()
    {

        PlayGamesPlatform.Instance.RealTime.DeclineInvitation(IncomingInvitation.InvitationId);

    }

    public Invitation IncomingInvitation = null;

    

    // called when an invitation is received:
    public void OnInvitationReceived(Invitation invitation, bool shouldAutoAccept)
    {
        if (shouldAutoAccept)
        {
            // Invitation should be accepted immediately. This happens if the user already
            // indicated (through the notification UI) that they wish to accept the invitation,
            // so we should not prompt again.
          //  ShowWaitScreen();
          //  PlayGamesPlatform.Instance.RealTime.AcceptInvitation(invitation.InvitationId, this);
        }
        else
        {
            // The user has not yet indicated that they want to accept this invitation.
            // We should *not* automatically accept it. Rather we store it and 
            // display an in-game popup:
            IncomingInvitation = invitation;
        }
    }

    public void ShowSelectUI()
    {
        uint maxNumToDisplay = 3;
        bool allowCreateNew = true;
        bool allowDelete = true;

        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.ShowSelectSavedGameUI("Select saved game",
            maxNumToDisplay,
            allowCreateNew,
            allowDelete,
            OnSavedGameSelected);
    }


    public void OnSavedGameSelected(SelectUIStatus status, ISavedGameMetadata game)
    {
        if (status == SelectUIStatus.SavedGameSelected)
        {
            // handle selected game save
        }
        else
        {
            // handle cancel or error
        }
    }

    public void SendToAll(string s) {
        Debug.Log("Sending Packet -> " + s);
        byte[] buffer = System.Text.Encoding.UTF8.GetBytes(s);
        if(PlayGamesPlatform.Instance!=null)
            if(PlayGamesPlatform.Instance.RealTime!=null)
        PlayGamesPlatform.Instance.RealTime.SendMessageToAll(false,buffer);
    }

    public void SendVector3(string cmd, string own, Vector3 vec) {
        Packet p = new Packet();
        p.own = own;
        p.cmd = cmd;
        p.msg = JsonUtility.ToJson(vec);
        string s = JsonUtility.ToJson(p);
        SendToAll(s);

    }

  
    /*  private Player m_localplayer;
      public Player LocalPlayer {
          get {
              return m_localplayer;
          }
          set {
              m_localplayer = value;
              if (OnLocalPlayerJoined != null)
                  OnLocalPlayerJoined(m_localplayer);
          }
      }
      
        byte[] buffer = System.Text.Encoding.UTF8.GetBytes(convert);

     */

}
[System.Serializable]
public class Packet {

    public string cmd;
    public string own;
    public string msg;
}

