﻿using System;
using System.Collections;
using UnityEngine;
using Photon.Pun.Demo.PunBasics;


/// <summary>
/// CoroutineNode.cs
/// 
/// Port of the Javascript version from 
/// http://www.unifycommunity.com/wiki/index.php?title=CoroutineScheduler
/// 
/// Linked list node type used by coroutine scheduler to track scheduling of coroutines.
///  
/// BMBF Researchproject http://playfm.htw-berlin.de
/// PlayFM - Serious Games für den IT-gestützten Wissenstransfer im Facility Management 
///	Gefördert durch das bmb+f - Programm Forschung an Fachhochschulen profUntFH
///	
///	<author>Frank.Otto@htw-berlin.de</author>
///
/// </summary>

public class CoroutineNode
{
    public CoroutineNode listPrevious = null;
    public CoroutineNode listNext = null;
    public IEnumerator fiber;
    public bool finished = false;
    public int waitForFrame = -1;
    public float waitForTime = -1.0f;
    public CoroutineNode waitForCoroutine;
    public IYieldWrapper waitForUnityObject; //lonewolfwilliams

    public CoroutineNode(IEnumerator _fiber)
    {
        this.fiber = _fiber;
    }
}

/*
 * gareth williams 
 * http://www.lonewolfwilliams.com
 */

public interface IYieldWrapper
{
    bool finished { get; }
}



public class LoginWrapper : IYieldWrapper
{
    private UnityEngine.WWW m_UnityObject;
    public string url5 = "http://wargamertable.org/StalkerBot/login.php?opcja=login";
    public string log;
    public MainMenu lun;
    public bool finished
    {
        get
        {
            if (m_UnityObject.isDone)
            {
                InfoPlayer[] player = JsonHelper.FromJson<InfoPlayer>(m_UnityObject.text);
                Debug.Log(player.Length);
                if (player.Length > 0)
                {
                   if (player[0].login == log)
                   {
                        Debug.Log("Logged in");
                        GameControler.instance.newPlayer(player[0],lun);
                      GameControler.instance.loged = true;

                   }
                }
                else
               {
                   Debug.Log("Error not logged");
                    GameControler.instance.loged = false;
                 }
            }
            return m_UnityObject.isDone;
        }
    }

    public LoginWrapper(string log, string pass,MainMenu lu)
    {
        string url2 = url5 + "&login=" + log + "&password=" + pass;
        this.log = log;
      //  Debug.Log(url2);
        lun = lu;
        m_UnityObject = new WWW(url2);
    }
}



public class NewPlayerWrapper : IYieldWrapper
{
    private UnityEngine.WWW m_UnityObject;
    public string url5 = "http://wargamertable.org/StalkerBot/login.php?opcja=new";
    public MainMenu lanch;
    public bool finished
    {
        get
        {
            if (m_UnityObject.isDone)
            {
                if (Boolean.Parse(m_UnityObject.text))
                {

                    lanch.show = true;
                    lanch.SwitchModel();
                    lanch.controlPanel.SetActive(false);

                }
                else
                {
                    lanch.Connect();
                }
            }
            return m_UnityObject.isDone;
        }
    }

    public NewPlayerWrapper(string id, MainMenu lan)
    {
        string url2 = url5 + "&id=" + id;
        lanch = lan;
       // Debug.Log(url2);
        m_UnityObject = new WWW(url2);
    }
}