﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControler : MonoBehaviour {
    public static GameControler _instance;
    [SerializeField]
    public GameControler aPrefab;

    public CoroutineScheduler sh;
    [System.Serializable]
    public class ConrollerSetting {
        public float size=1.0f;
        public float alpha = 255.0f;
        public Vector2 pos;
    }
   
    int qualityLevel;
    bool isAA;
    float render;

    GameObject activecont;

    public static GameControler instance
    {
        get
        {
            return _instance.aPrefab;
        }
    }

    public bool loged;

    void Awake()
    {
        _instance = this;
       // LoadSettings();
        sh = gameObject.AddComponent<CoroutineScheduler>();
    }

    public void LoadSettings() {
       
        qualityLevel = PlayerPrefs.GetInt("QualiyLevel");
        SetQuality(qualityLevel);
        if (!Boolean.TryParse(PlayerPrefs.GetString("isAA"),out isAA)) {
            isAA = false;
        }
       SetAA(isAA);
        render = PlayerPrefs.GetFloat("Render");
        SetRender(render);
    }


    public void setActive(GameObject act) {
        activecont = act;
    }

  

    public IEnumerator iLogin(string play,MainMenu lun)
    {
        yield return new NewPlayerWrapper(play, lun);
    }


    public void newPlayer(InfoPlayer play,MainMenu menu) {
        sh.StartCoroutine(iLogin(play.id,menu));
    }

    // Update is called once per frame
    void Update () {
        sh.UpdateAllCoroutines(Time.frameCount, Time.time);
	}


    public void SetRender(float level) {
        if (level < 0.1f)
            level = 0.1f;
        if (level > 1.0f)
            level = 1.0f;


        QualitySettings.resolutionScalingFixedDPIFactor = level * ((float)DisplayMetricsAndroid.DensityDPI/300.0f);
        render = level;
        PlayerPrefs.SetFloat("Render",render);
        PlayerPrefs.Save();
    }

    public float getRender() {

        return QualitySettings.resolutionScalingFixedDPIFactor;
    }
    public void SetQuality(float level) {
        if (level < 0)
            level = 0;
        if (level > 4)
            level = 4;
        QualitySettings.SetQualityLevel((int)level,false);
                qualityLevel = (int)level;
        PlayerPrefs.SetInt("QualiyLevel",qualityLevel);
        PlayerPrefs.Save();
    }

    public void SetAA(bool level) {
        
        if (level)
            QualitySettings.antiAliasing = 0;
        else
            QualitySettings.antiAliasing = 2;
        isAA = level;
        PlayerPrefs.SetString("isAA", isAA ? Boolean.TrueString : Boolean.FalseString);
        PlayerPrefs.Save();
    }

    public void Reset()
    {
        AndroidConnector.AppReset();
    }
}
