﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Setting: MonoBehaviour {

    public Toggle fullscreenToggle;
    public Dropdown resoluionDrop;
    public Dropdown AADrop;
    public Dropdown TextureQualityDrop;
    public Dropdown VSyncDropdown;

    public Resolution[] resolution;
    public Gamesettings gameSetings;


    public void Start()
    {
       // gameSetings = new Gamesettings();
        resolution = Screen.resolutions;

        resoluionDrop.options.Clear();
        foreach (Resolution resolutio in resolution)
        {
            resoluionDrop.options.Add(new Dropdown.OptionData(resolutio.ToString()));
        }
        LoadSettings();

    }
    private void OnEnable()
    {

        gameSetings = new Gamesettings();

        fullscreenToggle.onValueChanged.AddListener(delegate { OnFullscreenToggle(); });
        resoluionDrop.onValueChanged.AddListener(delegate { OnResolutionChange(); });
        AADrop.onValueChanged.AddListener(delegate { OnAAChange(); });
        TextureQualityDrop.onValueChanged.AddListener(delegate { OnTextureQualityChange(); });
        VSyncDropdown.onValueChanged.AddListener(delegate { OnVSyncChange(); });

        resolution = Screen.resolutions;

        resoluionDrop.options.Clear();
        foreach (Resolution resolutio in resolution) {
            resoluionDrop.options.Add(new Dropdown.OptionData(resolutio.ToString()));
        }
        LoadSettings();
    }

    public void OnFullscreenToggle() {

       gameSetings.fullscreen = Screen.fullScreen = fullscreenToggle.isOn;
    }

    public void OnResolutionChange() {

        Screen.SetResolution(resolution[resoluionDrop.value].width, resolution[resoluionDrop.value].height,Screen.fullScreen);
        gameSetings.ResolutionIndex = resoluionDrop.value;
     }

    public void OnTextureQualityChange() {
        QualitySettings.masterTextureLimit = gameSetings.TextureQuality = TextureQualityDrop.value;
       
    }

    public void OnAAChange() {
        QualitySettings.antiAliasing =  (int)Mathf.Pow(2.0f, AADrop.value);
        gameSetings.AA = AADrop.value;
      
    }

    public void OnVSyncChange() {
        QualitySettings.vSyncCount = gameSetings.VSync = VSyncDropdown.value;
    }

    public void SaveSettings() {

        string data = JsonUtility.ToJson(gameSetings,true);
        File.WriteAllText(Application.persistentDataPath + "/gamesettings.json", data);
   }

    public void LoadSettings() {
        gameSetings = JsonUtility.FromJson<Gamesettings>(File.ReadAllText(Application.persistentDataPath + "/gamesettings.json"));
      
        TextureQualityDrop.value = gameSetings.TextureQuality;
        AADrop.value = gameSetings.AA;
        VSyncDropdown.value = gameSetings.VSync;
        fullscreenToggle.isOn = gameSetings.fullscreen;
        resoluionDrop.value = gameSetings.ResolutionIndex;
      
        resoluionDrop.RefreshShownValue();
    }
}
