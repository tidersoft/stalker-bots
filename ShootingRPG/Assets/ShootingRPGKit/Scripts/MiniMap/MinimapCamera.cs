using UnityEngine;
using System.Collections;
using Photon.Pun;

public class MinimapCamera : MonoBehaviour {

    public Transform target;

IEnumerator Start (){
	if(!target){
		yield return new WaitForSeconds(0.1f);
            FindTarget();
    }

}

    void Update()
    {
        if (target == null)
        {
            FindTarget();
        }
        else { 
        if (Input.GetKeyDown(KeyCode.KeypadPlus) && GetComponent<Camera>().orthographicSize >= 20)
        {
            GetComponent<Camera>().orthographic = true;
            GetComponent<Camera>().orthographicSize -= 10;
        }
        if (Input.GetKeyDown(KeyCode.KeypadMinus) && GetComponent<Camera>().orthographicSize <= 70)
        {
            GetComponent<Camera>().orthographic = true;
            GetComponent<Camera>().orthographicSize += 10;
        }
        transform.position = new Vector3(target.position.x, transform.position.y, target.position.z);
    }
}

    public void FindTarget (){
       // yield return new WaitForSeconds(0.1f);
       if(PlayerHelper.getLocalPlayer()!=null)
        target = PlayerHelper.getLocalPlayer().transform;

	
    }
}