using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkillData : MonoBehaviour {

    public static SkillData _instance;
    [SerializeField]
    public SkillData aPrefab;

    public static SkillData instance
    {
        get
        {
            return _instance.aPrefab;
        }
    }

    void Awake()
    {
        _instance = this;
    }


    [System.Serializable]
    public enum BuffType {
        Def = 0,
        MagicDef = 1,
        MeleeDmg = 2,
        MagicPow = 3,
        GunDmg = 4,
        SelfHeal = 5,
        SelfShieldRec = 6,
        HealtRegen = 7
    }

    [System.Serializable]
    public class BuffEfect {
        public string name;
        public BuffType type;
        public int amount;
        public float dur;
       
    }

    [System.Serializable]
    public class Skil {
        public string skillName = "";
        public Texture2D icon;
        public string description = "";
        public Transform skillPrefab;
        public AnimationClip skillAnimation;
        public int manaCost = 10;
        public float castTime = 0.3f;
        public float skillDelay = 0.3f;
        public int pointCost;
        public int id;
        public bool have = false;
        public List<int> bufflist;
}
    [SerializeField]
    public Skil[] skill = new Skil[3];
    [SerializeField]
    public List<BuffEfect> Buffs = new List<BuffEfect>();
}