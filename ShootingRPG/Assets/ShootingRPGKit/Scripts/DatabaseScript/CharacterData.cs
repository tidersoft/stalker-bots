using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterData : MonoBehaviour {

    public static CharacterData _instance;

    public CharacterData aPrefab;

    public static CharacterData instance
    {
        get
        {
            return _instance.aPrefab;
        }
    }

    void Awake()
    {
        
        _instance = this;
    }


[System.Serializable]
    public class PlayerData
    {
        public string playerName = "";
        public GameObject playerPrefab;
        public GameObject characterSelectModel;
        public TextDialogue description;
        public Texture2D guiDescription;
        public int id;
        public List<AmmoType> weponFilter;
        public List<MeleType> meleFilter;
        public List<ArmorType> armorFilter;
}
    [SerializeField]
    public PlayerData[] player = new PlayerData[3];
}