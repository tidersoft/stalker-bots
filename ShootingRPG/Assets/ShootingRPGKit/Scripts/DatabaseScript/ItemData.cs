using UnityEngine;
using System.Collections;

public class ItemData : MonoBehaviour {

    [System.Serializable]
    public class Usable :Item{
        public GameObject model;
         public int hpRecover = 0;
        public int mpRecover = 0;
        public int shieldRecover = 0;
}

    public static ItemData _instance;
    [SerializeField]
    public ItemData aPrefab;

    public static ItemData instance
    {
        get
        {
            return _instance.aPrefab;
        }
    }

    void Awake()
    {
        _instance = this;
    }


    [System.Serializable]
    public class LegacyAnimSet
    {
        public AnimationClip idle;
        public AnimationClip run;
        public AnimationClip right;
        public AnimationClip left;
        public AnimationClip back;
        public AnimationClip jump;
        public AnimationClip hurt;
        public AnimationClip crouchIdle;
        public AnimationClip crouchForward;
        public AnimationClip crouchRight;
        public AnimationClip crouchLeft;
        public AnimationClip crouchBack;
}

    [System.Serializable]
    public class ModBuff{
        public ModsBuff buff;
        public float vallue;

    }

    public enum ModsBuff {

        Attack =0,
        Deffend=1,
        MAttack=2,
        MDeffend=3,
        MeleeDmg=4,
        Shield=5,
        Vinacc=6,
        Hinacc=7,
        AmmoMag=8,
        AmmoAmout=9,
        Scoup=10,
        AttackSpeed=11,
        ReloadSpeed=12


    }

    [System.Serializable]
    public class EquipMods : Item {
        [SerializeField]
        public ModBuff[] bonus = new ModBuff[2];
        public ModType modtype = ModType.Ammo;
      
    }

    [System.Serializable]

    public class Item {
        public string itemName = "";
        public Texture2D icon;
        public string description = "";
        public int price = 10;
        public int pointCost=0;
        public int id;
        public EqType equipmentType = EqType.PrimaryWeapon;

    }


    [System.Serializable]
    public class Weapon : Equip {

        //Ignore if the equipment type is not weapons
        public GameObject attackPrefab;
        public AnimationClip[] attackAnimation = new AnimationClip[1];
        public float attackAnimationSpeed = 1.0f;
        public AnimationClip reloadAnimation;
        public AnimationClip equipAnimation;
        public int maxAmmo = 30;
        public AmmoType useAmmo = AmmoType.Handgun;
        public MeleType meleType = MeleType.Knife;
        public LegacyAnimSet legacyAnimationSet; //Use for LegacyAnimation
        public GameObject gunFireEffect;
        public AudioClip gunSound;
        public float soundRadius = 0; // Can attract the enemy to gun fire position.
        public AudioClip reloadSound;
        public float attackCast = 0.18f; //For Melee
        public float attackDelay = 0.12f;
        public float cameraShakeValue = 0;
        public bool automatic = true;
        public bool animateWhileMoving = false; //Mark on If you want to play Shooting Animation while Moving.
        public Texture2D aimIcon;
        public Texture2D zoomIcon;
        public float zoomLevel = 30.0f;
        public ModType[] mods = new ModType[3];

        public GameObject muzzleFlash;

    }

    [System.Serializable]
    public class Equip : Item {
      public GameObject model;
         public int weaponAnimationType = 0; //Use for Mecanim
        public int attack = 5;
        public int defense = 0;
        public int magicAttack = 0;
        public int magicDefense = 0;
        public int meleeDamage = 0;
        public int shieldPlus = 0;
        public float Vinaccurty = 0.001f;
        public float Hinaccurty = 0.001f;
        public ArmorType armorType = ArmorType.Light;
        public bool have=false;
      
       
}
    [SerializeField]
    public Usable[] usableItem ;
    [SerializeField]
    public Equip[] equipment ;
    [SerializeField]
    public Weapon[] weapons ;
    [SerializeField]
    public EquipMods[] mods ;
    [SerializeField]
    public GameObject[] mechweapons ;


    [SerializeField]
    public Infrontery[] infloud ;
    [SerializeField]
    public MechLoud[] mechloud ;
   
}

public enum LoudotyType {
    Infrontry=0,
    Mech=1,
    Mix =2
}
[System.Serializable]
public class Infrontery : Loudout {

    public ItemData.Weapon primeryWeapon;
    
    public ItemData.Weapon seconderyWeapon;

    public ItemData.Weapon meleWeapon;

    public ItemData.Equip armor;

    public SkillData.Skil[] sklis = new SkillData.Skil[4];


}

[System.Serializable]
public class MechLoud : Loudout {

    public GameObject[] weapons;


}


public class Loudout {

    public string name;
    public LoudotyType type = LoudotyType.Infrontry;
    public int points;
    public GameObject charakter;
    public int charId;

}

public enum EqType
{
    PrimaryWeapon = 0,
    SecondaryWeapon = 1,
    MeleeWeapon = 2,
    Armor = 3,
    Usable = 4,
    Mod=5
}

public enum ModType {
    Ammo=0,
    Clip=1,
    Grip=2,
    Scope=3,
    Generator=4,
    Armor=5

}

public enum MeleType {
    Knife = 0,
    Sword = 1,
    Hammer = 2,
    Staff = 3
}

public enum ArmorType {
    Light = 0,
    Medium = 1,
    Heavy = 2,
    Composite = 3,
    Mechanic = 4
}

public enum AmmoType
{
    Handgun = 0,
    Machinegun = 1,
    Shotgun = 2,
    Magnum = 3,
    SMG = 4,
    SniperRifle = 5,
    GrenadeRounds = 6
}