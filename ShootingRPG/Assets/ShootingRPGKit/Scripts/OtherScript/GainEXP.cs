using UnityEngine;
using System.Collections;
using System;

public class GainEXP : MonoBehaviour {

public int expGain = 20;
void Start (){
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        GameObject[] ally = GameObject.FindGameObjectsWithTag("Ally");
         int i = 0;
        for (i = 0; i < players.Length; i++)
        {
            players[i].GetComponent<status>().gainEXP(expGain);
        }
        for (i = 0; i < ally.Length; i++)
        {
            ally[i].GetComponent<status>().gainEXP(expGain);
        }

        
}

}