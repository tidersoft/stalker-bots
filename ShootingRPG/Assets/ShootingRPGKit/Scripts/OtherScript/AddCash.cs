using UnityEngine;
using System.Collections;

public class AddCash : MonoBehaviour {

    public int cashMin = 10;
    public int cashMax = 50;
    public float duration = 30.0f;
private Transform master;
    public bool pernament;

    float time;
    void Start()
    {
        master = transform.root;

        time = Time.time;
        duration += Time.time;
    }
    private void Update()
    {
        if (pernament)
            return;
        
        time += Time.deltaTime;
        if (time > duration)
            Destroy(master.gameObject);

    }

    void OnTriggerEnter ( Collider other  ){
		//Pick up Item
	if (other.gameObject.tag == "Player") {
		AddCashToPlayer(other.gameObject);
     }
 }

    public void AddCashToPlayer ( GameObject other  ){
 		int gotCash= Random.Range(cashMin , cashMax);
		other.GetComponent<Inventory>().cash += gotCash;
		master = transform.root;
    	Destroy(master.gameObject);
 }
 
 
 
 

}