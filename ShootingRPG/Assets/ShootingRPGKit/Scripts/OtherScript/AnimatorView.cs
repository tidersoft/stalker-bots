﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorView : MonoBehaviour
{

    public Animation anim;

   

    
    void CrossFade(string clip) {
        anim.CrossFade(clip);
    }

    void PlayAnim(string clip)
    {
        anim.Play(clip);
    }
    
    void PlayQueued(string name, float speed) {

       anim.PlayQueued(name, QueueMode.PlayNow).speed = speed;

    }

}
