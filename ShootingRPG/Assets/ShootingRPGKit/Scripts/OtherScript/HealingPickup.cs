using UnityEngine;
using System.Collections;

public class HealingPickup : MonoBehaviour {

public int hpRecover = 20;
public int mpRecover = 0;
private Transform master;
    public float duration = 30.0f;
    public bool pernament;

    float time;
    void Start()
    {
        master = transform.root;

        time = Time.time;
        duration += Time.time;
    }
    private void Update()
    {
        if (pernament)
            return;

        time += Time.deltaTime;
        if (time > duration)
            Destroy(master.gameObject);

    }

    void OnTriggerEnter ( Collider col  ){

	if(col.tag == "Player"){
		col.GetComponent<status>().Heal(hpRecover , mpRecover);
		master = transform.root;
    	Destroy(master.gameObject);
	}
}
}