using UnityEngine;
using System.Collections;

public class IgnorePlayerCollision : MonoBehaviour {

private GameObject player;

void Start (){
	gameObject.layer = 2;
}

void Update (){
		if(!player){
				player = GameObject.FindWithTag("Player");
				if(player){
					Physics.IgnoreCollision(player.GetComponent<Collider>(), GetComponent<Collider>());
				}
		}
}
}