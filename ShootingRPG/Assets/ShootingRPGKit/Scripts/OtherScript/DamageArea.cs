using UnityEngine;
using System.Collections;

public class DamageArea : MonoBehaviour {

int damage = 50;
bool  ignoreGuard = false;

void OnTriggerEnter ( Collider other  ){
		if (other.gameObject.tag == "Player") {
			other.GetComponent<status>().OnDamage(damage , 0 , ignoreGuard,transform);
		}
 }
}