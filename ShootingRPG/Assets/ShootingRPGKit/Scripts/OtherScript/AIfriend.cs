using UnityEngine;
using System.Collections;

public class AIfriend : MonoBehaviour {


private enum AIStatef { Moving = 0, Pausing = 1 , Escape = 2 , Idle = 3, FollowMaster = 4 }

    public Transform master;

private GameObject mainModel;
    public bool useMecanim = false;
    public Animator animator; //For Mecanim
    public Transform followTarget;
    public float approachDistance = 3.0f;
public float detectRange = 15.0f;
public float lostSight = 100.0f;
public float speed = 4.0f;
    public AnimationClip movingAnimation;
    public AnimationClip idleAnimation;
    public AnimationClip[] attackAnimation = new AnimationClip[1];
    public AnimationClip hurtAnimation;
    public float inacurisy = 0.01f;
private bool  flinch = false;
    public bool stability = false;

    public bool freeze = false;

    public Transform attackPrefab;
    public Transform attackPoint;
    public GameObject attackEffect;

    public float attackCast = 0.5f;
    public float attackDelay = 1.0f;
private int continueAttack = 1;
    public float continueAttackDelay = 0.8f;

private AIStatef followState;
private float distance = 0.0f;
private float masterDistance = 0.0f;
private float atk = 0;
private float mag = 0;

private bool  cancelAttack = false;
private bool  meleefwd = false;

    public enum AIatkType {
	Immobile = 0,
	MeleeDash = 1,
}

    public AIatkType attackType = AIatkType.Immobile;

    public AudioClip[] attackVoice = new AudioClip[3];
    public AudioClip hurtVoice;
    public float attackSoundRadius = 0; // Can attract the enemy to gun fire position.

    public bool usePathfinding = false; //Require Nav Mesh Agent
/*float rayLength = 1.0f; // Use with Path Finding
float rotateSpeed = 4.0f; // Use with Path Finding*/

void Start (){
	gameObject.tag = "Ally"; 
	mainModel = GetComponent<status>().mainModel;
	if(!mainModel){
		mainModel = this.gameObject;
	}
	if(!master){
		print("Please Assign It's Master first");
	}
	
	if(!attackPoint){
		attackPoint = this.transform;
	}
	GetComponent<status>().useMecanim = useMecanim;
	
	continueAttack = attackAnimation.Length;
	atk = GetComponent<status>().atk;
	mag = GetComponent<status>().matk;
        
      	followState = AIStatef.FollowMaster;
      	
      	if(!useMecanim){
      		//If using Legacy Animation
	      	mainModel.GetComponent<Animation>().Play(movingAnimation.name);
	      	if(hurtAnimation){
	      		mainModel.GetComponent<Animation>()[hurtAnimation.name].layer = 10;
				GetComponent<status>().hurt = hurtAnimation;
	      	}
        }else{
        	//If using Mecanim Animation
        	if(!animator){
				animator = mainModel.GetComponent<Animator>();
			}
			animator.SetBool("run" , true);
        }
      	
      Physics.IgnoreCollision(GetComponent<Collider>(), master.GetComponent<Collider>());
      if(hurtVoice){
			GetComponent<status>().hurtVoice = hurtVoice;
		}
}

    public Vector3 GetDestination (){
        Vector3 destination = followTarget.position;
        destination.y = transform.position.y;
        return destination;
    }

    public Vector3 GetMasterPosition (){
    	if(!master){
    		return Vector3.zero;
    	}
        Vector3 destination = master.position;
        destination.y = transform.position.y;
        return destination;
    }

void FixedUpdate (){
	CharacterController controller = GetComponent<CharacterController>();
	status stat = GetComponent<status>();
	if(!master){
		stat.Death();
		return;
	}
	if (meleefwd && !stat.freeze){
		Vector3 lui = transform.TransformDirection(Vector3.forward);
		controller.Move(lui * 5 * Time.deltaTime);
		return;
	}
	if(freeze || stat.freeze){
		return;
	}
	if(useMecanim){
		animator.SetBool("hurt" , stat.flinch);
	}
	if (stat.flinch){
		cancelAttack = true;
            Vector3 lui = transform.TransformDirection(Vector3.back);
		controller.SimpleMove(lui * 5);
		return;
	}
	
	FindClosest();
	
		if (followState == AIStatef.FollowMaster) {
		//---------------------------------
			 if ((master.position - transform.position).magnitude <= 2.0f) {
				followState = AIStatef.Idle;
                //mainModel.animation.CrossFade(idleAnimation.name, 0.2ff);
                if(!useMecanim){
                //If using Legacy Animation
                	mainModel.GetComponent<Animation>().CrossFade(idleAnimation.name, 0.2f); 
                }else{
					animator.SetBool("run" , false);
				}
            }else {
     			if(usePathfinding){
     				PathFinding(master);
     			}else{
     				Vector3 forward= transform.TransformDirection(Vector3.forward);
     				Vector3 mas = master.position;
     				mas.y = transform.position.y;
  					transform.LookAt(mas);
     				controller.Move(forward * speed * Time.deltaTime);
     			}
     			   
  			}
		
		//---------------------------------
		}else if (followState == AIStatef.Moving) {
			masterDistance = (transform.position - GetMasterPosition()).magnitude;
            if (masterDistance > 7.0f){//////////////////GetMasterPosition
            	followState = AIStatef.FollowMaster;
            	//mainModel.animation.CrossFade(movingAnimation.name, 0.2ff);
            	if(!useMecanim){
	            //If using Legacy Animation
	                mainModel.GetComponent<Animation>().CrossFade(movingAnimation.name, 0.2f);
	            }else{
					animator.SetBool("run" , true);
				}
            }else if ((followTarget.position - transform.position).magnitude <= approachDistance) {
                followState = AIStatef.Pausing;
                //mainModel.animation.CrossFade(idleAnimation.name, 0.2ff);
                if(!useMecanim){
                //If using Legacy Animation
                	mainModel.GetComponent<Animation>().CrossFade(idleAnimation.name, 0.2f); 
                }else{
					animator.SetBool("run" , false);
				}
                //----Attack----
                if(attackPrefab){
                	Attack();
                }
            }else if ((followTarget.position - transform.position).magnitude >= lostSight)
            {//Lost Sight
            	GetComponent<status>().health = GetComponent<status>().maxHealth;
                followState = AIStatef.Idle;
                //mainModel.animation.CrossFade(idleAnimation.name, 0.2ff);
                if(!useMecanim){
                //If using Legacy Animation
                	mainModel.GetComponent<Animation>().CrossFade(idleAnimation.name, 0.2f); 
                }else{
					animator.SetBool("run" , false);
				}
            }else {
                Vector3 forward = transform.TransformDirection(Vector3.forward);
     			//controller.Move(forward * speed * Time.deltaTime);
     			if(usePathfinding && (followTarget.position - transform.position).magnitude >= 5){
     				PathFinding(followTarget);
     			}else{
     				Vector3 destiny = followTarget.position;
     				destiny.y = transform.position.y;
  					transform.LookAt(destiny);
     				controller.Move(forward * speed * Time.deltaTime);
     			}
     			   
            }
        }
        else if (followState == AIStatef.Pausing){
       			 Vector3 destinya = followTarget.position;
     			   destinya.y = transform.position.y;
  				   transform.LookAt(destinya);
  				   			   
            distance = (transform.position - GetDestination()).magnitude;
            masterDistance = (transform.position - GetMasterPosition()).magnitude;
            if (masterDistance > 12.0f){//////////////////GetMasterPosition
            	followState = AIStatef.FollowMaster;
            	//mainModel.animation.CrossFade(movingAnimation.name, 0.2ff);
            	if(!useMecanim){
	            //If using Legacy Animation
	                mainModel.GetComponent<Animation>().CrossFade(movingAnimation.name, 0.2f);
	            }else{
					animator.SetBool("run" , true);
				}
            }else if (distance > approachDistance) {
                followState = AIStatef.Moving;
                //mainModel.animation.CrossFade(movingAnimation.name, 0.2ff);
                if(!useMecanim){
	            //If using Legacy Animation
	                mainModel.GetComponent<Animation>().CrossFade(movingAnimation.name, 0.2f);
	            }else{
					animator.SetBool("run" , true);
				}
            }
        }
        //----------------Idle Mode--------------
        else if (followState == AIStatef.Idle){
  			Vector3 destinyheight = followTarget.position;
     			destinyheight.y = transform.position.y - destinyheight.y;
     		float getHealth= GetComponent<status>().maxHealth - GetComponent<status>().health;
     			
            distance = (transform.position - GetDestination()).magnitude;
            masterDistance = (transform.position - GetMasterPosition()).magnitude;
            if (distance < detectRange && Mathf.Abs(destinyheight.y) <= 4 && followTarget){
                followState = AIStatef.Moving;
                //mainModel.animation.CrossFade(movingAnimation.name, 0.2ff);
                if(!useMecanim){
	            //If using Legacy Animation
	                mainModel.GetComponent<Animation>().CrossFade(movingAnimation.name, 0.2f);
	            }else{
					animator.SetBool("run" , true);
				}
            }else if (masterDistance > 3.0f){//////////////////GetMasterPosition
            	followState = AIStatef.FollowMaster;
            	//mainModel.animation.CrossFade(movingAnimation.name, 0.2ff);
            	if(!useMecanim){
	            //If using Legacy Animation
	                mainModel.GetComponent<Animation>().CrossFade(movingAnimation.name, 0.2f);
	            }else{
					animator.SetBool("run" , true);
				}
            }
        }
//-----------------------------------
}

public IEnumerator Attack (){
	status stat = GetComponent<status>();
	atk = GetComponent<status>().atk;
	mag = GetComponent<status>().matk;
	Transform bulletShootout;
	cancelAttack = false;
	int c = 0;
	if(flinch){
		yield return false;
	}
	while (c < continueAttack && followTarget){
		freeze = true;
		if(attackType == AIatkType.MeleeDash){
				MeleeDash();
		}
		if(followTarget){
				Vector3 destiny = followTarget.position;
     			   destiny.y = transform.position.y;
  				   transform.LookAt(destiny);
  			}
  				   
		if(!useMecanim){
        	//If using Legacy Animation
			mainModel.GetComponent<Animation>().PlayQueued(attackAnimation[c].name, QueueMode.PlayNow);
		}else{
			animator.Play(attackAnimation[c].name);
		}
		
		yield return new WaitForSeconds(attackCast);
		if(flinch || stat.freeze){
			freeze = false;
			c = continueAttack;
			yield return false;
		}
		//attackPoint.transform.LookAt(followTarget);
		if(!cancelAttack || GetComponent<status>().freeze){
			if(attackVoice.Length > c && attackVoice[c]){
				GetComponent<AudioSource>().clip = attackVoice[c];
				GetComponent<AudioSource>().Play();
			}
			if(attackSoundRadius > 0){
				GunSoundRadius(attackSoundRadius);
			}
			if(attackEffect){
				GameObject eff = Instantiate(attackEffect, attackPoint.transform.position , attackPoint.transform.rotation);
				eff.transform.parent = attackPoint.transform;
			}
                Vector3 vec = new Vector3(UnityEngine.Random.Range(-inacurisy, inacurisy), UnityEngine.Random.Range(-inacurisy, inacurisy), UnityEngine.Random.Range(-inacurisy, inacurisy));
                vec += attackPoint.forward;

                bulletShootout = Instantiate(attackPrefab, attackPoint.transform.position , attackPoint.transform.rotation);
			bulletShootout.GetComponent<BulletStatus>().Setting((int)atk ,(int) mag , "Player" , this.gameObject,vec,attackPoint.forward);
			c++;
			yield return new WaitForSeconds(continueAttackDelay);
			//print(c);
			//yield return new WaitForSeconds(attackDelay);
		}else{
			freeze = false;
			c = continueAttack;
		}
	}
	yield return new WaitForSeconds(attackDelay);
		//yield return new WaitForSeconds(attackDelay);
		c = 0;
		freeze = false;
		//mainModel.animation.CrossFade(movingAnimation.name, 0.2ff);
		CheckDistance();
	
}

public void CheckDistance (){
	masterDistance = (transform.position - GetMasterPosition()).magnitude;
    if (masterDistance > 7.0f){//////////////////GetMasterPosition
            followState = AIStatef.FollowMaster;
            if(!useMecanim){
	            //If using Legacy Animation
	            mainModel.GetComponent<Animation>().CrossFade(movingAnimation.name, 0.2f);
	        }else{
				animator.SetBool("run" , true);
			}
            return;
      }
	if(!followTarget){
		if(!useMecanim){
           //If using Legacy Animation
           mainModel.GetComponent<Animation>().CrossFade(idleAnimation.name, 0.2f); 
        }else{
			animator.SetBool("run" , false);
		}
		followState = AIStatef.Idle;
		return;
	}
	float distancea = (followTarget.position - transform.position).magnitude;
	if (distancea <= approachDistance){
			Vector3 destinya = followTarget.position;
     		 destinya.y = transform.position.y;
  			 transform.LookAt(destinya);
              Attack();
          }else{
          		followState = AIStatef.Moving;
				if(!useMecanim){
		            //If using Legacy Animation
		            mainModel.GetComponent<Animation>().CrossFade(movingAnimation.name, 0.2f);
		        }else{
					animator.SetBool("run" , true);
				}
          }
}
//GameObject FindClosest (){ 
public GameObject FindClosest (){ 
    // Find Closest Player   
    GameObject[] gos; 
    gos = GameObject.FindGameObjectsWithTag("Enemy"); 
    if(gos == null){
    	return null;
    }
    GameObject closest= null; 
    
    float distance= Mathf.Infinity; 
    Vector3 position= transform.position; 

    foreach(GameObject go in gos) { 
       Vector3 diff= (go.transform.position - position); 
       float curDistance= diff.sqrMagnitude; 
       if (curDistance < distance) { 
       //------------
         closest = go; 
         distance = curDistance; 
       } 
    } 
   // target = closest;
     if(!closest){
     	followTarget = null;
   		followState = AIStatef.FollowMaster;
   		//mainModel.animation.CrossFade(movingAnimation.name, 0.2ff);
   		if(!useMecanim){
	        //If using Legacy Animation
	        mainModel.GetComponent<Animation>().CrossFade(movingAnimation.name, 0.2f);
	    }else{
			animator.SetBool("run" , true);
		}
    	return null;
    }
   followTarget = closest.transform;
    return closest; 
}

public IEnumerator  MeleeDash (){
	meleefwd = true;
	yield return new WaitForSeconds(0.2f);
	meleefwd = false;
}

void GunSoundRadius ( float radius  ){
	Collider[] hitColliders= Physics.OverlapSphere(transform.position, radius);
 		 
	for (int i = 0; i < hitColliders.Length; i++) {
		if(hitColliders[i].tag == "Enemy"){	  
	    	hitColliders[i].SendMessage("SetDestination" , transform.position);
	    }
	}
}

/*void PathFinding ( Transform target  ){
 	 CharacterController controller = GetComponent<CharacterController>();
 	 FIXME_VAR_TYPE dir= (target.position - transform.position).normalized;
 	 RaycastHit hit;
	 // check for forward raycast
	 if (Physics.Raycast(transform.position, transform.forward, hit, rayLength)){
	    //if(hit.transform != this.transform){
	    if(hit.transform.tag == "Wall"){
	      dir += hit.normal * 20; // 20 is force to repel by
	    }
	 }
	 // more raycasts   
	 FIXME_VAR_TYPE leftRay= transform.position + Vector3(-1.5f , 0, 0);
	 FIXME_VAR_TYPE rightRay= transform.position + Vector3(1.5f , 0, 0);
	 
	 // check for leftRay raycast
	 if (Physics.Raycast(leftRay, transform.forward, hit, rayLength)){
	    if(hit.transform.tag == "Wall"){
	      dir += hit.normal * 20; // 20 is force to repel by
	    }
	 }
	 
	 // check for rightRay raycast
	 if (Physics.Raycast(rightRay, transform.forward, hit, rayLength)){
	    if(hit.transform.tag == "Wall"){
	      dir += hit.normal * 20; // 20 is force to repel by
	    }
	 }
	 // rotation
	 FIXME_VAR_TYPE rot= Quaternion.LookRotation (dir);
	 rot.x = transform.rotation.x;
	 rot.z = transform.rotation.z;
	 transform.rotation = Quaternion.Slerp (transform.rotation, rot, rotateSpeed* Time.deltaTime);
	 
	 //position
	 //transform.position += transform.forward * (2 * Time.deltaTime); // 20 is speed
	 FIXME_VAR_TYPE forward= transform.TransformDirection(Vector3.forward);
     controller.Move(forward * speed * Time.deltaTime);
     			
}*/

void PathFinding ( Transform target  ){
	//Require Nav Mesh Agent.
	UnityEngine.AI.NavMeshAgent agent;
	agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
	//agent.destination = target.position; 
	agent.SetDestination (target.position);
}
    
}