using UnityEngine;
using System.Collections;

public class ForceEquip : MonoBehaviour {

ItemData.Equip equipmentId ;
int ammo = 30;
private Transform master;
private GameObject database;
private ItemData dataItem;
bool  forceSwap = true; 

void Start (){
	master = transform.root;
}

void OnTriggerEnter ( Collider other  ){
	//Pick up Item
	if (other.gameObject.tag == "Player") {
		Equip(other.gameObject);
    }
    
}

void Equip ( GameObject other  ){
	database = other.GetComponent<Inventory>().database;
	dataItem = database.GetComponent<ItemData>();
	GunTrigger gun = other.GetComponent<GunTrigger>();
	Inventory inven = other.GetComponent<Inventory>();
	ItemData.Equip tempEquipment = null;
	int tempAmmo = 0;
		
	if(equipmentId.equipmentType == EqType.PrimaryWeapon){
		tempEquipment = inven.primaryEquip;
		tempAmmo = gun.primaryWeapon.ammo;
		
		inven.EquipItem(equipmentId , inven.equipment.Length +5);
		gun.primaryWeapon.ammo = ammo;
		if(forceSwap && gun.weaponEquip != 0){
			gun.ForceSwap();
		}
		
	}else if(equipmentId.equipmentType == EqType.SecondaryWeapon){
		tempEquipment = inven.secondaryEquip;
		tempAmmo = gun.secondaryWeapon.ammo;
		
		inven.EquipItem(equipmentId , inven.equipment.Length +5);
		gun.secondaryWeapon.ammo = ammo;
		if(forceSwap && gun.weaponEquip != 1){
			gun.ForceSwap();
		}
		
	}else if(equipmentId.equipmentType == EqType.MeleeWeapon){
		tempEquipment = inven.meleeEquip;
		inven.EquipItem(equipmentId , inven.equipment.Length +5);
		
	}else{
		tempEquipment = inven.armorEquip;
		inven.EquipItem(equipmentId , inven.equipment.Length +5);
	}
	inven.AddEquipment(tempEquipment , tempAmmo);
	
    master = transform.root;
    Destroy(master.gameObject);
}

}