using UnityEngine;
using System.Collections;

public class AssassinateMission : MonoBehaviour {

    public GameObject target;
private bool  done = false;
    public string baseScene = "Base";
    public int rewardCash = 5500;
    public int missionUnlock = 4;

void Start (){
	gameObject.name = "Mission";
}

void Update (){
	if(done){
		return;
	}
	if(!target){
		SendMessage("MissionClear");
	}
}

    public IEnumerator MissionClear (){
	done = true;
	print("Clear");
	yield return new WaitForSeconds(10);
	GameObject player = GameObject.FindWithTag("Player");
	player.GetComponent<Inventory>().cash += rewardCash;
	
	int saveSlot = PlayerPrefs.GetInt("SaveSlot");
	int currentUnlock = PlayerPrefs.GetInt("MissionUnlock" +saveSlot.ToString());
	if(currentUnlock <= missionUnlock){
		PlayerPrefs.SetInt("MissionUnlock" +saveSlot.ToString(), missionUnlock);
	}
	Application.LoadLevel(baseScene);

}
    

}