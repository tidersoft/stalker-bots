using UnityEngine;
using System.Collections;

public class ExterminateMission : MonoBehaviour {

    private int currentEnemies;
    private GameObject[] enemy;
    public string enemyTag = "Enemy";
    public GUIStyle fontStyle;
    private bool finish = false;
    public string completeSendMessage = "MissionClear";
    public string baseScene = "Base";
    public int rewardCash = 1000;
    public int missionUnlock = 1;

    void Start() {
        gameObject.name = "Mission";
        enemy = GameObject.FindGameObjectsWithTag(enemyTag);
    }

    void Update() {
        if (finish) {
            return;
        }
        enemy = GameObject.FindGameObjectsWithTag(enemyTag);
        currentEnemies = enemy.Length;
        if (enemy.Length <= 0) {
            //Clear
            if (completeSendMessage != "") {
                SendMessage(completeSendMessage);
            }
            finish = true;
        }

    }
    public void MissionClear() {
        StartCoroutine(Clear());
    }

    public IEnumerator Clear (){
	print("Clear");
	yield return new WaitForSeconds(10);
	GameObject[] player = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < player.Length; i++)
        {
         //   player[i].GetComponent<Inventory>().cash += rewardCash;
            if(player[i]!=null)
            player[i].GetComponent<SaveLoad>().LeftRoom();
        }
            int saveSlot = PlayerPrefs.GetInt("SaveSlot");
            int currentUnlock = PlayerPrefs.GetInt("MissionUnlock" + saveSlot.ToString());
            if (currentUnlock <= missionUnlock)
            {
                PlayerPrefs.SetInt("MissionUnlock" + saveSlot.ToString(), missionUnlock);
            }
            
        
}

void OnGUI (){
	GUI.Label ( new Rect(Screen.width - 160, Screen.height /2 - 175, 150, 50), "Enemies", fontStyle);
	GUI.Label ( new Rect(Screen.width - 130, Screen.height /2 - 150 , 150, 50), currentEnemies.ToString(), fontStyle);
}
}