using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi.Multiplayer;
using System.Collections.Generic;


public class MissionSelect : MonoBehaviour {
    [System.Serializable]
    public class MissionDetails
    {
        public string missionName = "";
        public int unlockTier = 0;
        public string missionScene = "";
}

    public bool showServers = false;

    public AudioSource aS;

    bool createRoom = false;

    Vector2 scroll = Vector2.zero;
    Vector2 mapScroll = Vector2.zero;
    public string roomName = "Room 01";

    [SerializeField]
    public mapItem[] maps;
    mapItem mapToLoad;


    [SerializeField]
    public MissionDetails[] mission = new MissionDetails[4];
private bool  showGui = false;
private int saveSlot = 0;
private int currentUnlock = 0;
    public GUISkin skin;


    void Start (){
	saveSlot = PlayerPrefs.GetInt("SaveSlot");
	currentUnlock = PlayerPrefs.GetInt("MissionUnlock" +saveSlot.ToString());
        SaveUtil.SaveGame();
        rooms = new List<Room>();
        
}

void Update (){

}

   


void OnGUI (){
        GUI.skin = skin;
        float skala = Screen.height / 600 * GameControler.instance.getRender();
        if (GameMenager.Instance.IncomingInvitation != null)
        {
            Invitation mIncomingInvitation = GameMenager.Instance.IncomingInvitation;

            GUILayout.BeginArea(new Rect(Screen.width / 2  - (250*skala), Screen.height / 2 - (125*skala),  500*skala, 250*skala));
            GUILayout.BeginVertical("Window");

            // show the popup
            string who = (mIncomingInvitation.Inviter != null &&
                mIncomingInvitation.Inviter.DisplayName != null) ?
                    mIncomingInvitation.Inviter.DisplayName : "Someone";
            GUILayout.Label(who + " invite you for battle!", "Label");
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Accept!", GUILayout.Width(100*skala)))
            {
                // user wants to accept the invitation!
                //  ShowWaitScreen();
                GameMenager.Instance.AcceptInvitation(GameMenager.Instance.IncomingInvitation.InvitationId);
               
            }
            if (GUILayout.Button("Decline", GUILayout.Width(100*skala)))
            {
                // user wants to decline the invitation
                //PlayGamesPlatform.Instance.RealTime.DeclineInvitation(
                GameMenager.Instance.DeclineInvitation();
            }
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndArea();
        }
        
        if (showGui)
        {
           
                GUILayout.BeginArea(new Rect(Screen.width / 2 - (250*skala), 125*skala, 500*skala, 700*skala));
                GUILayout.BeginVertical("Window");
               /* if (!createRoom)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.BeginVertical();
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Server Browser:", "Label");
                    if (GUILayout.Button("X", GUILayout.Width(45)))
                    {
                        OnOffMenu();
                    }
                    GUILayout.EndHorizontal();
                    scroll = GUILayout.BeginScrollView(scroll);
                    if (rooms.Count>0)
                    {
                        foreach (Room room in rooms)
                        {
                            GUILayout.BeginHorizontal("Box");
                        GUILayout.Label(room.name);// +" "+maps[room.mission].MissionName+" Max Players:"+maps[room.mission].maxPlayer);
                            if (GUILayout.Button("Join", GUILayout.Width(100)))
                            {

                            GameMenager.Instance.AcceptInvitation(room.inviteId);
                            //PlayGamesPlatform.Instance.RealTime.AcceptInvitation(room.inviteId, GameMenager.Instance);
                            //   PhotonNetwork.playerName = "[" + curRank.abbreviation + "] " + playerName;
                            //    StartMission(room.CustomProperties["Mision"]as string, room.MaxPlayers, room.name);
                            }
                            GUILayout.EndHorizontal();
                        }
                    }
                    else
                    {
                        GUILayout.Label("No Rooms Found...");
                    }
                    GUILayout.EndScrollView();

                    GUILayout.BeginHorizontal();
                    roomName = GUILayout.TextField(roomName);
                    if (GUILayout.Button("Create", GUILayout.Width(100)))
                    {
                        createRoom = true;
                    }
                    if (GUILayout.Button("Scan", GUILayout.Width(100)))
                    {
                            ScanRooms();
                    }
                GUILayout.EndHorizontal();
                    GUILayout.EndVertical();
                    GUILayout.EndHorizontal();


                }
                else
                {*/
                    GUILayout.BeginVertical();
                    GUILayout.Label("Creating Room");
                    mapScroll = GUILayout.BeginScrollView(mapScroll, GUILayout.Height((325*skala)));
                    GUILayout.BeginHorizontal();
               
                    foreach (mapItem map in maps)
                    {
                        
                        GUILayout.BeginVertical("Window");
                        GUILayout.Label(map.levelToLoad);
                        GUILayout.Label(map.icon, GUILayout.Width(300*skala), GUILayout.Height(150*skala));
                        GUILayout.Label(map.size + " | Kill Limit: " + map.scoreLimit);
                        if (GUILayout.Button("Choose"))
                        {
                    // mapToLoad = map;
                    StartMission(map.levelToLoad,4,getID());
                        }
                        GUILayout.EndVertical();
                 
                    }
                    GUILayout.EndHorizontal();
                    GUILayout.EndScrollView();
                  
                 
                 
                    GUILayout.BeginHorizontal();
                  
                if (GUILayout.Button("Cancel", GUILayout.Height(35*skala)))
                    {
                      //  showGui = false;
                        OnOffMenu();
                    }

               if (GUILayout.Button("Invite", GUILayout.Height(35*skala)))
               {
                                    
                      GameMenager.Instance.CreateWithInvitationScreen(0, 4);
                //   StartCoroutine(createRooms("mission="+mapToLoad.levelToLoad+"&max_player="+mapToLoad.maxPlayer+"&name="+roomName));
                // StartMission(mapToLoad.levelToLoad, mapToLoad.maxPlayer, roomName);

               }
                  GUILayout.EndHorizontal();
                  GUILayout.EndVertical();
                //}
                GUILayout.EndVertical();
                GUILayout.EndArea();

        }

        

}
      public string getID() {
        var stringChars = new char[30];
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        System.Random random = new System.Random();

        for (int i = 0; i < stringChars.Length; i++)
        {
            stringChars[i] = chars[random.Next(chars.Length)];
        }

        return  new String(stringChars);
    }

   

void StartMission ( string leveln,byte maxP,string roomN ){
	showGui = false;
        /*	Time.timeScale = 1.0f;
                 photonConnect con = GameObject.FindObjectOfType<photonConnect>();
                 con.levelName = leveln;
                 roomN = getID();
                 Debug.Log(roomN);
                 con.roomName = roomN;
                 con.MaxPlayers = maxP;
                 Debug.Log(con.roomName);
                 PhotonNetwork.LeaveRoom();

              */
        GameMenager.Instance.EnterRoom(leveln);
      //  GameManager.instance.ChangeRoom(leveln,roomN);
        
}

void OnOffMenu (){
	//Freeze Time Scale to 0 if Window is Showing
	if(!showGui && Time.timeScale != 0.0f){
			showGui = true;
          //  ScanRooms();
            //Time.timeScale = 0.0f;
           PlayerHelper.getLocalPlayer().GetComponent<status>().setEnable(false);
        }
        else if(showGui){
			showGui = false;
            //Time.timeScale = 1.0f;
            PlayerHelper.getLocalPlayer().GetComponent<status>().setEnable(true);
        }
}
    [Serializable]
    public class Room {
        public string name;
        public int mission;
        public string inviteId;
        

    }

    public List<Room> rooms ;
    bool scan = false;
    public void ScanRooms() {
       
        getRooms();
    }

    void getRooms()
    {
        
       
            PlayGamesPlatform.Instance.RealTime.GetAllInvitations(
      (invites) =>
      {
          Debug.Log("Got " + invites.Length + " invites");
          string logMessage = "";
          rooms = new List<Room>();
        
          foreach (Invitation invite in invites)
          {
             
                  logMessage += " " + invite.InvitationId + " (" +
                      invite.InvitationType + ") from " +
                      invite.Inviter + " " + invite.Variant + " \n";

                  Room rom = new Room();
                  
                  rom.name = invite.Inviter.Player.userName;
                  rom.mission = invite.Variant;
                  rom.inviteId = invite.InvitationId;
                  rooms.Add(rom);

             
          }
          Debug.Log(logMessage);
      });
          
        
    }

    bool createroom = false;
    public string url2 = "http://wargamertable.org/StalkerBot/quary.php?opcja=createRoom&team=red&enemy=creep";
    IEnumerator createRooms(string param)
    {
        createroom = false;
          using (WWW www = new WWW(url2+"&"+ param))
        {
            yield return www;
            createroom = Boolean.Parse(www.text);
            Debug.Log(www.text);
            if(createroom)
                StartMission(mapToLoad.levelToLoad, mapToLoad.maxPlayer, roomName);
        }
      
    }
    public string url3 = "http://wargamertable.org/StalkerBot/quary.php?opcja=joinRoom";
    IEnumerator joinRooms(string param)
    {
        bool joinroom = false;
        using (WWW www = new WWW(url3 + "&" + param))
        {
            yield return www;
            joinroom = Boolean.Parse(www.text);
            Debug.Log(www.text);
            if (joinroom)
                StartMission(mapToLoad.levelToLoad, mapToLoad.maxPlayer, roomName);
        }

    }


}

public static class PlayerHelper {



    public static GameObject getLocalPlayer() {
        GameObject[] players= GameObject.FindGameObjectsWithTag("Player");

        for (int i = 0; i < players.Length; i++) {
            if (players[i].GetComponent<CharakterSync>().isMine())
                return players[i];
        }

        return null;
    }

}

[System.Serializable]
public class InfoPlayer {
    public string login;
    public string room;
    public string team;
    public string id;
}


public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}