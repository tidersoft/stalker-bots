using UnityEngine;
using System.Collections;

public class ExtractionPoint : MonoBehaviour {

bool  waitAlly = false;
private bool  playerEnter = false;
private bool  allyEnter = false;
GameObject missionObj;
string completeSendMessage = "MissionClear";
private bool  done = false;

void Start (){
	if(!missionObj){
		missionObj = GameObject.Find("Mission");
	}
}

void Update (){
	if(done){
		return;
	}
	
	if(waitAlly && playerEnter && allyEnter){
		ClearMission();
	}else if(!waitAlly && playerEnter){
		ClearMission();
	}

}

void ClearMission (){
	done = true;
	if(completeSendMessage != ""){
		missionObj.SendMessage(completeSendMessage);
	}
}

void OnTriggerEnter ( Collider other  ){
	if(other.tag == "Player"){
		playerEnter = true;
	}
	if(other.tag == "Ally"){
		allyEnter = true;
	}

}

void OnTriggerExit ( Collider other  ){
	if(other.tag == "Player"){
		playerEnter = false;
	}
	if(other.tag == "Ally"){
		allyEnter = false;
	}

}

}