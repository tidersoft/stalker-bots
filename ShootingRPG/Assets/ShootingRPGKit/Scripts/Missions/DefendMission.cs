using UnityEngine;
using System.Collections;

public class DefendMission : MonoBehaviour {

    public GameObject defendTarget;
    public OperatorSet[] failMessage = new OperatorSet[1];
private bool  done = false;
    public string baseScene = "Base";
    public int rewardCash = 4000;
    public int missionUnlock = 3;

void Start (){
	gameObject.name = "Mission";
}

void Update (){
	if(done){
		return;
	}
	if(!defendTarget){
		//MissionFail();
		SendMessage("MissionFail");
	}
}

    public IEnumerator MissionFail (){
	done = true;
	GetComponent<Operator>().otherMessage = failMessage;
	GetComponent<Operator>().ShowOtherMessage();
	yield return new WaitForSeconds(10);
	GameObject player = GameObject.FindWithTag("Player");
	if(player){
		Application.LoadLevel(baseScene);
	}
}

    public IEnumerator MissionClear (){
	done = true;
	print("Clear");
	yield return new WaitForSeconds(10);
	GameObject player = GameObject.FindWithTag("Player");
	player.GetComponent<Inventory>().cash += rewardCash;
	
	int saveSlot = PlayerPrefs.GetInt("SaveSlot");
	int currentUnlock = PlayerPrefs.GetInt("MissionUnlock" +saveSlot.ToString());
	if(currentUnlock <= missionUnlock){
		PlayerPrefs.SetInt("MissionUnlock" +saveSlot.ToString(), missionUnlock);
	}
	Application.LoadLevel(baseScene);

}
    

}