using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class SaveLoad : MonoBehaviour {

bool  autoLoad = false;
private GameObject player;
private bool  menu = false;
private Vector3 lastPosition;
private Transform mainCam;
    string goScene = null;
    int players;
    string roomname;
    private int saveSlot = 0;

void Start (){
	if(!player){
    	player = GameObject.FindWithTag ("Player");
    }
    saveSlot = PlayerPrefs.GetInt("SaveSlot");
    GetComponent<status>().characterName = PlayerPrefs.GetString("Name" +saveSlot.ToString());
    //If PlayerPrefs Loadgame = 10 That mean You Start with Load Game Menu.
	//If You Set Autoload = true It will LoadGame when you start.
     if(PlayerPrefs.GetInt("Loadgame") == 10 || autoLoad){
   		 LoadGame();
   		 if(!autoLoad){
   			 //If You didn't Set autoLoad then reset PlayerPrefs Loadgame to 0 after LoadGame.
   		 	PlayerPrefs.SetInt("Loadgame", 0);
   		 }
    }
        goScene = null;

}

void Update (){
        if (!GetComponent<CharakterSync>().isMine() )
            return;

        if (Input.GetKeyUp(KeyCode.Escape)) {
		//Open Save Load Menu
		OnOffMenu();
	}

}

void OnOffMenu (){
	//Freeze Time Scale to 0 if Window is Showing
	if(!menu ){
			menu = true;
            //	Time.timeScale = 0.0f;
            GetComponent<status>().setEnable(false);
		//	Screen.lockCursor = false;
	}else if(menu){
			menu = false;
			//Time.timeScale = 1.0f;
		//	Screen.lockCursor = true;

            GetComponent<status>().setEnable(true);
        }
}

void OnGUI (){
        float scala = (float)Screen.height / (float)600;

        if (menu){
		GUI.Box ( new Rect(Screen.width / 2 - 180*scala,190 * scala, 360 * scala, 340 * scala), "Menu");
		if (GUI.Button ( new Rect(Screen.width / 2 - 110 * scala, 265 * scala, 220 * scala, 100 * scala), "Settings")) {
		//	SaveData();
			OnOffMenu();
		}
		
		/*if (GUI.Button ( new Rect(Screen.width / 2 - 110,355,220,80), "Load Game")) {
			LoadData();
			OnOffMenu();
		}*/
		
		if (GUI.Button ( new Rect(Screen.width / 2 - 110 * scala, 405 * scala, 220 * scala, 100 * scala), "Quit Game")) {
                //GameObject cam = GameObject.FindWithTag ("MainCamera");
                //Destroy(cam);
                //Destroy(player);
                //Time.timeScale = 1.0f;
                //Application.LoadLevel ("Title");
                if (SceneManager.GetActiveScene().name.Equals("Base")){

                    GameMenager.Instance.LeaveRoom("Title");
                }
                else {
                    LeftRoom();
                }
            }

            if (GUI.Button ( new Rect(Screen.width / 2 + 110 * scala, 195 * scala, 60 * scala, 60 * scala), "X")) {
			OnOffMenu();
		}
	}

}


    public void LeftRoom() {

        goScene = "Base";
        roomname = "Base";

        GameMenager.Instance.LeaveRoom(roomname);
        //    StartCoroutine(leftRooms("name=" + PhotonNetwork.CurrentRoom.Name));

    }
    void SaveData (){
			PlayerPrefs.SetInt("PreviousSave" +saveSlot.ToString(), 10);
			PlayerPrefs.SetString("Name" +saveSlot.ToString(), player.GetComponent<status>().characterName);
			PlayerPrefs.SetFloat("PlayerX", player.transform.position.x);
			PlayerPrefs.SetFloat("PlayerY", player.transform.position.y);
			PlayerPrefs.SetFloat("PlayerZ", player.transform.position.z);
			PlayerPrefs.SetInt("PlayerLevel" +saveSlot.ToString(), player.GetComponent<status>().level);
			PlayerPrefs.SetInt("PlayerID" +saveSlot.ToString(), player.GetComponent<status>().playerId);
			
			PlayerPrefs.SetInt("PlayerATK" +saveSlot.ToString(), player.GetComponent<status>().atk);
			PlayerPrefs.SetInt("PlayerDEF" +saveSlot.ToString(), player.GetComponent<status>().def);
			PlayerPrefs.SetInt("PlayerMATK" +saveSlot.ToString(), player.GetComponent<status>().matk);
			PlayerPrefs.SetInt("PlayerMDEF" +saveSlot.ToString(), player.GetComponent<status>().mdef);
			PlayerPrefs.SetInt("PlayerMelee" +saveSlot.ToString(), player.GetComponent<status>().melee);
			PlayerPrefs.SetInt("PlayerEXP" +saveSlot.ToString(), player.GetComponent<status>().exp);
			PlayerPrefs.SetInt("PlayerMaxEXP" +saveSlot.ToString(), player.GetComponent<status>().maxExp);
			PlayerPrefs.SetInt("PlayerMaxHP" +saveSlot.ToString(), player.GetComponent<status>().maxHealth);
			//PlayerPrefs.Set("PlayerHP" +saveSlot.ToString(), player.GetComponent<status>().health);
			//PlayerPrefs.SetInt("PlayerMaxMP" +saveSlot.ToString(), player.GetComponent<status>().maxMana);
		//	PlayerPrefs.SetInt("PlayerMP", player.GetComponent<Status>().mana);
			PlayerPrefs.SetInt("PlayerMaxShield" +saveSlot.ToString(), player.GetComponent<status>().maxShield);
			PlayerPrefs.SetInt("PlayerSTP" +saveSlot.ToString(), player.GetComponent<status>().statusPoint);
			
			PlayerPrefs.SetInt("Cash" +saveSlot.ToString(), player.GetComponent<Inventory>().cash);
			int itemSize = player.GetComponent<Inventory>().itemSlot.Length;
			int a = 0;
			/*if(itemSize > 0){
				while(a < itemSize){
					PlayerPrefs.SetInt("Item" + a.ToString() +saveSlot.ToString(), player.GetComponent<Inventory>().itemSlot[a]);
					PlayerPrefs.SetInt("ItemQty" + a.ToString() +saveSlot.ToString(), player.GetComponent<Inventory>().itemQuantity[a]);
					a++;
				}
			}
			
			int equipSize = player.GetComponent<Inventory>().equipment.Length;
			a = 0;
			if(equipSize > 0){
				while(a < equipSize){
					PlayerPrefs.SetInt("Equipm" + a.ToString() +saveSlot.ToString(), player.GetComponent<Inventory>().equipment[a]);
					PlayerPrefs.SetInt("EquipAmmo" + a.ToString() +saveSlot.ToString(), player.GetComponent<Inventory>().equipAmmo[a]);
					a++;
				}
			}
			PlayerPrefs.SetInt("PrimaryEquip" +saveSlot.ToString(), player.GetComponent<Inventory>().primaryEquip);
			PlayerPrefs.SetInt("SecondaryEquip" +saveSlot.ToString(), player.GetComponent<Inventory>().secondaryEquip);
			PlayerPrefs.SetInt("MeleeEquip" +saveSlot.ToString(), player.GetComponent<Inventory>().meleeEquip);
			PlayerPrefs.SetInt("ArmoEquip" +saveSlot.ToString(), player.GetComponent<Inventory>().armorEquip);
			
			PlayerPrefs.SetInt("PrimaryAmmo" +saveSlot.ToString(), player.GetComponent<GunTrigger>().primaryWeapon.ammo);
			PlayerPrefs.SetInt("SecondaryAmmo" +saveSlot.ToString(), player.GetComponent<GunTrigger>().secondaryWeapon.ammo);
			//Save Skill Slot
			a = 0;
				while(a < player.GetComponent<SkillWindow>().skill.Length){
					PlayerPrefs.SetInt("Skill" + a.ToString() +saveSlot.ToString(), player.GetComponent<SkillWindow>().skill[a]);
					a++;
			}*/
			//Skill List Slot
			a = 0;
			while(a < player.GetComponent<SkillWindow>().skillListSlot.Length){
				PlayerPrefs.SetInt("SkillList" + a.ToString() +saveSlot.ToString(), player.GetComponent<SkillWindow>().skillListSlot[a]);
				a++;
			}
			
			//Save Ammo
			PlayerPrefs.SetInt("HandgunAmmo" +saveSlot.ToString(), player.GetComponent<GunTrigger>().allAmmo.handgunAmmo);
			PlayerPrefs.SetInt("MachinegunAmmo" +saveSlot.ToString(), player.GetComponent<GunTrigger>().allAmmo.machinegunAmmo);
			PlayerPrefs.SetInt("ShotgunAmmo" +saveSlot.ToString(), player.GetComponent<GunTrigger>().allAmmo.shotgunAmmo);
			PlayerPrefs.SetInt("MagnumAmmo" +saveSlot.ToString(), player.GetComponent<GunTrigger>().allAmmo.magnumAmmo);
			PlayerPrefs.SetInt("SmgAmmo" +saveSlot.ToString(), player.GetComponent<GunTrigger>().allAmmo.smgAmmo);
			PlayerPrefs.SetInt("SniperRifleAmmo" +saveSlot.ToString(), player.GetComponent<GunTrigger>().allAmmo.sniperRifleAmmo);
			PlayerPrefs.SetInt("GrenadeRounds" +saveSlot.ToString(), player.GetComponent<GunTrigger>().allAmmo.grenadeRounds);
			
			print("Saved");
}
void LoadData (){
		//oldPlayer = GameObject.FindWithTag ("Player");
		GameObject respawn = GameObject.FindWithTag ("Player");
		
		respawn.GetComponent<status>().characterName = PlayerPrefs.GetString("Name" +saveSlot.ToString());
		lastPosition.x = PlayerPrefs.GetFloat("PlayerX");
		lastPosition.y = PlayerPrefs.GetFloat("PlayerY");
		lastPosition.z = PlayerPrefs.GetFloat("PlayerZ");
		respawn.transform.position = lastPosition;
		//GameObject respawn = Instantiate(player, lastPosition , transform.rotation);
		respawn.GetComponent<status>().level = PlayerPrefs.GetInt("PlayerLevel" +saveSlot.ToString());
		respawn.GetComponent<status>().playerId = PlayerPrefs.GetInt("PlayerID" +saveSlot.ToString());
		respawn.GetComponent<status>().atk = PlayerPrefs.GetInt("PlayerATK" +saveSlot.ToString());
		respawn.GetComponent<status>().def = PlayerPrefs.GetInt("PlayerDEF" +saveSlot.ToString());
		respawn.GetComponent<status>().matk = PlayerPrefs.GetInt("PlayerMATK" +saveSlot.ToString());
		respawn.GetComponent<status>().mdef = PlayerPrefs.GetInt("PlayerMDEF" +saveSlot.ToString());
		respawn.GetComponent<status>().mdef = PlayerPrefs.GetInt("PlayerMDEF" +saveSlot.ToString());
		respawn.GetComponent<status>().exp = PlayerPrefs.GetInt("PlayerEXP" +saveSlot.ToString());
		respawn.GetComponent<status>().maxExp = PlayerPrefs.GetInt("PlayerMaxEXP" +saveSlot.ToString());
		respawn.GetComponent<status>().maxHealth = PlayerPrefs.GetInt("PlayerMaxHP" +saveSlot.ToString());
		respawn.GetComponent<status>().health = PlayerPrefs.GetInt("PlayerHP" +saveSlot.ToString());
		//respawn.GetComponent<Status>().health = PlayerPrefs.GetInt("PlayerMaxHP");
		respawn.GetComponent<status>().maxMana = PlayerPrefs.GetInt("PlayerMaxMP" +saveSlot.ToString());
		respawn.GetComponent<status>().mana = PlayerPrefs.GetInt("PlayerMaxMP" +saveSlot.ToString());
		respawn.GetComponent<status>().statusPoint = PlayerPrefs.GetInt("PlayerSTP" +saveSlot.ToString());
		respawn.GetComponent<status>().maxShield = PlayerPrefs.GetInt("PlayerMaxShield" +saveSlot.ToString());
		respawn.GetComponent<status>().melee = PlayerPrefs.GetInt("PlayerMelee" +saveSlot.ToString());
		
		mainCam = GameObject.FindWithTag ("MainCamera").transform;
		//mainCam.GetComponent<ARPGcamera>().target = respawn.transform;
		//-------------------------------
		respawn.GetComponent<Inventory>().cash = PlayerPrefs.GetInt("Cash" +saveSlot.ToString());
		int itemSize = player.GetComponent<Inventory>().itemSlot.Length;
			int a = 0;
	/*		if(itemSize > 0){
				while(a < itemSize){
					respawn.GetComponent<Inventory>().itemSlot[a] = PlayerPrefs.GetInt("Item" + a.ToString() +saveSlot.ToString());
					respawn.GetComponent<Inventory>().itemQuantity[a] = PlayerPrefs.GetInt("ItemQty" + a.ToString() +saveSlot.ToString());
					//-------
					a++;
				}
			}
			
			int equipSize = player.GetComponent<Inventory>().equipment.Length;
			a = 0;
			if(equipSize > 0){
				while(a < equipSize){
					respawn.GetComponent<Inventory>().equipment[a] = PlayerPrefs.GetInt("Equipm" + a.ToString() +saveSlot.ToString());
					respawn.GetComponent<Inventory>().equipAmmo[a] = PlayerPrefs.GetInt("EquipAmmo" + a.ToString() +saveSlot.ToString());
					a++;
				}
			}
			respawn.GetComponent<Inventory>().primaryEquip = 0;
			respawn.GetComponent<Inventory>().secondaryEquip = 0;
			respawn.GetComponent<Inventory>().meleeEquip = 0;
			respawn.GetComponent<Inventory>().armorEquip = PlayerPrefs.GetInt("ArmoEquip" +saveSlot.ToString());
		
		respawn.GetComponent<Inventory>().cancelAssign = true;
		if(PlayerPrefs.GetInt("PrimaryEquip" +saveSlot.ToString()) > 0){
			respawn.GetComponent<Inventory>().EquipItem(PlayerPrefs.GetInt("PrimaryEquip" +saveSlot.ToString()) , respawn.GetComponent<Inventory>().equipment.Length + 5);
			respawn.GetComponent<GunTrigger>().primaryWeapon.ammo = PlayerPrefs.GetInt("PrimaryAmmo" +saveSlot.ToString());
			
		}
		if(PlayerPrefs.GetInt("SecondaryEquip" +saveSlot.ToString()) > 0){
			respawn.GetComponent<Inventory>().EquipItem(PlayerPrefs.GetInt("SecondaryEquip" +saveSlot.ToString()) , respawn.GetComponent<Inventory>().equipment.Length + 5);
			respawn.GetComponent<GunTrigger>().secondaryWeapon.ammo = PlayerPrefs.GetInt("SecondaryAmmo" +saveSlot.ToString());
		}
		if(PlayerPrefs.GetInt("MeleeEquip" +saveSlot.ToString()) > 0){
			respawn.GetComponent<Inventory>().EquipItem(PlayerPrefs.GetInt("MeleeEquip" +saveSlot.ToString()) , respawn.GetComponent<Inventory>().equipment.Length + 5);
		}
			respawn.GetComponent<Inventory>().RemoveWeaponMesh();*/
			//----------------------------------
		//Screen.lockCursor = true;
		
		 GameObject[] mon; 
  		 mon = GameObject.FindGameObjectsWithTag("Enemy"); 
  			 foreach(GameObject mo in mon) { 
  			 	if(mo){
  			 		mo.GetComponent<AIenemy>().followTarget = respawn.transform;
  			 	}
  			 }
			
			//Load Skill Slot
			a = 0;
			while(a < player.GetComponent<SkillWindow>().skill.Length){
	//			respawn.GetComponent<SkillWindow>().skill[a] = PlayerPrefs.GetInt("Skill" + a.ToString() +saveSlot.ToString());
				a++;
			}
			//Skill List Slot
			a = 0;
			while(a < player.GetComponent<SkillWindow>().skillListSlot.Length){
				player.GetComponent<SkillWindow>().skillListSlot[a] = PlayerPrefs.GetInt("SkillList" + a.ToString() +saveSlot.ToString());
				a++;
			}
			respawn.GetComponent<SkillWindow>().AssignAllSkill();
		//---------------Set Target to Minimap--------------
  		GameObject minimap = GameObject.FindWithTag("Minimap");
  		if(minimap){
  			GameObject mapcam = minimap.GetComponent<MinimapOnOff>().minimapCam;
  			mapcam.GetComponent<MinimapCamera>().target = respawn.transform;
  		}
  		//Load Ammo
		respawn.GetComponent<GunTrigger>().allAmmo.handgunAmmo = PlayerPrefs.GetInt("HandgunAmmo" +saveSlot.ToString());
		respawn.GetComponent<GunTrigger>().allAmmo.machinegunAmmo = PlayerPrefs.GetInt("MachinegunAmmo" +saveSlot.ToString());
		respawn.GetComponent<GunTrigger>().allAmmo.shotgunAmmo = PlayerPrefs.GetInt("ShotgunAmmo" +saveSlot.ToString());
		respawn.GetComponent<GunTrigger>().allAmmo.magnumAmmo = PlayerPrefs.GetInt("MagnumAmmo" +saveSlot.ToString());
		respawn.GetComponent<GunTrigger>().allAmmo.smgAmmo = PlayerPrefs.GetInt("SmgAmmo" +saveSlot.ToString());
		respawn.GetComponent<GunTrigger>().allAmmo.sniperRifleAmmo = PlayerPrefs.GetInt("SniperRifleAmmo" +saveSlot.ToString());
		respawn.GetComponent<GunTrigger>().allAmmo.grenadeRounds = PlayerPrefs.GetInt("GrenadeRounds" +saveSlot.ToString());
		
		player = GameObject.FindWithTag ("Player");
}

//Function LoadGame is unlike the Function LoadData.
//This Function will not spawn new Player.
void LoadGame (){
		if(!player){
	    	player = GameObject.FindWithTag ("Player");
	    }
		player.GetComponent<status>().characterName = PlayerPrefs.GetString("Name" +saveSlot.ToString());
		player.GetComponent<status>().level = PlayerPrefs.GetInt("PlayerLevel" +saveSlot.ToString());
		player.GetComponent<status>().playerId = PlayerPrefs.GetInt("PlayerID" +saveSlot.ToString());
		player.GetComponent<status>().atk = PlayerPrefs.GetInt("PlayerATK" +saveSlot.ToString());
		player.GetComponent<status>().def = PlayerPrefs.GetInt("PlayerDEF" +saveSlot.ToString());
		player.GetComponent<status>().matk = PlayerPrefs.GetInt("PlayerMATK" +saveSlot.ToString());
		player.GetComponent<status>().mdef = PlayerPrefs.GetInt("PlayerMDEF" +saveSlot.ToString());
		player.GetComponent<status>().mdef = PlayerPrefs.GetInt("PlayerMDEF" +saveSlot.ToString());
		player.GetComponent<status>().exp = PlayerPrefs.GetInt("PlayerEXP" +saveSlot.ToString());
		player.GetComponent<status>().maxExp = PlayerPrefs.GetInt("PlayerMaxEXP" +saveSlot.ToString());
		player.GetComponent<status>().maxHealth = PlayerPrefs.GetInt("PlayerMaxHP" +saveSlot.ToString());
		player.GetComponent<status>().health = PlayerPrefs.GetInt("PlayerMaxHP" +saveSlot.ToString());
		player.GetComponent<status>().maxMana = PlayerPrefs.GetInt("PlayerMaxMP" +saveSlot.ToString());
		player.GetComponent<status>().mana = PlayerPrefs.GetInt("PlayerMaxMP" +saveSlot.ToString());	
		player.GetComponent<status>().statusPoint = PlayerPrefs.GetInt("PlayerSTP" +saveSlot.ToString());
		player.GetComponent<status>().maxShield = PlayerPrefs.GetInt("PlayerMaxShield" +saveSlot.ToString());
		player.GetComponent<status>().melee = PlayerPrefs.GetInt("PlayerMelee" +saveSlot.ToString());
		//-------------------------------
		player.GetComponent<Inventory>().cash = PlayerPrefs.GetInt("Cash" +saveSlot.ToString());
		int itemSize = player.GetComponent<Inventory>().itemSlot.Length;
			int a = 0;
		/*	if(itemSize > 0){
				while(a < itemSize){
					player.GetComponent<Inventory>().itemSlot[a] = PlayerPrefs.GetInt("Item" + a.ToString() +saveSlot.ToString());
					player.GetComponent<Inventory>().itemQuantity[a] = PlayerPrefs.GetInt("ItemQty" + a.ToString() +saveSlot.ToString());
					//-------
					a++;
				}
			}
			
			int equipSize = player.GetComponent<Inventory>().equipment.Length;
			a = 0;
			if(equipSize > 0){
				while(a < equipSize){
					player.GetComponent<Inventory>().equipment[a] = PlayerPrefs.GetInt("Equipm" + a.ToString() +saveSlot.ToString());
					player.GetComponent<Inventory>().equipAmmo[a] = PlayerPrefs.GetInt("EquipAmmo" + a.ToString() +saveSlot.ToString());
					a++;
				}
			}
			player.GetComponent<Inventory>().primaryEquip = 0;
			player.GetComponent<Inventory>().secondaryEquip = 0;
			player.GetComponent<Inventory>().meleeEquip = 0;
			player.GetComponent<Inventory>().armorEquip = PlayerPrefs.GetInt("ArmoEquip" +saveSlot.ToString());
		
		player.GetComponent<Inventory>().cancelAssign = true;
		if(PlayerPrefs.GetInt("PrimaryEquip" +saveSlot.ToString()) > 0){
			player.GetComponent<Inventory>().EquipItem(PlayerPrefs.GetInt("PrimaryEquip" +saveSlot.ToString()) , player.GetComponent<Inventory>().equipment.Length + 5);
			player.GetComponent<GunTrigger>().primaryWeapon.ammo = PlayerPrefs.GetInt("PrimaryAmmo" +saveSlot.ToString());
		}
		if(PlayerPrefs.GetInt("SecondaryEquip" +saveSlot.ToString()) > 0){
			player.GetComponent<Inventory>().EquipItem(PlayerPrefs.GetInt("SecondaryEquip" +saveSlot.ToString()) , player.GetComponent<Inventory>().equipment.Length + 5);
			player.GetComponent<GunTrigger>().secondaryWeapon.ammo = PlayerPrefs.GetInt("SecondaryAmmo" +saveSlot.ToString());
		}
		if(PlayerPrefs.GetInt("MeleeEquip" +saveSlot.ToString()) > 0){
			player.GetComponent<Inventory>().EquipItem(PlayerPrefs.GetInt("MeleeEquip" +saveSlot.ToString()) , player.GetComponent<Inventory>().equipment.Length + 5);
		}
			//----------------------------------*/
		//Screen.lockCursor = true;
		
		 GameObject[] mon; 
  		 mon = GameObject.FindGameObjectsWithTag("Enemy"); 
  			 foreach(GameObject mo in mon) { 
  			 	if(mo){
  			 		mo.GetComponent<AIenemy>().followTarget = player.transform;
  			 	}
  			 }
  		
			//Load Skill Slot
			a = 0;
			while(a < player.GetComponent<SkillWindow>().skill.Length){
			//	player.GetComponent<SkillWindow>().skill[a] = PlayerPrefs.GetInt("Skill" + a.ToString() +saveSlot.ToString());
				a++;
			}
			//Skill List Slot
			a = 0;
			while(a < player.GetComponent<SkillWindow>().skillListSlot.Length){
				player.GetComponent<SkillWindow>().skillListSlot[a] = PlayerPrefs.GetInt("SkillList" + a.ToString() +saveSlot.ToString());
				a++;
			}
			player.GetComponent<SkillWindow>().AssignAllSkill();
			
		//Load Ammo
		player.GetComponent<GunTrigger>().allAmmo.handgunAmmo = PlayerPrefs.GetInt("HandgunAmmo" +saveSlot.ToString());
		player.GetComponent<GunTrigger>().allAmmo.machinegunAmmo = PlayerPrefs.GetInt("MachinegunAmmo" +saveSlot.ToString());
		player.GetComponent<GunTrigger>().allAmmo.shotgunAmmo = PlayerPrefs.GetInt("ShotgunAmmo" +saveSlot.ToString());
		player.GetComponent<GunTrigger>().allAmmo.magnumAmmo = PlayerPrefs.GetInt("MagnumAmmo" +saveSlot.ToString());
		player.GetComponent<GunTrigger>().allAmmo.smgAmmo = PlayerPrefs.GetInt("SmgAmmo" +saveSlot.ToString());
		player.GetComponent<GunTrigger>().allAmmo.sniperRifleAmmo = PlayerPrefs.GetInt("SniperRifleAmmo" +saveSlot.ToString());
		player.GetComponent<GunTrigger>().allAmmo.grenadeRounds = PlayerPrefs.GetInt("GrenadeRounds" +saveSlot.ToString());

		print("Loaded");
	
}

   public void OnLeftRoom()
    {
        if (goScene != null || leftroom)
        {
          
                
            
        }
    }
    bool leftroom = false;
    public string url2 = "http://wargamertable.org/StalkerBot/quary.php?opcja=deleteRoom";
    public string url3 = "http://wargamertable.org/StalkerBot/quary.php?opcja=leaveRoom";
    IEnumerator leftRooms(string param)
    {
        leftroom = false;
        players -= 1;
        if (players < 1)
        {
            Debug.Log(param);
            using (WWW www = new WWW(url2 + "&" + param))
            {
                yield return www;
                leftroom = Boolean.Parse(www.text);
                Debug.Log(www.text);
                if (leftroom)
                {
                   
                }
            }
        }
        else {

            Debug.Log(param);
            using (WWW www = new WWW(url3 + "&" + param))
            {
                yield return www;
                leftroom = Boolean.Parse(www.text);
                Debug.Log(www.text);
                if (leftroom)
                {
                   
                }
            }
            
        }

    }
}