using UnityEngine;
using System.Collections;
using Photon.Pun;
using System.Collections.Generic;

public class GunTrigger : MonoBehaviour {

public GameObject mainModel;
    public bool useMecanim = true;
    public Transform attackPoint;

    public GameObject mainCamPrefab;
private float nextFire = 0.0f;
private bool  reloading = false;
private bool  meleefwd = false;
    public Texture2D aimIcon;
private Texture2D zoomIcon;
private status stat;
[HideInInspector]
    public int weaponEquip = 0;

[HideInInspector]
public bool attacking = false;
[HideInInspector]
public int c = 0;

[HideInInspector]
public GameObject mainCam;

private int str = 0;
private int matk = 0;

    public Transform weaponPosition; //Position of Your Weapon

    public GameObject primaryWeaponModel; //Assign Model of your Primary Weapon in your Main Model.
    public GameObject secondaryWeaponModel; //Assign Model of your Secondary Weapon in your Main Model.
    public GameObject meleeWeaponModel; //Assign Model of your Primary Weapon in your Main Model.
    [System.Serializable]
    public class WeaponSet
    {
        public GameObject hitPrefab;
        public Transform weaponAtkPoint;
        public AnimationClip shootAnimation;
        public float shootAnimationSpeed = 1.0f;
	public bool animateWhileMoving = false; //Mark on If you want to play Shooting Animation while Moving.
        public AnimationClip reloadAnimation;
        public GameObject gunFireEffect;
        public AudioClip gunSound;
        public float soundRadius = 0; // Can attract the enemy to gun fire position.
        public AudioClip reloadSound;
        public int ammo = 30;
        public int maxAmmo = 30;
        public AmmoType useAmmo = AmmoType.Handgun;
        public float attackDelay = 0.15f;
        public AnimationClip equipAnimation;
        public float cameraShakeValue = 0;
        public bool automatic = true;
        public Texture2D aimIcon;
        public Texture2D zoomIcon;
        public float zoomLevel = 30.0f;
        public float Vinacurisy = 0.015f;
        public float Hinacurisy = 0.015f;
}
    [SerializeField]
    public WeaponSet primaryWeapon;
    [SerializeField]
    public WeaponSet secondaryWeapon;
    [System.Serializable]
    public class MeleeSet
    {
        public bool canMelee = false;
        public GameObject meleePrefab;
        public AnimationClip[] meleeAnimation = new AnimationClip[3];
        public float meleeAnimationSpeed = 1.0f;
        public float meleeCast = 0.15f;
        public float meleeDelay = 0.15f;
        public AudioClip meleeSound;
}
    [SerializeField]
    public MeleeSet meleeAttack;
    [System.Serializable]
    public class AllAmmo {
        public int handgunAmmo = 0;
        public int machinegunAmmo = 0;
        public int shotgunAmmo = 0;
        public int magnumAmmo = 0;
        public int smgAmmo = 0;
        public int sniperRifleAmmo = 0;
        public int grenadeRounds = 0;
}
    [SerializeField]
    public AllAmmo allAmmo;
    [System.Serializable]
    public class SkilAtk {
        public Texture2D icon;
        public Transform skillPrefab;
        public AnimationClip skillAnimation;
        public float skillAnimationSpeed = 1.0f;
	public float castTime = 0.3f;
        public float skillDelay = 0.3f;
        public int manaCost = 10;
        public int id;
}
    [SerializeField]
    public SkilAtk[] skill = new SkilAtk[4];
    [System.Serializable]
    public class AtkSound
    {
        public AudioClip[] attackComboVoice = new AudioClip[3];
        public AudioClip magicCastVoice;
        public AudioClip hurtVoice;
}
    [SerializeField]
    public AtkSound sound;
    public GUIStyle statusFont;
    public GUIStyle ammoFont;

    public Texture2D backGroundBar;

private float zoomLevel = 30.0f;
public bool  onAiming = false;
private bool  automatic = true;
private bool  freeze = false;
private int spareAmmo = 30;

   
    private int m_LastFrameShot = -10;
    public GameObject muzzleFlash;

    void Start (){
	gameObject.tag = "Player";
	if(!attackPoint){
		GameObject n = new GameObject();
		n.transform.parent = this.transform;
		attackPoint = n.transform;
	}
	stat = GetComponent<status>();
	
	if(!mainModel){
		mainModel = stat.mainModel;
	}
      //  GetComponent<Inventory>().Setup(0);


    stat.useMecanim = useMecanim;
	stat.CalculateStatus();
	
	
    
    if(sound.hurtVoice){
		stat.hurtVoice = sound.hurtVoice;
	}
	SettingWeapon();

}

    public void createCamera() {
        GameObject[] oldcam = GameObject.FindGameObjectsWithTag("MainCamera");
        foreach (GameObject o in oldcam)
        {
            Destroy(o);
        }
        GameObject newCam = Instantiate(mainCamPrefab, transform.position, transform.rotation);
        mainCam = newCam;
    }

    void LateUpdate()
    {
        if (m_LastFrameShot == Time.frameCount)
        {
            if (muzzleFlash != null)
            {
                muzzleFlash.transform.rotation = Quaternion.AngleAxis(Random.value * 360, Vector3.forward);
                muzzleFlash.SetActive(true);
            }
        }
        else
        {
            if (muzzleFlash != null)
            {
                muzzleFlash.SetActive(false);
            }
        }
    }

    void Update (){


        if (!GetComponent<CharakterSync>().isMine())
            return;

        //Release Zoom
        if(onAiming && !GameMenager.Instance.InputControler.Scope){
        Camera.main.fieldOfView = 60;
       	onAiming = false;
    }
	if(stat.freeze || Time.timeScale == 0.0f || freeze || !stat.Enable){
		return;
	}
	CharacterController controller = GetComponent<CharacterController>();
	if (stat.flinch){
		Vector3 knock = transform.TransformDirection(Vector3.back);
		controller.Move(knock * 8 *Time.deltaTime);
		return;
	}
	if (meleefwd){
		Vector3 lui = transform.TransformDirection(Vector3.forward);
		controller.Move(lui * 5 * Time.deltaTime);
	}
	
	if(automatic){
		if (GameMenager.Instance.InputControler.Fire1 && Time.time > nextFire && !reloading && !attacking) {
			if(weaponEquip == 0){
				PrimaryAttack();
			}else{
				SecondaryAttack();
			}
		}
	}
	if(!automatic){
		if (GameMenager.Instance.InputControler.Fire1 && Time.time > nextFire && !reloading && !attacking) {
			if(weaponEquip == 0){
				PrimaryAttack();
			}else{
				SecondaryAttack();
			}
		}
	}
	if(GameMenager.Instance.InputControler.Reload)
        {
		//Reload();
		StartCoroutine(Reload()); 
	}
	Aiming();
	
	//Melee
	if (GameMenager.Instance.InputControler.Mele && Time.time > nextFire && !attacking && meleeAttack.canMelee) {
		if (Time.time > nextFire) {
			if(Time.time > (nextFire + 0.5f)){
				c = 0;
			}
			//Attack Combo
			if(meleeAttack.meleeAnimation.Length >= 1){
			StartCoroutine(	MeleeCombo());
			}
		}
	}
	
	//Zoom
	if(GameMenager.Instance.InputControler.Scope){
       	Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, zoomLevel, Time.deltaTime * 8);
       	onAiming = true;
           // Debug.Log("Scope");
    }
		
	//Magic
	if(GameMenager.Instance.InputControler.Skill1 && !attacking && skill[0].skillPrefab){
	StartCoroutine(	MagicSkill(0));
	}
	if(GameMenager.Instance.InputControler.Skill2 && !attacking && skill[1].skillPrefab){
            StartCoroutine(MagicSkill(1));
	}
	if(GameMenager.Instance.InputControler.Skill3 && !attacking && skill[2].skillPrefab){
	StartCoroutine(	MagicSkill(2));
	}
	if(GameMenager.Instance.InputControler.Skill4 && !attacking && skill[3].skillPrefab){
	StartCoroutine(	MagicSkill(3));
	}
	
	//Switch Weapon
	if (GameMenager.Instance.InputControler.changeWeapon && Time.time > nextFire && !reloading) {
    ////////       GetComponent<PhotonView>().RPC("ForceSwap", RpcTarget.All);
    StartCoroutine(	SwapWeapon());
	}
    
    //Primary Weapon Ammo
    if(weaponEquip == 0 && primaryWeapon.useAmmo == AmmoType.Handgun){
    	spareAmmo = allAmmo.handgunAmmo;
    }
    if(weaponEquip == 0 && primaryWeapon.useAmmo == AmmoType.Machinegun){
    	spareAmmo = allAmmo.machinegunAmmo;
    }
    if(weaponEquip == 0 && primaryWeapon.useAmmo == AmmoType.Shotgun){
    	spareAmmo = allAmmo.shotgunAmmo;
    }
    if(weaponEquip == 0 && primaryWeapon.useAmmo == AmmoType.Magnum){
    	spareAmmo = allAmmo.magnumAmmo;
    }
    if(weaponEquip == 0 && primaryWeapon.useAmmo == AmmoType.SMG){
    	spareAmmo = allAmmo.smgAmmo;
    }
    if(weaponEquip == 0 && primaryWeapon.useAmmo == AmmoType.SniperRifle){
    	spareAmmo = allAmmo.sniperRifleAmmo;
    }
    if(weaponEquip == 0 && primaryWeapon.useAmmo == AmmoType.GrenadeRounds){
    	spareAmmo = allAmmo.grenadeRounds;
    }
    //Secondary Weapon Ammo
    if(weaponEquip == 1 && secondaryWeapon.useAmmo == AmmoType.Handgun){
    	spareAmmo = allAmmo.handgunAmmo;
    }
    if(weaponEquip == 1 && secondaryWeapon.useAmmo == AmmoType.Machinegun){
    	spareAmmo = allAmmo.machinegunAmmo;
    }
    if(weaponEquip == 1 && secondaryWeapon.useAmmo == AmmoType.Shotgun){
    	spareAmmo = allAmmo.shotgunAmmo;
    }
    if(weaponEquip == 1 && secondaryWeapon.useAmmo == AmmoType.Magnum){
    	spareAmmo = allAmmo.magnumAmmo;
    }
    if(weaponEquip == 1 && secondaryWeapon.useAmmo == AmmoType.SMG){
    	spareAmmo = allAmmo.smgAmmo;
    }
    if(weaponEquip == 1 && secondaryWeapon.useAmmo == AmmoType.SniperRifle){
    	spareAmmo = allAmmo.sniperRifleAmmo;
    }
    if(weaponEquip == 1 && secondaryWeapon.useAmmo == AmmoType.GrenadeRounds){
    	spareAmmo = allAmmo.grenadeRounds;
    }
}

    public float getScopeForce() {
        return (60.0f / (float)zoomLevel)*(60.0f/(float)zoomLevel);
    }

    public void PrimaryAttack (){
//	if(!primaryWeapon.bulletPrefab){
//	return;
//	}
	if(primaryWeapon.ammo <= 0){
		//Reload();
		StartCoroutine(Reload());
		return;
	}
	str = stat.addAtk;
	matk = stat.addMatk;
	nextFire = Time.time + primaryWeapon.attackDelay;
	c = 0;
	//Shooting Animation
	if(primaryWeapon.shootAnimation && primaryWeapon.animateWhileMoving || primaryWeapon.shootAnimation && !primaryWeapon.animateWhileMoving && !Input.GetButton("Horizontal") && !Input.GetButton("Vertical")){
		if(!useMecanim){
			//For Legacy Animation
			mainModel.GetComponent<Animation>()[primaryWeapon.shootAnimation.name].layer = 15;
		//	mainModel.GetComponent<Animation>().PlayQueued(primaryWeapon.shootAnimation.name, QueueMode.PlayNow).speed = primaryWeapon.shootAnimationSpeed;
               // GetComponent<PhotonView>().RPC("PlayQueued", RpcTarget.All, primaryWeapon.shootAnimation.name, primaryWeapon.shootAnimationSpeed);

            }
            else
            {
			//For Mecanim Animation
			GetComponent<PlayerMecanimAnimation>().AttackAnimation(primaryWeapon.shootAnimation.name);
		}
	}
	if(primaryWeapon.cameraShakeValue > 0){
		Camera.main.GetComponent<ThirdPersonCamera>().Shake(primaryWeapon.cameraShakeValue , 0.05f);
	}
	if(primaryWeapon.soundRadius > 0){
		GunSoundRadius(primaryWeapon.soundRadius);
	}
	
	if(primaryWeapon.gunFireEffect){
		GameObject eff = Instantiate(primaryWeapon.gunFireEffect, primaryWeapon.weaponAtkPoint.transform.position , primaryWeapon.weaponAtkPoint.transform.rotation);
		eff.transform.parent = primaryWeapon.weaponAtkPoint.transform;
            eff.transform.localPosition = eff.transform.forward * 0.1f;

        }
	if(primaryWeapon.gunSound){
		GetComponent<AudioSource>().PlayOneShot(primaryWeapon.gunSound);

	}
       Vector3 vec = attackPoint.forward;
        vec.x += Random.Range(-primaryWeapon.Hinacurisy, primaryWeapon.Hinacurisy);
        vec.z += Random.Range(-primaryWeapon.Hinacurisy, primaryWeapon.Hinacurisy);
        vec.y += Random.Range(-primaryWeapon.Vinacurisy, primaryWeapon.Vinacurisy);

        GameObject bulletShootout = Instantiate(primaryWeapon.hitPrefab, attackPoint.position, attackPoint.rotation);
        if(bulletShootout.GetComponent<BulletStatus>()!=null)
        bulletShootout.GetComponent<BulletStatus>().Setting(str , matk , "Player" , this.gameObject,vec,attackPoint.forward);
        else
        bulletShootout.GetComponent<BulletColider>().Setting(str, matk, "Player", this.gameObject, vec, attackPoint.up);
        m_LastFrameShot = Time.frameCount;
        primaryWeapon.ammo--;
}

    public void SecondaryAttack (){
//	if(!secondaryWeapon.bulletPrefab){
//		return;
//	}
	if(secondaryWeapon.ammo <= 0){
		//Reload();
		StartCoroutine(Reload());
		return;
	}
	str = stat.addAtk;
	matk = stat.addMatk;
	nextFire = Time.time + secondaryWeapon.attackDelay;
	c = 0;
	//Shooting Animation
	if(secondaryWeapon.shootAnimation  && secondaryWeapon.animateWhileMoving || secondaryWeapon.shootAnimation && !secondaryWeapon.animateWhileMoving && !Input.GetButton("Horizontal") && !Input.GetButton("Vertical")){
		if(!useMecanim){
			//For Legacy Animation
			mainModel.GetComponent<Animation>()[secondaryWeapon.shootAnimation.name].layer = 15;
                //mainModel.GetComponent<Animation>().PlayQueued(secondaryWeapon.shootAnimation.name, QueueMode.PlayNow).speed = secondaryWeapon.shootAnimationSpeed;
                //GetComponent<PhotonView>().RPC("PlayQueued", RpcTarget.All, secondaryWeapon.shootAnimation.name, secondaryWeapon.shootAnimationSpeed);

            }
            else
            {
			//For Mecanim Animation
			GetComponent<PlayerMecanimAnimation>().AttackAnimation(secondaryWeapon.shootAnimation.name);
		}
	}
	if(secondaryWeapon.cameraShakeValue > 0){
		Camera.main.GetComponent<ThirdPersonCamera>().Shake(secondaryWeapon.cameraShakeValue , 0.1f);
	}
	if(secondaryWeapon.soundRadius > 0){
		GunSoundRadius(secondaryWeapon.soundRadius);
	}
	
	if(secondaryWeapon.gunFireEffect){
		GameObject eff = Instantiate(secondaryWeapon.gunFireEffect, secondaryWeapon.weaponAtkPoint.transform.position , secondaryWeapon.weaponAtkPoint.transform.rotation);
		eff.transform.parent = secondaryWeapon.weaponAtkPoint.transform;
            eff.transform.localPosition = Vector3.forward*0.25f ;
	}
        if (secondaryWeapon.gunSound)
        {
            GetComponent<AudioSource>().PlayOneShot(secondaryWeapon.gunSound);
        }
            Vector3 vec = attackPoint.forward;
        vec.x += Random.Range(-secondaryWeapon.Hinacurisy, secondaryWeapon.Hinacurisy);
        vec.z += Random.Range(-secondaryWeapon.Hinacurisy, secondaryWeapon.Hinacurisy);
        vec.y += Random.Range(-secondaryWeapon.Vinacurisy, secondaryWeapon.Vinacurisy);
            GameObject bulletShootout = Instantiate(secondaryWeapon.hitPrefab, attackPoint.position, attackPoint.rotation);
        if (bulletShootout.GetComponent<BulletStatus>() != null)
            bulletShootout.GetComponent<BulletStatus>().Setting(str, matk, "Player", this.gameObject, vec,attackPoint.forward);
        else
            bulletShootout.GetComponent<BulletColider>().Setting(str, matk, "Player", this.gameObject, vec,attackPoint.up);
        secondaryWeapon.ammo--;
        m_LastFrameShot = Time.frameCount;
    }

void GunSoundRadius ( float radius  ){
	Collider[] hitColliders= Physics.OverlapSphere(transform.position, radius);
 		 
	for (int i = 0; i < hitColliders.Length; i++) {
		if(hitColliders[i].tag == "Enemy"){	  
	    	hitColliders[i].SendMessage("SetDestination" , transform.position);
	    }
	}
}

    public IEnumerator Reload (){
        if (reloading || primaryWeapon.ammo >= primaryWeapon.maxAmmo && weaponEquip == 0 || secondaryWeapon.ammo >= secondaryWeapon.maxAmmo && weaponEquip == 1 || spareAmmo <= 0)
        {
            yield return false;
        }
        else
        {
            float wait = 0;

            reloading = true;
            PlayerMecanimAnimation mecanim = GetComponent<PlayerMecanimAnimation>();
            if (weaponEquip == 0)
            { //Primary Weapon
                if (primaryWeapon.reloadSound)
                {
                    GetComponent<AudioSource>().PlayOneShot(primaryWeapon.reloadSound);
                }
                if (useMecanim)
                {
                    //Mecanim
                    mecanim.animator.SetLayerWeight(mecanim.upperBodyLayer, 1);
                    mecanim.AttackAnimation(primaryWeapon.reloadAnimation.name);
                    AnimatorClipInfo[] clip = mecanim.animator.GetCurrentAnimatorClipInfo(0);
                    wait = clip.Length;
                }
                else
                {
                    //Legacy
                    mainModel.GetComponent<Animation>()[primaryWeapon.reloadAnimation.name].layer = 6;
                    mainModel.GetComponent<Animation>().Play(primaryWeapon.reloadAnimation.name);
                    wait = mainModel.GetComponent<Animation>()[primaryWeapon.reloadAnimation.name].length;
                }
                yield return new WaitForSeconds(wait);
            }
            else
            {
                if (secondaryWeapon.reloadSound)
                {
                    GetComponent<AudioSource>().PlayOneShot(secondaryWeapon.reloadSound);
                }
                if (useMecanim)
                {
                    //Mecanim
                    mecanim.animator.SetLayerWeight(mecanim.upperBodyLayer, 1);
                    mecanim.AttackAnimation(secondaryWeapon.reloadAnimation.name);
                    AnimatorClipInfo[] clip = mecanim.animator.GetCurrentAnimatorClipInfo(0);
                    wait = clip.Length;
                }
                else
                {
                    //Legacy
                    mainModel.GetComponent<Animation>()[secondaryWeapon.reloadAnimation.name].layer = 6;
                    mainModel.GetComponent<Animation>().Play(secondaryWeapon.reloadAnimation.name);
                    wait = mainModel.GetComponent<Animation>()[secondaryWeapon.reloadAnimation.name].length;
                }
                yield return new WaitForSeconds(wait);
            }
            if (useMecanim)
            {
                mecanim.animator.SetLayerWeight(mecanim.upperBodyLayer, 0);
            }
            ResetAmmo();
            reloading = false;
        }
}

public void ResetAmmo (){
	//Primary Weapon Ammo
	if(weaponEquip == 0 && primaryWeapon.useAmmo == AmmoType.Handgun){
		allAmmo.handgunAmmo += primaryWeapon.ammo;
		if(allAmmo.handgunAmmo >= primaryWeapon.maxAmmo){
			primaryWeapon.ammo = primaryWeapon.maxAmmo;
			allAmmo.handgunAmmo -= primaryWeapon.maxAmmo;
		}else if(allAmmo.handgunAmmo < primaryWeapon.maxAmmo){
			primaryWeapon.ammo = allAmmo.handgunAmmo;
			allAmmo.handgunAmmo = 0;
		}
	}
    if(weaponEquip == 0 && primaryWeapon.useAmmo == AmmoType.Machinegun){
    	allAmmo.machinegunAmmo += primaryWeapon.ammo;
		if(allAmmo.machinegunAmmo >= primaryWeapon.maxAmmo){
			primaryWeapon.ammo = primaryWeapon.maxAmmo;
			allAmmo.machinegunAmmo -= primaryWeapon.maxAmmo;
		}else if(allAmmo.machinegunAmmo < primaryWeapon.maxAmmo){
			primaryWeapon.ammo = allAmmo.machinegunAmmo;
			allAmmo.machinegunAmmo = 0;
		}
    }
    if(weaponEquip == 0 && primaryWeapon.useAmmo == AmmoType.Shotgun){
    	allAmmo.shotgunAmmo += primaryWeapon.ammo;
		if(allAmmo.shotgunAmmo >= primaryWeapon.maxAmmo){
			primaryWeapon.ammo = primaryWeapon.maxAmmo;
			allAmmo.shotgunAmmo -= primaryWeapon.maxAmmo;
		}else if(allAmmo.shotgunAmmo < primaryWeapon.maxAmmo){
			primaryWeapon.ammo = allAmmo.shotgunAmmo;
			allAmmo.shotgunAmmo = 0;
		}
    }
    if(weaponEquip == 0 && primaryWeapon.useAmmo == AmmoType.Magnum){
    	allAmmo.magnumAmmo += primaryWeapon.ammo;
    	if(allAmmo.magnumAmmo >= primaryWeapon.maxAmmo){
			primaryWeapon.ammo = primaryWeapon.maxAmmo;
			allAmmo.magnumAmmo -= primaryWeapon.maxAmmo;
		}else if(allAmmo.magnumAmmo < primaryWeapon.maxAmmo){
			primaryWeapon.ammo = allAmmo.magnumAmmo;
			allAmmo.magnumAmmo = 0;
		}
    }
    if(weaponEquip == 0 && primaryWeapon.useAmmo == AmmoType.SMG){
    	allAmmo.smgAmmo += primaryWeapon.ammo;
    	if(allAmmo.smgAmmo >= primaryWeapon.maxAmmo){
			primaryWeapon.ammo = primaryWeapon.maxAmmo;
			allAmmo.smgAmmo -= primaryWeapon.maxAmmo;
		}else if(allAmmo.smgAmmo < primaryWeapon.maxAmmo){
			primaryWeapon.ammo = allAmmo.smgAmmo;
			allAmmo.smgAmmo = 0;
		}
    }
    if(weaponEquip == 0 && primaryWeapon.useAmmo == AmmoType.SniperRifle){
    	allAmmo.sniperRifleAmmo += primaryWeapon.ammo;
    	if(allAmmo.sniperRifleAmmo >= primaryWeapon.maxAmmo){
			primaryWeapon.ammo = primaryWeapon.maxAmmo;
			allAmmo.sniperRifleAmmo -= primaryWeapon.maxAmmo;
		}else if(allAmmo.sniperRifleAmmo < primaryWeapon.maxAmmo){
			primaryWeapon.ammo = allAmmo.sniperRifleAmmo;
			allAmmo.sniperRifleAmmo = 0;
		}
    }
    if(weaponEquip == 0 && primaryWeapon.useAmmo == AmmoType.GrenadeRounds){
    	allAmmo.grenadeRounds += primaryWeapon.ammo;
    	if(allAmmo.grenadeRounds >= primaryWeapon.maxAmmo){
			primaryWeapon.ammo = primaryWeapon.maxAmmo;
			allAmmo.grenadeRounds -= primaryWeapon.maxAmmo;
		}else if(allAmmo.grenadeRounds < primaryWeapon.maxAmmo){
			primaryWeapon.ammo = allAmmo.grenadeRounds;
			allAmmo.grenadeRounds = 0;
		}
    }
    //Secondary Weapon Ammo
    if(weaponEquip == 1 && secondaryWeapon.useAmmo == AmmoType.Handgun){
    	allAmmo.handgunAmmo += secondaryWeapon.ammo;
    	if(allAmmo.handgunAmmo >= secondaryWeapon.maxAmmo){
			secondaryWeapon.ammo = secondaryWeapon.maxAmmo;
			allAmmo.handgunAmmo -= secondaryWeapon.maxAmmo;
		}else if(allAmmo.handgunAmmo < secondaryWeapon.maxAmmo){
			secondaryWeapon.ammo = allAmmo.handgunAmmo;
			allAmmo.handgunAmmo = 0;
		}
    }
    if(weaponEquip == 1 && secondaryWeapon.useAmmo == AmmoType.Machinegun){
    	allAmmo.machinegunAmmo += secondaryWeapon.ammo;
    	if(allAmmo.machinegunAmmo >= secondaryWeapon.maxAmmo){
			secondaryWeapon.ammo = secondaryWeapon.maxAmmo;
			allAmmo.machinegunAmmo -= secondaryWeapon.maxAmmo;
		}else if(allAmmo.machinegunAmmo < secondaryWeapon.maxAmmo){
			secondaryWeapon.ammo = allAmmo.machinegunAmmo;
			allAmmo.machinegunAmmo = 0;
		}
    }
    if(weaponEquip == 1 && secondaryWeapon.useAmmo == AmmoType.Shotgun){
    	allAmmo.shotgunAmmo += secondaryWeapon.ammo;
    	if(allAmmo.shotgunAmmo >= secondaryWeapon.maxAmmo){
			secondaryWeapon.ammo = secondaryWeapon.maxAmmo;
			allAmmo.shotgunAmmo -= secondaryWeapon.maxAmmo;
		}else if(allAmmo.shotgunAmmo < secondaryWeapon.maxAmmo){
			secondaryWeapon.ammo = allAmmo.shotgunAmmo;
			allAmmo.shotgunAmmo = 0;
		}
    }
    if(weaponEquip == 1 && secondaryWeapon.useAmmo == AmmoType.Magnum){
    	allAmmo.magnumAmmo += secondaryWeapon.ammo;
    	if(allAmmo.magnumAmmo >= secondaryWeapon.maxAmmo){
			secondaryWeapon.ammo = secondaryWeapon.maxAmmo;
			allAmmo.magnumAmmo -= secondaryWeapon.maxAmmo;
		}else if(allAmmo.magnumAmmo < secondaryWeapon.maxAmmo){
			secondaryWeapon.ammo = allAmmo.magnumAmmo;
			allAmmo.magnumAmmo = 0;
		}
    }
    if(weaponEquip == 1 && secondaryWeapon.useAmmo == AmmoType.SMG){
    	allAmmo.smgAmmo += secondaryWeapon.ammo;
    	if(allAmmo.smgAmmo >= secondaryWeapon.maxAmmo){
			secondaryWeapon.ammo = secondaryWeapon.maxAmmo;
			allAmmo.smgAmmo -= secondaryWeapon.maxAmmo;
		}else if(allAmmo.smgAmmo < secondaryWeapon.maxAmmo){
			secondaryWeapon.ammo = allAmmo.smgAmmo;
			allAmmo.smgAmmo = 0;
		}
    }
    if(weaponEquip == 1 && secondaryWeapon.useAmmo == AmmoType.SniperRifle){
    	allAmmo.sniperRifleAmmo += secondaryWeapon.ammo;
    	if(allAmmo.sniperRifleAmmo >= secondaryWeapon.maxAmmo){
			secondaryWeapon.ammo = secondaryWeapon.maxAmmo;
			allAmmo.sniperRifleAmmo -= secondaryWeapon.maxAmmo;
		}else if(allAmmo.sniperRifleAmmo < secondaryWeapon.maxAmmo){
			secondaryWeapon.ammo = allAmmo.sniperRifleAmmo;
			allAmmo.sniperRifleAmmo = 0;
		}
    }
    if(weaponEquip == 1 && secondaryWeapon.useAmmo == AmmoType.GrenadeRounds){
    	allAmmo.grenadeRounds += secondaryWeapon.ammo;
    	if(allAmmo.grenadeRounds >= secondaryWeapon.maxAmmo){
			secondaryWeapon.ammo = secondaryWeapon.maxAmmo;
			allAmmo.grenadeRounds -= secondaryWeapon.maxAmmo;
		}else if(allAmmo.grenadeRounds < secondaryWeapon.maxAmmo){
			secondaryWeapon.ammo = allAmmo.grenadeRounds;
			allAmmo.grenadeRounds = 0;
		}
    }
}

void OnGUI (){
        if (!GetComponent<CharakterSync>().isMine())
            return;

	if(aimIcon){
		GUI.DrawTexture( new Rect(Screen.width /2 -20,Screen.height /2 -20,40,40), aimIcon);
	}
	if(zoomIcon && onAiming){
		GUI.DrawTexture( new Rect(0 ,0 ,Screen.width,Screen.height), zoomIcon);
	}

     
	if(weaponEquip == 0){ //Primary Weapon
		GUI.Label ( new Rect(Screen.width - 280, Screen.height - 80, 200, 50), "Ammo : " + primaryWeapon.ammo.ToString() + " / " + spareAmmo.ToString(), ammoFont);
	}
	if(weaponEquip == 1){ //Secondary Weapon
		GUI.Label ( new Rect(Screen.width - 280, Screen.height - 80, 200, 50), "Ammo : " + secondaryWeapon.ammo.ToString() + " / " + spareAmmo.ToString() , ammoFont);
	}
	
	

}

public void Aiming (){
		Ray ray = Camera.main.ViewportPointToRay (new Vector3(0.5f,0.5f,0));
		// Do a raycast
		RaycastHit hit;
		if (Physics.Raycast (ray,out hit)){
			attackPoint.transform.LookAt(hit.point);
			if(secondaryWeapon.weaponAtkPoint)
				secondaryWeapon.weaponAtkPoint.transform.LookAt(hit.point);
			if(primaryWeapon.weaponAtkPoint)
				primaryWeapon.weaponAtkPoint.transform.LookAt(hit.point);
		}else{
			attackPoint.transform.rotation = Camera.main.transform.rotation;
			if(secondaryWeapon.weaponAtkPoint)
				secondaryWeapon.weaponAtkPoint.transform.rotation = Camera.main.transform.rotation;
			if(primaryWeapon.weaponAtkPoint)
				primaryWeapon.weaponAtkPoint.transform.rotation = Camera.main.transform.rotation;
		}
}

public IEnumerator MagicSkill ( int skillID  ){
	if(!skill[skillID].skillAnimation){
		print("Please assign skill animation in Skill Animation");
		yield return false;
	}
	str = stat.addAtk;
	matk = stat.addMatk;

        if (stat.mana > skill[skillID].manaCost && !stat.silence)
        {



            if (sound.magicCastVoice)
            {
                GetComponent<AudioSource>().PlayOneShot(sound.magicCastVoice);
            }
            attacking = true;
            GetComponent<CharacterMotor>().canControl = false;

            if (!useMecanim)
            {
                //For Legacy Animation
                mainModel.GetComponent<Animation>()[skill[skillID].skillAnimation.name].layer = 16;
                mainModel.GetComponent<Animation>()[skill[skillID].skillAnimation.name].speed = skill[skillID].skillAnimationSpeed;
                mainModel.GetComponent<Animation>().Play(skill[skillID].skillAnimation.name);
            }
            else
            {
                //For Mecanim Animation
                GetComponent<PlayerMecanimAnimation>().AttackAnimation(skill[skillID].skillAnimation.name);
            }

            nextFire = Time.time + skill[skillID].skillDelay;

            yield return new WaitForSeconds(skill[skillID].castTime);
            if (skill[skillID].skillPrefab != null)
            {
                GameObject bulletShootout = Instantiate(skill[skillID].skillPrefab, attackPoint.transform.position, attackPoint.transform.rotation).gameObject;
                if (bulletShootout.GetComponent<BulletStatus>() != null)
                    bulletShootout.GetComponent<BulletStatus>().Setting(str, matk, "Player", this.gameObject, attackPoint.transform.forward, attackPoint.forward);
                else
                    bulletShootout.GetComponent<BulletColider>().Setting(str, matk, "Player", this.gameObject, attackPoint.transform.forward, attackPoint.transform.up);
            }
           
            attacking = false;
            GetComponent<CharacterMotor>().canControl = true;
            stat.mana -= skill[skillID].manaCost;
            GetComponent<StatsBars>().mpTimer = Time.time;
            yield return new WaitForSeconds(skill[skillID].skillDelay);

        }
    }

public IEnumerator MeleeCombo (){
	if(!meleeAttack.meleePrefab){
            yield return false;

    }
	if(!meleeAttack.meleeAnimation[c]){
		print("Please assign attack animation in Attack Combo");
            yield return false;
	}
	str = stat.addMelee;
	matk = stat.addMatk;
	attacking = true;
	GetComponent<CharacterMotor>().canControl = false;
StartCoroutine(	MeleeDash());
	//---------Hide Gun-----------
	if(primaryWeaponModel){
		primaryWeaponModel.SetActive(false);
	}
	if(secondaryWeaponModel){
		secondaryWeaponModel.SetActive(false);
	}
	//----------------------------
	if(meleeWeaponModel){
		meleeWeaponModel.SetActive(true);
	}
	//---------------------------
	if(meleeAttack.meleeSound){
		GetComponent<AudioSource>().PlayOneShot(meleeAttack.meleeSound);
	}
	if(sound.attackComboVoice.Length > c && sound.attackComboVoice[c]){
		GetComponent<AudioSource>().PlayOneShot(sound.attackComboVoice[c]);
	}
        float wait = 0;

    if (!useMecanim){
		//For Legacy Animation
		mainModel.GetComponent<Animation>()[meleeAttack.meleeAnimation[c].name].layer = 15;
		mainModel.GetComponent<Animation>().PlayQueued(meleeAttack.meleeAnimation[c].name, QueueMode.PlayNow).speed = meleeAttack.meleeAnimationSpeed;
		wait = mainModel.GetComponent<Animation>()[meleeAttack.meleeAnimation[c].name].length;
	}else{
		//For Mecanim Animation
		GetComponent<PlayerMecanimAnimation>().AttackAnimation(meleeAttack.meleeAnimation[c].name);
		AnimatorClipInfo[] clip= GetComponent<PlayerMecanimAnimation>().animator.GetCurrentAnimatorClipInfo(0);
		wait = clip.Length -0.5f;
	}
	
	yield return new WaitForSeconds(meleeAttack.meleeCast);
	c++;
	
	nextFire = Time.time + meleeAttack.meleeDelay;
	GameObject bulletShootout = Instantiate(meleeAttack.meleePrefab, attackPoint.transform.position , attackPoint.transform.rotation);
	bulletShootout.GetComponent<BulletColider>().Setting(str , matk , "Player" , this.gameObject,attackPoint.forward,attackPoint.up);
			
	if(c >= meleeAttack.meleeAnimation.Length){
		c = 0;
		yield return new WaitForSeconds(wait);
	}else{
		yield return new WaitForSeconds(meleeAttack.meleeDelay);
	}
	
	attacking = false;
	GetComponent<CharacterMotor>().canControl = true;
	
	if(primaryWeaponModel && weaponEquip == 0){
		primaryWeaponModel.SetActive(true);
	}
	if(secondaryWeaponModel && weaponEquip == 1){
		secondaryWeaponModel.SetActive(true);
	}
	//----------------------------
	if(meleeWeaponModel){
		meleeWeaponModel.SetActive(false);
	}
	//---------------------------
}

public IEnumerator MeleeDash (){
	meleefwd = true;
	yield return new WaitForSeconds(0.2f);
	meleefwd = false;
}

public void SettingWeapon (){
	stat = GetComponent<status>();
	if(onAiming){
       Camera.main.fieldOfView = 60;
       	onAiming = false;
    }


    
	if(weaponEquip == 0){
		//Primary Weapon
		zoomLevel = primaryWeapon.zoomLevel;
		if(primaryWeapon.aimIcon){
			aimIcon = primaryWeapon.aimIcon;
		}
        if(primaryWeapon.zoomIcon != null)
		zoomIcon = primaryWeapon.zoomIcon;

		automatic = primaryWeapon.automatic;
        //    primaryWeaponModel.GetComponentInChildren<Crosshair>().cam = Camera.main;
		/*if(primaryWeapon.weaponAtkPoint){
			attackPoint = primaryWeapon.weaponAtkPoint;
		}*/
		stat.currentWeaponAtk = stat.weaponAtk;
		stat.CalculateStatus();
            muzzleFlash = primaryWeaponModel.GetComponent<WeaponAttackPoint>().muzzleFlash;

            if (primaryWeaponModel){
			primaryWeaponModel.SetActive(true);
		}
		if(secondaryWeaponModel){
			secondaryWeaponModel.SetActive(false);
		}
	}else{
		//Secondary Weapon
		zoomLevel = secondaryWeapon.zoomLevel;
		if(secondaryWeapon.aimIcon){
			aimIcon = secondaryWeapon.aimIcon;
		}
        if(secondaryWeapon.zoomIcon!=null)
		zoomIcon = secondaryWeapon.zoomIcon;
		automatic = secondaryWeapon.automatic;
        //    secondaryWeaponModel.GetComponentInChildren<Crosshair>().cam = Camera.main;

            /*if(secondaryWeapon.weaponAtkPoint){
                attackPoint = primaryWeapon.weaponAtkPoint;
            }*/
            stat.currentWeaponAtk = stat.weaponAtk2;
		stat.CalculateStatus();
            muzzleFlash = secondaryWeaponModel.GetComponent<WeaponAttackPoint>().muzzleFlash;

            if (primaryWeaponModel){
			primaryWeaponModel.SetActive(false);
		}
		if(secondaryWeaponModel){
			secondaryWeaponModel.SetActive(true);
		}
	}

}


public IEnumerator SwapWeapon (){
	/*if(reloading){
		return;
	}*/
	StopCoroutine(Reload());
        float wait = 0;
        if (onAiming){
       	Camera.main.fieldOfView = 60;
       	onAiming = false;
    }
    PlayerMecanimAnimation mecanim = GetComponent<PlayerMecanimAnimation>();
	if(weaponEquip == 0){
		//Swap from Primary to Secondary
	//	if(!secondaryWeapon.bulletPrefab){
	//		return;	//Do Nothing If you don't have Secondary Weapon's Bullet
	//	}
		if(primaryWeaponModel){
			primaryWeaponModel.SetActive(false);
		}
		if(secondaryWeaponModel){
			secondaryWeaponModel.SetActive(true);
		}

       
        weaponEquip = 1;
		if(!useMecanim && secondaryWeapon.equipAnimation){
			//For Legacy Animation
			mainModel.GetComponent<Animation>()[secondaryWeapon.equipAnimation.name].layer = 10;
		//	mainModel.GetComponent<Animation>().Play(secondaryWeapon.equipAnimation.name);
           //     GetComponent<PhotonView>().RPC("PlayAnim", RpcTarget.All, secondaryWeapon.equipAnimation.name); 
			freeze = true;
			wait = mainModel.GetComponent<Animation>()[secondaryWeapon.equipAnimation.name].length;
			yield return new WaitForSeconds(wait);
			freeze = false;
		}else{
			//For Mecanim Animation
			mecanim.animator.SetLayerWeight(mecanim.upperBodyLayer, 1);
			GetComponent<PlayerMecanimAnimation>().PlayAnim(secondaryWeapon.equipAnimation.name);
			AnimatorClipInfo[] clip= GetComponent<PlayerMecanimAnimation>().animator.GetCurrentAnimatorClipInfo(0);
			freeze = true;
			wait = clip.Length -0.4f;
			yield return new WaitForSeconds(wait);
			mecanim.animator.SetLayerWeight(mecanim.upperBodyLayer, 0);
			freeze = false;
		}
		
	}else{
		//Swap from Secondary to Primary
	//	if(!primaryWeapon.bulletPrefab){
	//		return; //Do Nothing If you don't have Primary Weapon's Bullet
	//	}
		if(primaryWeaponModel){
			primaryWeaponModel.SetActive(true);
		}
		if(secondaryWeaponModel){
			secondaryWeaponModel.SetActive(false);
		}
		weaponEquip = 0;
		if(!useMecanim && primaryWeapon.equipAnimation){
			//For Legacy Animation
			mainModel.GetComponent<Animation>()[primaryWeapon.equipAnimation.name].layer = 10;
			mainModel.GetComponent<Animation>().Play(primaryWeapon.equipAnimation.name);
			freeze = true;
			wait = mainModel.GetComponent<Animation>()[primaryWeapon.equipAnimation.name].length;
			yield return new WaitForSeconds(wait);
			freeze = false;
		}else{
			//For Mecanim Animation
			mecanim.animator.SetLayerWeight(mecanim.upperBodyLayer, 1);
			GetComponent<PlayerMecanimAnimation>().PlayAnim(primaryWeapon.equipAnimation.name);
                AnimatorClipInfo[] clip = GetComponent<PlayerMecanimAnimation>().animator.GetCurrentAnimatorClipInfo(0);
			freeze = true;
			wait = clip.Length -0.4f;
			yield return new WaitForSeconds(wait);
			mecanim.animator.SetLayerWeight(mecanim.upperBodyLayer, 0);
			freeze = false;
		}
	}
	if(!useMecanim){
		GetComponent<PlayerLegacyAnimation>().SetAnimation();
	}else{
		GetComponent<PlayerMecanimAnimation>().SetAnimation();
		GetComponent<PlayerMecanimAnimation>().SetIdle();
	}
	SettingWeapon();
}

public void ForceSwap (){
	//Use when call by other script.
    StartCoroutine(SwapWeapon());
}

public void ShowMeleeWeapon ( bool b  ){
	 //---------Hide Gun-----------
	if(b){
		if(primaryWeaponModel && weaponEquip == 0){
			primaryWeaponModel.SetActive(false);
		}
		if(secondaryWeaponModel && weaponEquip == 1){
			secondaryWeaponModel.SetActive(false);
		}
	}else{
		if(primaryWeaponModel && weaponEquip == 0){
			primaryWeaponModel.SetActive(true);
		}
		if(secondaryWeaponModel && weaponEquip == 1){
			secondaryWeaponModel.SetActive(true);
		}
	}
	//----------------------------
	if(meleeWeaponModel){
		meleeWeaponModel.SetActive(b);
	}

}
    

}