using UnityEngine;
using System.Collections;
using Photon.Pun;

public class ThirdPersonCamera : MonoBehaviour {

    public Transform target;
    public float targetSide = 0;
    public float xSpeed = 250.0f;
    public float distance;
    public float targetHeight;
    public float ySpeed = 120.0f;
    public float yMinLimit = -10;
    public float yMaxLimit = 70;
private float x = 20.0f;
private float y = 0.0f;
    public bool freeze = false;

[HideInInspector]
    public float shakeValue = 0.0f;
[HideInInspector]
    public bool onShaking = false;
private float shakingv = 0.0f;
         
void Start (){
     if(!target){
            GameObject[] trans = GameObject.FindGameObjectsWithTag("Player");
            for (int i = 0; i < trans.Length; i++)
                if (trans[i].GetComponent<CharakterSync>().isMine())
                {
                    target = trans[i].transform;
                }
     }
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;
     
          if (GetComponent<Rigidbody>())
          GetComponent<Rigidbody>().freezeRotation = true;
         
}
     
void LateUpdate (){
        if (!target)
        {
            GameObject[] trans = GameObject.FindGameObjectsWithTag("Player");
            for (int i = 0; i < trans.Length; i++)
                if (trans[i].GetComponent<CharakterSync>().isMine())
                {
                    target = trans[i].transform;
                }
        }
        if (Time.timeScale == 0.0f || freeze){
		return;
	}
        float force = 0.015f;
        if (target.GetComponent<GunTrigger>().onAiming)
            force = 0.03f/ target.GetComponent<GunTrigger>().getScopeForce();

         x += GameMenager.Instance.InputControler.MouseInput.x * xSpeed * force;
      	 y -= GameMenager.Instance.InputControler.MouseInput.y * ySpeed * force;
       
       y = ClampAngle(y, yMinLimit, yMaxLimit);
       
       //Rotate Camera
       Quaternion rotation = Quaternion.Euler(y, x, 0);
       transform.rotation = rotation;
       
        //Rotate Target
        if(!freeze){
        	target.transform.rotation = Quaternion.Euler(0, x, 0);
        }
       
       //Camera Position
       Vector3 neoTargetSide = transform.position - target.position;
       Vector3 position = target.position - (rotation *new Vector3(targetSide , 0 , 1) * distance +new Vector3(0,-targetHeight,0));
       transform.position = position;
       
        RaycastHit hit;
        Vector3 trueTargetPosition = target.transform.position -new  Vector3(0,-targetHeight,0);
        if (Physics.Linecast (trueTargetPosition, transform.position,out hit)) {  
        	if(hit.transform.tag == "Wall"){
            	float tempDistance = Vector3.Distance (trueTargetPosition, hit.point) - 0.28f;

            	position = target.position - (rotation * new Vector3(targetSide , 0 , 1) * tempDistance +new  Vector3(0,-targetHeight,0));
            	transform.position = position;
            }
        }
        
        if(onShaking){
        	shakeValue = Mathf.Lerp(shakeValue, shakingv, Time.deltaTime * 2);
            //shakeValue = Random.Range(-shakingv , shakingv)* 0.2f;
            Vector3 vec = transform.position;
        	vec.y += shakeValue;
            transform.position = vec;
        }
}
     
static float ClampAngle ( float angle ,   float min ,   float max  ){
       if (angle < -360)
          angle += 360;
       if (angle > 360)
          angle -= 360;
       return Mathf.Clamp (angle, min, max);
       
}
public void Shake ( float val  ,   float dur  ){
	if(onShaking){
		return;
	}

       StartCoroutine(Shaking(val , dur));
}

    public IEnumerator Shaking ( float val  ,   float dur  ){
	onShaking = true;
	shakingv = val;
	yield return new WaitForSeconds(dur);
	shakingv = 0;
	shakeValue = 0;
	onShaking = false;
}

}