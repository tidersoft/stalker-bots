using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using GooglePlayGames;

public class SpawnPlayer : MonoBehaviour {

public GameObject[] playerPrefab = new GameObject[4];
public int spawnId = 0;
private Transform mainCam;
public bool  autoLoadTempData = false; //Auto load character data from Temporary Save Data.

public GameObject characterDatabase;
private int charId = 0;
private CharacterData player;
    public Transform SpawnPosition;
    public float PositionOffset = 2.0f;
    public GameObject[] PrefabsToInstantiate;   // set in inspector
  public   GameObject currentPlayer;
    public GameObject deadbody;
    public bool autospawn=false;

    public LoudotyType typ;
    Loudout[] loudout = new Loudout[1];
    public Camera cam;

    public string returnToScene = "Base";
    public bool show = false;

    private string goScene = "Base";
    int players;
    string roomname;

    public float[] counter = new float[1];
    private void Awake()
    {
        counter = new float[ItemData.instance.mechloud.Length];
        if (autospawn)
        Spawn(0,LoudotyType.Infrontry);
    }

    string pref = "InfLoud";

    void FixedUpdate() {
        for (int i = 0; i < counter.Length; i++) {
            if (counter[i] > 0.0f)
            {
                counter[i] -= Time.fixedDeltaTime;
            }
            else {
                counter[i] = 0.0f;
            }
        }
    }
   
    public void OnGUI()
    {
         if (currentPlayer == null && !autospawn) {
            cam.enabled = true;
            Loudout[] inf = null;
            Loudout[] mech = null;
            LoudotyType typ2 =LoudotyType.Infrontry;
            if (typ == LoudotyType.Infrontry) {
                inf = ItemData.instance.infloud;
            }
            else
                if (typ == LoudotyType.Mech) {
                inf = ItemData.instance.mechloud;
                typ2 = typ;
            }
            else
            {
                inf = ItemData.instance.infloud;
                mech = ItemData.instance.mechloud;
               
            }
            if (show)
            {
                for (int i = 0; i < inf.Length; i++)
                {
                    if (GUI.Button(new Rect(10, 10 + (i * 50), 150, 40), inf[i].name))
                    {

                        Spawn(i,typ2);
                    }
                }

                if (mech != null) {
                    for (int i = 0; i < mech.Length; i++)
                    {
                        if (GUI.Button(new Rect(160, 10 + (i * 50), 150, 40), mech[i].name) && counter[i]<=0.0f)
                        {

                            Spawn(i, LoudotyType.Mech);
                            counter[i] = 120.0f;
                        }
                        GUI.Label(new Rect(315, 20 + (i * 50), 250, 40), counter[i].ToString());
                    }

                }
                if (GUI.Button(new Rect(10, Screen.height - 80, 150, 40), "Abort Mission"))
                {
                     GameMenager.Instance.LeaveRoom("Base");
                    
              
                    //LoadTempData();
                }

            }
        }

    }

    public  GameObject Spawn (int index,LoudotyType typ3){
	if(characterDatabase)
		player = characterDatabase.GetComponent<CharacterData>();

		//Check for Current Player in the scene
     if(deadbody!=null)
		 Destroy(deadbody) ;

        //Spawn Player

        GameObject o = null;
        if (typ3== LoudotyType.Infrontry) {

                o = ItemData.instance.infloud[index].charakter;
            }
            else {
                o = ItemData.instance.mechloud[index].charakter;
            }
            if(o != null) { 
                    Debug.Log("Instantiating: " + o.name);

                    Vector3 spawnPos = Vector3.up;
                    if (this.SpawnPosition != null)
                    {
                        spawnPos = this.SpawnPosition.position;
                    }

                    Vector3 random = UnityEngine.Random.insideUnitSphere;
                    random.y = 0;
                    random = random.normalized;
                    Vector3 itempos = spawnPos + this.PositionOffset * random;

                   currentPlayer = GameObject.Instantiate(o, itempos, Quaternion.identity);

         //   Debug.Log(PlayGamesPlatform.Instance.GetUserId());
            currentPlayer.GetComponent<CharakterSync>().setParticipantId(PlayGamesPlatform.Instance.GetUserId());
            currentPlayer.GetComponent<setplayername>().activeText();

            
            }
        if (typ3 == LoudotyType.Mech)
            currentPlayer.GetComponentInChildren<WeaponSelector>().Setup(index);
        else
            currentPlayer.GetComponent<Inventory>().Setup(index);

        cam.enabled = false;
        return currentPlayer;
    }

    
  public void OnLeftRoom()
    {
        if (goScene != null && roomname!=null)
        {

         
        }
    }
    bool leftroom = true;
    public string url2 = "http://wargamertable.org/StalkerBot/quary.php?opcja=deleteRoom";
    public string url3 = "http://wargamertable.org/StalkerBot/quary.php?opcja=leaveRoom";
    IEnumerator leftRooms(string param)
    {
        leftroom = false;
        players -= 1;
        if (players < 1)
        {
            Debug.Log(param);
            using (WWW www = new WWW(url2 + "&" + param))
            {
                yield return www;
                leftroom = Boolean.Parse(www.text);
                Debug.Log(www.text);
                if (leftroom)
                {
                
                }
            }
        }
        else
        {

            Debug.Log(param);
            using (WWW www = new WWW(url3 + "&" + param))
            {
                yield return www;
                leftroom = Boolean.Parse(www.text);
                Debug.Log(www.text);
                if (leftroom)
                {
                  
                }
            }

        }

    }

    void LoadTempData (){
	int saveSlot = -1; //-1 is a Temp ID
	//int saveSlot = PlayerPrefs.GetInt("SaveSlot");
	charId = PlayerPrefs.GetInt("PlayerID" +saveSlot.ToString());
	//CharacterData player = characterDatabase.GetComponent<CharacterData>();
	
		GameObject respawn = Instantiate(player.player[charId].playerPrefab, transform.position , transform.rotation);
		//yield return new WaitForSeconds(0.2f);
		respawn.GetComponent<status>().characterName = PlayerPrefs.GetString("Name" +saveSlot.ToString());
		respawn.GetComponent<status>().level = PlayerPrefs.GetInt("PlayerLevel" +saveSlot.ToString());
		respawn.GetComponent<status>().playerId = PlayerPrefs.GetInt("PlayerID" +saveSlot.ToString());
		respawn.GetComponent<status>().atk = PlayerPrefs.GetInt("PlayerATK" +saveSlot.ToString());
		respawn.GetComponent<status>().def = PlayerPrefs.GetInt("PlayerDEF" +saveSlot.ToString());
		respawn.GetComponent<status>().matk = PlayerPrefs.GetInt("PlayerMATK" +saveSlot.ToString());
		respawn.GetComponent<status>().mdef = PlayerPrefs.GetInt("PlayerMDEF" +saveSlot.ToString());
		respawn.GetComponent<status>().mdef = PlayerPrefs.GetInt("PlayerMDEF" +saveSlot.ToString());
		respawn.GetComponent<status>().exp = PlayerPrefs.GetInt("PlayerEXP" +saveSlot.ToString());
		respawn.GetComponent<status>().maxExp = PlayerPrefs.GetInt("PlayerMaxEXP" +saveSlot.ToString());
		respawn.GetComponent<status>().maxHealth = PlayerPrefs.GetInt("PlayerMaxHP" +saveSlot.ToString());
		respawn.GetComponent<status>().health = PlayerPrefs.GetInt("PlayerHP" +saveSlot.ToString());
		//respawn.GetComponent<Status>().health = PlayerPrefs.GetInt("PlayerMaxHP");
		respawn.GetComponent<status>().maxMana = PlayerPrefs.GetInt("PlayerMaxMP" +saveSlot.ToString());
		respawn.GetComponent<status>().mana = PlayerPrefs.GetInt("PlayerMaxMP" +saveSlot.ToString());
		respawn.GetComponent<status>().statusPoint = PlayerPrefs.GetInt("PlayerSTP" +saveSlot.ToString());
		respawn.GetComponent<status>().maxShield = PlayerPrefs.GetInt("PlayerMaxShield" +saveSlot.ToString());
		respawn.GetComponent<status>().melee = PlayerPrefs.GetInt("PlayerMelee" +saveSlot.ToString());
		
		//-------------------------------
		respawn.GetComponent<Inventory>().cash = PlayerPrefs.GetInt("Cash" +saveSlot.ToString());
		int itemSize = respawn.GetComponent<Inventory>().itemSlot.Length;
			int a = 0;
		/*	if(itemSize > 0){
				while(a < itemSize){
					respawn.GetComponent<Inventory>().itemSlot[a] = PlayerPrefs.GetInt("Item" + a.ToString() +saveSlot.ToString());
					respawn.GetComponent<Inventory>().itemQuantity[a] = PlayerPrefs.GetInt("ItemQty" + a.ToString() +saveSlot.ToString());
					//-------
					a++;
				}
			}
			
			int equipSize = respawn.GetComponent<Inventory>().equipment.Length;
			a = 0;
			if(equipSize > 0){
				while(a < equipSize){
					respawn.GetComponent<Inventory>().equipment[a] = PlayerPrefs.GetInt("Equipm" + a.ToString() +saveSlot.ToString());
					respawn.GetComponent<Inventory>().equipAmmo[a] = PlayerPrefs.GetInt("EquipAmmo" + a.ToString() +saveSlot.ToString());
					a++;
				}
			}
			respawn.GetComponent<Inventory>().primaryEquip = 0;
			//respawn.GetComponent<Inventory>().primaryEquip = PlayerPrefs.GetInt("PrimaryEquip" +saveSlot.ToString());
			respawn.GetComponent<Inventory>().secondaryEquip = 0;
			respawn.GetComponent<Inventory>().meleeEquip = 0;
			respawn.GetComponent<Inventory>().armorEquip = PlayerPrefs.GetInt("ArmoEquip" +saveSlot.ToString());
		
		respawn.GetComponent<Inventory>().cancelAssign = true;
		if(PlayerPrefs.GetInt("PrimaryEquip" +saveSlot.ToString()) > 0){
			respawn.GetComponent<Inventory>().EquipItem(PlayerPrefs.GetInt("PrimaryEquip" +saveSlot.ToString()) , respawn.GetComponent<Inventory>().equipment.Length + 5);
			respawn.GetComponent<GunTrigger>().primaryWeapon.ammo = PlayerPrefs.GetInt("PrimaryAmmo" +saveSlot.ToString());
			
		}
		if(PlayerPrefs.GetInt("SecondaryEquip" +saveSlot.ToString()) > 0){
			respawn.GetComponent<Inventory>().EquipItem(PlayerPrefs.GetInt("SecondaryEquip" +saveSlot.ToString()) , respawn.GetComponent<Inventory>().equipment.Length + 5);
			respawn.GetComponent<GunTrigger>().secondaryWeapon.ammo = PlayerPrefs.GetInt("SecondaryAmmo" +saveSlot.ToString());
		}
		if(PlayerPrefs.GetInt("MeleeEquip" +saveSlot.ToString()) > 0){
			respawn.GetComponent<Inventory>().EquipItem(PlayerPrefs.GetInt("MeleeEquip" +saveSlot.ToString()) , respawn.GetComponent<Inventory>().equipment.Length + 5);
		}
			respawn.GetComponent<Inventory>().RemoveWeaponMesh();*/
			//----------------------------------
		//Screen.lockCursor = true;
		
		 GameObject[] mon; 
  		 mon = GameObject.FindGameObjectsWithTag("Enemy"); 
  			 foreach(GameObject mo in mon) { 
  			 	if(mo){
  			 		mo.GetComponent<AIenemy>().followTarget = respawn.transform;
  			 	}
  			 }
			
			//Load Skill Slot
			a = 0;
			while(a < respawn.GetComponent<SkillWindow>().skill.Length){
			//	respawn.GetComponent<SkillWindow>().skill[a] = PlayerPrefs.GetInt("Skill" + a.ToString() +saveSlot.ToString());
				a++;
			}
			//Skill List Slot
			a = 0;
			while(a < respawn.GetComponent<SkillWindow>().skillListSlot.Length){
				respawn.GetComponent<SkillWindow>().skillListSlot[a] = PlayerPrefs.GetInt("SkillList" + a.ToString() +saveSlot.ToString());
				a++;
			}
			respawn.GetComponent<SkillWindow>().AssignAllSkill();
		//---------------Set Target to Minimap--------------
  		GameObject minimap = GameObject.FindWithTag("Minimap");
  		if(minimap){
  			GameObject mapcam = minimap.GetComponent<MinimapOnOff>().minimapCam;
  			mapcam.GetComponent<MinimapCamera>().target = respawn.transform;
  		}
  		//Load Ammo
		respawn.GetComponent<GunTrigger>().allAmmo.handgunAmmo = PlayerPrefs.GetInt("HandgunAmmo" +saveSlot.ToString());
		respawn.GetComponent<GunTrigger>().allAmmo.machinegunAmmo = PlayerPrefs.GetInt("MachinegunAmmo" +saveSlot.ToString());
		respawn.GetComponent<GunTrigger>().allAmmo.shotgunAmmo = PlayerPrefs.GetInt("ShotgunAmmo" +saveSlot.ToString());
		respawn.GetComponent<GunTrigger>().allAmmo.magnumAmmo = PlayerPrefs.GetInt("MagnumAmmo" +saveSlot.ToString());
		respawn.GetComponent<GunTrigger>().allAmmo.smgAmmo = PlayerPrefs.GetInt("SmgAmmo" +saveSlot.ToString());
		respawn.GetComponent<GunTrigger>().allAmmo.sniperRifleAmmo = PlayerPrefs.GetInt("SniperRifleAmmo" +saveSlot.ToString());
		respawn.GetComponent<GunTrigger>().allAmmo.grenadeRounds = PlayerPrefs.GetInt("GrenadeRounds" +saveSlot.ToString());
}

}