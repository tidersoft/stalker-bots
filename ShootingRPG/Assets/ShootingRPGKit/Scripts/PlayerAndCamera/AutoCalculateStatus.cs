using UnityEngine;
using System.Collections;

public class AutoCalculateStatus : MonoBehaviour {

public int currentLv = 1;
    public int maxLevel = 100;
    [System.Serializable]
    public class StatusParam
    {
        public float atk = 0;
        public float def = 0;
        public float matk = 0;
        public float mdef = 0;
        public float melee = 0;
        public float maxHealth = 100;
        public float maxMana = 100;
        public float maxShield = 100;
}
    [SerializeField]
    public StatusParam minStatus;
    [SerializeField]
    public StatusParam maxStatus;

private float min = 0;
private float max = 0;
    int players = 0;

void Start (){
    
	CalculateStatLv();
}

    public void Update()
    {
        if (players != GameObject.FindGameObjectsWithTag("Player").Length) {
            GameObject[] player= GameObject.FindGameObjectsWithTag("Player");
            int sumlv = 0;
            for (int i = 0; i < player.Length; i++) {
                sumlv += player[i].GetComponent<status>().level;
            }
            currentLv = (int)(sumlv / player.Length);
            CalculateStatLv();
            players = player.Length;
        }    
    }

    public void CalculateStatLv (){
	status stat = GetComponent<status>();
        stat.level = currentLv;
	//currentLv = stat.level -1;
	//[min_stat*(max_lv-lv)/(max_lv- 1)] + [max_stat*(lv- 1)/(max_lv- 1)]

	//Atk
	min = minStatus.atk ;
	max = maxStatus.atk * currentLv;
	stat.atk = (int)(min + max);
    //Def
    min = minStatus.def;
    max = maxStatus.def * currentLv;
    stat.def = (int)(min + max);
    //Matk
    min = minStatus.matk;
    max = maxStatus.matk * currentLv;
    stat.matk = (int)(min + max);
    //Mdef
    min = minStatus.mdef;
    max = maxStatus.mdef * currentLv;
    stat.mdef = (int)(min + max);
    //Melee
    min = minStatus.melee;
    max = maxStatus.melee * currentLv;
    stat.melee = (int)(min + max);
    //Shield
    min = minStatus.maxShield;
    max = maxStatus.maxShield * currentLv;
    stat.maxShield = (int)(min + max);
    stat.shield = stat.maxShield;

    //HP
    min = minStatus.maxHealth;
    max = maxStatus.maxHealth * currentLv;
    stat.maxHealth = (int)(min + max);
    stat.health = stat.maxHealth;
    //MP
    min = minStatus.maxMana;
    max = maxStatus.maxMana * currentLv;
    stat.maxMana = (int)(min + max);
    stat.mana = stat.maxMana;
	
	stat.CalculateStatus();
}

}