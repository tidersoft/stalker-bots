using UnityEngine;
using System.Collections;
using Photon.Pun;

public class Inventory : MonoBehaviour {

private bool  menu = false;
private bool  itemMenu = true;
private bool  equipMenu = false;

public bool  autoAssignWeapon = false; // Use for Auto Assign Weapon Equip to Gun Trigger.
    public ItemData.Usable[] itemSlot = new ItemData.Usable[18];
    public int[] itemQuantity = new int[18];
    public ItemData.Equip[] equipment = new ItemData.Equip[15];
    public int[] equipAmmo = new int[15];

    public ItemData.Weapon primaryEquip ;
    public ItemData.Weapon secondaryEquip ;
    public ItemData.Weapon meleeEquip ;
    public ItemData.Equip armorEquip ;

    public GameObject database;

    public int cash = 500;

    public GUISkin skin;
    public Rect windowRect = new Rect (360 ,140 ,480 ,550);
private Rect originalRect;

    public int itemPageMultiply = 6;
    public int equipmentPageMultiply = 5;
private int page = 0;

    public GUIStyle itemNameText;
    public GUIStyle itemDescriptionText;
    public GUIStyle itemQuantityText;
private GunTrigger gun;
private ItemData dataItem;
[HideInInspector]
    public bool cancelAssign = false;

    public int pWeapon = 0;
    public int sWeapon = 2;
    public int mWeapon = 1;

public void Setup (int Loudout){
	dataItem = database.GetComponent<ItemData>();
	gun = GetComponent<GunTrigger>();
	originalRect = windowRect;
	SetEquipmentStatus();
	if(autoAssignWeapon){
		//yield return new WaitForSeconds(0.2f);
		if(cancelAssign){
			return;
		}
           primaryEquip = ItemData.instance.infloud[Loudout].primeryWeapon;
            secondaryEquip = ItemData.instance.infloud[Loudout].seconderyWeapon;
            meleeEquip = ItemData.instance.infloud[Loudout].meleWeapon;

		ItemData.Weapon e = primaryEquip;
		if(e !=null){
			EquipItem(e , equipment.Length +5);
			gun.primaryWeapon.ammo = e.maxAmmo;
                AddAmmo(e.maxAmmo * 5, e.useAmmo);
               
		}
		
		e = secondaryEquip;
		if(e !=null){
			EquipItem(e , equipment.Length +5);
			gun.secondaryWeapon.ammo = e.maxAmmo;
            AddAmmo(e.maxAmmo * 5, e.useAmmo);
            }
		
		e = meleeEquip;
		if(e !=null){
			EquipItem(e , equipment.Length +5);

		}
	}

        for (int i = 0; i < 4; i++) {
            GetComponent<SkillWindow>().skill[i] = ItemData.instance.infloud[Loudout].sklis[i];
        }

        GetComponent<SkillWindow>().AssignAllSkill();


}

    public void SetEquipmentStatus (){
		//Reset Power of Current Weapon & Armor
		GetComponent<status>().weaponAtk = 0;
		GetComponent<status>().weaponMatk = 0;
		GetComponent<status>().weaponAtk2 = 0;
		GetComponent<status>().armorDef = 0;
		GetComponent<status>().armorMdef = 0;
		GetComponent<status>().weaponMelee = 0;
		GetComponent<status>().addHPpercent = 0;
		GetComponent<status>().addMPpercent = 0;
		GetComponent<status>().armorShield = 0;
		//Set New Variable of Primary Weapon
		GetComponent<status>().weaponAtk += primaryEquip.attack;
		GetComponent<status>().armorDef += primaryEquip.defense;
		GetComponent<status>().weaponMatk += primaryEquip.magicAttack;
		GetComponent<status>().armorMdef += primaryEquip.magicDefense;
		GetComponent<status>().weaponMelee += primaryEquip.meleeDamage;
		GetComponent<status>().armorShield += primaryEquip.shieldPlus;
		//Set New Variable of Secondary Weapon
		GetComponent<status>().weaponAtk2 += secondaryEquip.attack;
		GetComponent<status>().armorDef += secondaryEquip.defense;
		GetComponent<status>().weaponMatk += secondaryEquip.magicAttack;
		GetComponent<status>().armorMdef += secondaryEquip.magicDefense;
		GetComponent<status>().weaponMelee += secondaryEquip.meleeDamage;
		GetComponent<status>().armorShield += secondaryEquip.shieldPlus;
		//Set New Variable of Melee Weapon
		GetComponent<status>().armorDef += meleeEquip.defense;
		GetComponent<status>().weaponMatk += meleeEquip.magicAttack;
		GetComponent<status>().armorMdef += meleeEquip.magicDefense;
		GetComponent<status>().weaponMelee += meleeEquip.meleeDamage;
		GetComponent<status>().armorShield += meleeEquip.shieldPlus;
		//Set New Variable of Armor
		GetComponent<status>().weaponAtk += armorEquip.attack;
		GetComponent<status>().armorDef += armorEquip.defense;
		GetComponent<status>().weaponMatk += armorEquip.magicAttack;
		GetComponent<status>().armorMdef += armorEquip.magicDefense;
		GetComponent<status>().weaponMelee += armorEquip.meleeDamage;
		GetComponent<status>().armorShield += armorEquip.shieldPlus;
		
		GetComponent<status>().CalculateStatus();
}

void Update (){
      //  if (!GetComponent<CharakterSync>().isMine())
            return;

      //  if (Input.GetKeyDown("i") || Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter)) {
	//	OnOffMenu();
		//AutoSortItem();
//	}
}

    public void UseItem ( ItemData.Usable id  ){
	GetComponent<status>().Heal(id.hpRecover , id.mpRecover);
	GetComponent<status>().ShieldRecovery(id.shieldRecover);
	AutoSortItem();
	
}

    public void EquipItem ( ItemData.Equip id  ,   int slot  ){
	if(id == null){
		return;
	}
	dataItem = database.GetComponent<ItemData>();
	gun = GetComponent<GunTrigger>();
	//Backup Your Current Equipment before Unequip
	ItemData.Equip tempEquipment = null;
	int tempAmmo = 0;
        GameObject wea = null;

    if (id.equipmentType == EqType.PrimaryWeapon){
		//Primary Weapon
		tempEquipment = primaryEquip;
		tempAmmo = gun.primaryWeapon.ammo;
		primaryEquip = (ItemData.Weapon)id;
		if(slot <= equipment.Length){
			gun.primaryWeapon.ammo = equipAmmo[slot];
		}
		if(((ItemData.Weapon)id).attackPrefab){
			gun.primaryWeapon.hitPrefab = ((ItemData.Weapon)id).attackPrefab;
		}
            gun.primaryWeapon.Hinacurisy = primaryEquip.Hinaccurty;
            gun.primaryWeapon.Vinacurisy = primaryEquip.Vinaccurty;
		//Change Weapon Mesh
		if(id.model && gun.weaponPosition){
			//gun.primaryWeaponModel.SetActive(true);
		    wea = Instantiate(id.model,gun.weaponPosition.position,gun.weaponPosition.rotation);
	   		wea.transform.parent = gun.weaponPosition;
	   		
	   		//Set new Attack Point.
	   		if(wea.GetComponent<WeaponAttackPoint>()){
				//Get Attack Point from WeaponAttackPoint of new weapon's model
				gun.primaryWeapon.weaponAtkPoint = wea.GetComponent<WeaponAttackPoint>().attackPoint;
			}else{
				GameObject n = new GameObject();
				n.transform.parent = wea.transform;
				gun.primaryWeapon.weaponAtkPoint = n.transform;
			}
			if(gun.primaryWeaponModel)
	  			Destroy(gun.primaryWeaponModel.gameObject);
	  		gun.primaryWeaponModel = wea;
		}
	}else if(id.equipmentType == EqType.SecondaryWeapon){
		//Secondary Weapon
		if(((ItemData.Weapon)id).attackPrefab){
			gun.secondaryWeapon.hitPrefab = ((ItemData.Weapon)id).attackPrefab;
		}
		tempEquipment = secondaryEquip;
		tempAmmo = gun.secondaryWeapon.ammo;
		secondaryEquip = (ItemData.Weapon)id;
		if(slot <= equipment.Length){
			gun.secondaryWeapon.ammo = equipAmmo[slot];
		}

            gun.secondaryWeapon.Hinacurisy = secondaryEquip.Hinaccurty;
            gun.secondaryWeapon.Vinacurisy = secondaryEquip.Vinaccurty;
		//Change Weapon Mesh
		if(id.model && gun.weaponPosition){
			//gun.secondaryWeaponModel.SetActive(true);
		    wea = Instantiate(id.model,gun.weaponPosition.position,gun.weaponPosition.rotation);
	   		wea.transform.parent = gun.weaponPosition;
	   		
	   		//Set new Attack Point.
	   		if(wea.GetComponent<WeaponAttackPoint>()){
				//Get Attack Point from WeaponAttackPoint of new weapon's model
				gun.secondaryWeapon.weaponAtkPoint = wea.GetComponent<WeaponAttackPoint>().attackPoint;
			}else{
                    GameObject n = new GameObject();
                    n.transform.parent = wea.transform;
				gun.secondaryWeapon.weaponAtkPoint = n.transform;
			}
			
			if(gun.secondaryWeaponModel)
	  			Destroy(gun.secondaryWeaponModel.gameObject);
	  		gun.secondaryWeaponModel = wea;
		}
	}else if(id.equipmentType == EqType.MeleeWeapon){
		//Melee Weapon
		if(((ItemData.Weapon)id).attackPrefab){
			gun.meleeAttack.meleePrefab = ((ItemData.Weapon)id).attackPrefab;
		}
		tempEquipment = meleeEquip;
		meleeEquip = (ItemData.Weapon)id;
		//Change Weapon Mesh
		if(id.model && gun.weaponPosition){
		    wea = Instantiate(id.model,gun.weaponPosition.transform.position,gun.weaponPosition.transform.rotation);
	   		wea.transform.parent = gun.weaponPosition;
	   		if(gun.meleeWeaponModel)
	  			Destroy(gun.meleeWeaponModel.gameObject);
	  		gun.meleeWeaponModel = wea;
	  		gun.meleeWeaponModel.SetActive(false);
		}
	}else{
		//Armor
		tempEquipment = armorEquip;
		armorEquip = id;
	}
	
	if(slot <= equipment.Length){
		equipment[slot] = null;
		equipAmmo[slot] = 0;
		AddEquipment(tempEquipment , tempAmmo);
	}
	//Assign Weapon Value from Database to GunTrigger
	SetWeaponValue(id);
	//Assign Weapon Animation to PlayerLegacyAnimation Script
	AssignWeaponAnimation(id);
  //      AssignMecanimAnimation(id, (int)id.equipmentType);
	//Reset Power of Current Weapon & Armor
	SetEquipmentStatus();
	GetComponent<GunTrigger>().SettingWeapon();
	
	AutoSortEquipment();
	//AddEquipment(tempEquipment , tempAmmo);

}

    public void UnEquip ( ItemData.Equip id  ){
        bool full=false;

    if (id.equipmentType == EqType.PrimaryWeapon){
			full = AddEquipment(primaryEquip , gun.primaryWeapon.ammo);
			//-----------------------------------
	}else if(id.equipmentType == EqType.SecondaryWeapon){
			full = AddEquipment(secondaryEquip , gun.secondaryWeapon.ammo);
			//-----------------------------------
	}else if(id.equipmentType == EqType.MeleeWeapon){
			full = AddEquipment(meleeEquip , 0);
			//-----------------------------------
	}else{
			full = AddEquipment(armorEquip , 0);
	}
	if(!full){
		if(id.equipmentType == EqType.PrimaryWeapon){
			primaryEquip = null;
		//	gun.primaryWeapon.bulletPrefab = null;
			if(gun.primaryWeaponModel){
  				gun.primaryWeaponModel.SetActive(false);
  			}
  			//------------------------
		}else if(id.equipmentType == EqType.SecondaryWeapon){
			secondaryEquip = null;
		//	gun.secondaryWeapon.bulletPrefab = null;
			if(gun.secondaryWeaponModel){
  				gun.secondaryWeaponModel.SetActive(false);
  			}
			//--------------------
		}else if(id.equipmentType == EqType.MeleeWeapon){
			meleeEquip = null;
			gun.meleeAttack.meleePrefab = null;
			if(gun.meleeWeaponModel){
  				gun.meleeWeaponModel.SetActive(false);
  			}
			//--------------------
		}else{
			armorEquip = null;
		}
		SetEquipmentStatus();
    }
}

void OnGUI (){
		GUI.skin = skin;
		if(menu && itemMenu){
			windowRect = GUI.Window (1, windowRect, ItemWindow, "Items");
		}
		if(menu && equipMenu){
			windowRect = GUI.Window (1, windowRect, ItemWindow, "Equipment");
		}
		
		if(menu){
			if (GUI.Button ( new Rect(windowRect.x -50, windowRect.y +105,50,100), "Item")) {
				//Switch to Item Tab
				page = 0;
				itemMenu = true;
				equipMenu = false;
			}
			if (GUI.Button ( new Rect(windowRect.x -50, windowRect.y +225,50,100), "Equip")) {
				//Switch to Equipment Tab
				page = 0;
				equipMenu = true;
				itemMenu = false;	
			}
		}
	}

    //-----------Item Window-------------
    public void ItemWindow ( int windowID  ){
		if(menu && itemMenu){
			//GUI.Box ( new Rect(260,140,280,385), "Items");
			//Close Window Button
			if (GUI.Button ( new Rect(windowRect.width -50 ,8,40,40), "X")) {
				OnOffMenu();
			}
			//Items Slot
			if (GUI.Button ( new Rect(30,30,60,60),itemSlot[0 + page].icon)){
				UseItem(itemSlot[0 + page]);
				if(itemQuantity[0 + page] > 0){
					itemQuantity[0 + page]--;
				}
				if(itemQuantity[0 + page] <= 0){
					itemSlot[0 + page] = null;
					itemQuantity[0 + page] = 0;
					AutoSortItem();
				}
			}
			GUI.Label ( new Rect(125, 40, 320, 75), itemSlot[0 + page].itemName.ToString() , itemNameText); //Item Name
			GUI.Label ( new Rect(125, 65, 320, 75), itemSlot[0 + page].description.ToString() , itemDescriptionText); //Item Description
			if(itemQuantity[0 + page] > 0){
				GUI.Label ( new Rect(73, 73, 40, 30), itemQuantity[0 + page].ToString() , itemQuantityText); //Quantity
			}
			//------------------------------------------------------
			if (GUI.Button ( new Rect(30,105,60,60),itemSlot[1 + page].icon)){
				UseItem(itemSlot[1 + page]);
				if(itemQuantity[1 + page] > 0){
					itemQuantity[1 + page]--;
				}
				if(itemQuantity[1 + page] <= 0){
					itemSlot[1 + page] = null;
					itemQuantity[1 + page] = 0;
					AutoSortItem();
				}
			}
			GUI.Label ( new Rect(125, 115, 320, 75), itemSlot[1 + page].itemName.ToString() , itemNameText); //Item Name
			GUI.Label ( new Rect(125, 140, 320, 75), itemSlot[1 + page].description.ToString() , itemDescriptionText); //Item Description
			if(itemQuantity[1 + page] > 0){
				GUI.Label ( new Rect(73, 148, 40, 30), itemQuantity[1 + page].ToString() , itemQuantityText); //Quantity
			}
			//------------------------------------------------------
			if (GUI.Button ( new Rect(30,180,60,60),itemSlot[2 + page].icon)){
				UseItem(itemSlot[2 + page]);
				if(itemQuantity[2 + page] > 0){
					itemQuantity[2 + page]--;
				}
				if(itemQuantity[2 + page] <= 0){
					itemSlot[2 + page] =null;
					itemQuantity[2 + page] = 0;
					AutoSortItem();
				}
			}
			GUI.Label ( new Rect(125, 190, 320, 75), itemSlot[2 + page].itemName.ToString() , itemNameText); //Item Name
			GUI.Label ( new Rect(125, 215, 320, 75),itemSlot[2 + page].description.ToString() , itemDescriptionText); //Item Description
			if(itemQuantity[2 + page] > 0){
				GUI.Label ( new Rect(73, 223, 40, 30), itemQuantity[2 + page].ToString() , itemQuantityText); //Quantity
			}
			//------------------------------------------------------
			if (GUI.Button ( new Rect(30,255,60,60),itemSlot[3 + page].icon)){
				UseItem(itemSlot[3 + page]);
				if(itemQuantity[3 + page] > 0){
					itemQuantity[3 + page]--;
				}
				if(itemQuantity[3 + page] <= 0){
					itemSlot[3 + page] = null;
					itemQuantity[3 + page] = 0;
					AutoSortItem();
				}
			}
			GUI.Label ( new Rect(125, 265, 320, 75), itemSlot[3 + page].itemName.ToString() , itemNameText); //Item Name
			GUI.Label ( new Rect(125, 290, 320, 75),itemSlot[3 + page].description.ToString() , itemDescriptionText); //Item Description
			if(itemQuantity[3 + page] > 0){
				GUI.Label ( new Rect(73, 298, 40, 30), itemQuantity[3 + page].ToString() , itemQuantityText); //Quantity
			}
			//------------------------------------------------------
			if (GUI.Button ( new Rect(30,330,60,60),itemSlot[4 + page].icon)){
				UseItem(itemSlot[4 + page]);
				if(itemQuantity[4 + page] > 0){
					itemQuantity[4 + page]--;
				}
				if(itemQuantity[4 + page] <= 0){
					itemSlot[4 + page] = null;
					itemQuantity[4 + page] = 0;
					AutoSortItem();
				}
			}
			GUI.Label ( new Rect(125, 340, 320, 75),itemSlot[4 + page].itemName.ToString() , itemNameText); //Item Name
			GUI.Label ( new Rect(125, 365, 320, 75),itemSlot[4 + page].description.ToString() , itemDescriptionText); //Item Description
			if(itemQuantity[4 + page] > 0){
				GUI.Label ( new Rect(73, 373, 40, 30), itemQuantity[4 + page].ToString() , itemQuantityText); //Quantity
			}
			//------------------------------------------------------
			if (GUI.Button ( new Rect(30,405,60,60),itemSlot[5 + page].icon)){
				UseItem(itemSlot[5 + page]);
				if(itemQuantity[5 + page] > 0){
					itemQuantity[5 + page]--;
				}
				if(itemQuantity[5 + page] <= 0){
					itemSlot[5 + page] = null;
					itemQuantity[5 + page] = 0;
					AutoSortItem();
				}
			}
			GUI.Label ( new Rect(125, 415, 320, 75), itemSlot[5 + page].itemName.ToString() , itemNameText); //Item Name
			GUI.Label ( new Rect(125, 440, 320, 75), itemSlot[5 + page].description.ToString() , itemDescriptionText); //Item Description
			if(itemQuantity[5 + page] > 0){
				GUI.Label ( new Rect(73, 448, 40, 30), itemQuantity[5 + page].ToString() , itemQuantityText); //Quantity
			}
			//------------------------------------------------------
			
			if (GUI.Button ( new Rect(220,485,50,52), "1")) {
				page = 0;
			}
			if (GUI.Button ( new Rect(290,485,50,52), "2")) {
				page = itemPageMultiply;
			}
			if (GUI.Button ( new Rect(360,485,50,52), "3")) {
				page = itemPageMultiply *2;
			}
			
			GUI.Label ( new Rect(20, 505, 150, 50), "$ " + cash.ToString() , itemDescriptionText);
			//---------------------------
		}
		
		//---------------Equipment Tab----------------------------
		if(menu && equipMenu){
			//Close Window Button
			if (GUI.Button ( new Rect(windowRect.width -50 ,8,40,40), "X")) {
				OnOffMenu();
			}
			//Primary Weapon
			GUI.Label ( new Rect(60, 95, 150, 50), "Primary");			
			if (GUI.Button ( new Rect(50,20,70,70), primaryEquip.icon)){
				if(primaryEquip == null){
					return;
				}
				UnEquip(primaryEquip);
			}
			//Secondary Weapon
			GUI.Label ( new Rect(145, 95, 150, 50), "Secondary");			
			if (GUI.Button ( new Rect(140,20,70,70), secondaryEquip.icon)){
				if(secondaryEquip == null){
					return;
				}
				UnEquip(secondaryEquip);
			}
			//Melee Weapon
			GUI.Label ( new Rect(245, 95, 150, 50), "Melee");			
			if (GUI.Button ( new Rect(230,20,70,70), meleeEquip.icon)){
				if(meleeEquip == null){
					return;
				}
				UnEquip(meleeEquip);
			}
			//Armor
			GUI.Label ( new Rect(330, 95, 150, 50), "Armor");
			if (GUI.Button ( new Rect(320,20,70,70), armorEquip.icon)){
				if(armorEquip == null){
					return;
				}
				UnEquip(armorEquip);
				
			}
			//--------Equipment Slot---------
			if (GUI.Button ( new Rect(30,130,60,60),equipment[0 + page].icon)){
				EquipItem(equipment[0 + page] , 0 + page);
			}
			GUI.Label ( new Rect(125, 140, 320, 75), equipment[0 + page].itemName.ToString() , itemNameText); //Item Name
			GUI.Label ( new Rect(125, 165, 320, 75),equipment[0 + page].description.ToString() , itemDescriptionText); //Item Description
			
			if(equipment[0 + page] !=null)
				GUI.Label ( new Rect(33, 180, 60, 30),"Ammo: " + equipAmmo[0 + page].ToString() , itemQuantityText); //Ammo
			//------------------------------------------------------
			if (GUI.Button ( new Rect(30,205,60,60),equipment[1 + page].icon)){
				EquipItem(equipment[1 + page] , 1 + page);
			}
			GUI.Label ( new Rect(125, 215, 320, 75), equipment[1 + page].itemName.ToString() , itemNameText); //Item Name
			GUI.Label ( new Rect(125, 240, 320, 75), equipment[1 + page].description.ToString() , itemDescriptionText); //Item Description
			
			if(equipment[1 + page] != null)
				GUI.Label ( new Rect(33, 255, 60, 30),"Ammo: " + equipAmmo[1 + page].ToString() , itemQuantityText); //Ammo
			//------------------------------------------------------
			if (GUI.Button ( new Rect(30,280,60,60),equipment[2 + page].icon)){
				EquipItem(equipment[2 + page] , 2 + page);
			}
			GUI.Label ( new Rect(125, 290, 320, 75), equipment[2 + page].itemName.ToString() , itemNameText); //Item Name
			GUI.Label ( new Rect(125, 315, 320, 75), equipment[2 + page].description.ToString() , itemDescriptionText); //Item Description
			
			if(equipment[2 + page] != null)
				GUI.Label ( new Rect(33, 330, 60, 30),"Ammo: " + equipAmmo[2 + page].ToString() , itemQuantityText); //Ammo
			//------------------------------------------------------
			if (GUI.Button ( new Rect(30,355,60,60),equipment[3 + page].icon)){
				EquipItem(equipment[3 + page] , 3 + page);
			}
			GUI.Label ( new Rect(125, 365, 320, 75), equipment[3 + page].itemName.ToString() , itemNameText); //Item Name
			GUI.Label ( new Rect(125, 390, 320, 75), equipment[3 + page].description.ToString() , itemDescriptionText); //Item Description
			
			if(equipment[3 + page] != null)
				GUI.Label ( new Rect(33, 405, 60, 30),"Ammo: " + equipAmmo[3 + page].ToString() , itemQuantityText); //Ammo
			//------------------------------------------------------
			if (GUI.Button ( new Rect(30,430,60,60),equipment[4 + page].icon)){
				EquipItem(equipment[4 + page] , 4 + page);
			}
			GUI.Label ( new Rect(125, 440, 320, 75), equipment[4 + page].itemName.ToString() , itemNameText); //Item Name
			GUI.Label ( new Rect(125, 465, 320, 75), equipment[4 + page].description.ToString() , itemDescriptionText); //Item Description
			
			if(equipment[4 + page] != null)
				GUI.Label ( new Rect(33, 480, 60, 30),"Ammo: " + equipAmmo[4 + page].ToString() , itemQuantityText); //Ammo
			//------------------------------------------------------
			
			if (GUI.Button ( new Rect(220,485,50,52), "1")) {
				page = 0;
			}
			if (GUI.Button ( new Rect(290,485,50,52), "2")) {
				page = equipmentPageMultiply;
			}
			if (GUI.Button ( new Rect(360,485,50,52), "3")) {
				page = equipmentPageMultiply *2;
			}

			GUI.Label ( new Rect(20, 505, 150, 50), "$ " + cash.ToString() , itemDescriptionText);
			
		}
		GUI.DragWindow (new Rect (0,0,10000,10000)); 
	}

    public bool AddItem ( ItemData.Usable id ,  int quan  ){
	 bool  full = false;
	bool  geta = false;
		
	int pt = 0;
	while(pt < itemSlot.Length && !geta){
		if(itemSlot[pt] == id){
			itemQuantity[pt] += quan;
			geta = true;
		}else if(itemSlot[pt] == null){
			itemSlot[pt] = id;
			itemQuantity[pt] = quan;
			geta = true;
		}else{
			pt++;
			if(pt >= itemSlot.Length){
				full = true;
				print("Full");
			}
		}
		
	}
	
	return full;

}

    public bool AddEquipment ( ItemData.Equip id  ,   int ammo1  ){
	 bool  full = false;
	bool  geta = false;
	
	
	int pt = 0;
	while(pt < equipment.Length && !geta){
		if(equipment[pt] == null){
			equipment[pt] = id;
			equipAmmo[pt] = ammo1;
			geta = true;
		}else{
			pt++;
			if(pt >= equipment.Length){
				full = true;
				print("Full");
			}
		}
		
	}
	
	return full;

}
    //------------AutoSort----------
    public void AutoSortItem (){
		int pt = 0;
		int nextp = 0;
		bool  clearr = false;
	while(pt < itemSlot.Length){
		if(itemSlot[pt] == null){
			nextp = pt + 1;
			while(nextp < itemSlot.Length && !clearr){
				if(itemSlot[nextp] != null){
				//Fine Next Item and Set
					itemSlot[pt] = itemSlot[nextp];
					itemQuantity[pt] = itemQuantity[nextp];
					itemSlot[nextp] = null;
					itemQuantity[nextp] = 0;
					clearr = true;
				}else{
					nextp++;
				}
			
			}
		//Continue New Loop
			clearr = false;
			pt++;
		}else{
			pt++;
		}
		
	}

}

    public void AutoSortEquipment (){
		int pt = 0;
		int nextp = 0;
		bool  clearr = false;
	while(pt < equipment.Length){
		if(equipment[pt] == null){
			nextp = pt + 1;
			while(nextp < equipment.Length && !clearr){
				if(equipment[nextp] != null){
				//Fine Next Item and Set
					equipment[pt] = equipment[nextp];
					equipAmmo[pt] = equipAmmo[nextp];
					equipment[nextp] = null;
					equipAmmo[nextp] = 0;
					clearr = true;
				}else{
					nextp++;
				}
			
			}
		//Continue New Loop
			clearr = false;
			pt++;
		}else{
			pt++;
		}
		
	}

}
    public void OnOffMenu (){
	//Freeze Time Scale to 0 if Window is Showing
	if(!menu && Time.timeScale != 0.0f){
			menu = true;
			Time.timeScale = 0.0f;
			ResetPosition();
			//Screen.lockCursor = false;
	}else if(menu){
			menu = false;
			Time.timeScale = 1.0f;
			//Screen.lockCursor = true;
	}

}

    public void SetWeaponValue (ItemData.Equip id  ){
	int equipType = (int)id.equipmentType;
	StopCoroutine("Reload");
	if(equipType == 0){ // Primary Weapon
		GetComponent<GunTrigger>().primaryWeapon.attackDelay = ((ItemData.Weapon)id).attackDelay;
  		GetComponent<GunTrigger>().primaryWeapon.maxAmmo = ((ItemData.Weapon)id).maxAmmo;
  		GetComponent<GunTrigger>().primaryWeapon.useAmmo = ((ItemData.Weapon)id).useAmmo;
            if (((ItemData.Weapon)id).gunFireEffect)
                GetComponent<GunTrigger>().primaryWeapon.gunFireEffect = ((ItemData.Weapon)id).gunFireEffect;
  		GetComponent<GunTrigger>().primaryWeapon.cameraShakeValue = ((ItemData.Weapon)id).cameraShakeValue;
  		GetComponent<GunTrigger>().primaryWeapon.shootAnimationSpeed = ((ItemData.Weapon)id).attackAnimationSpeed;
            if (((ItemData.Weapon)id).gunSound)
                GetComponent<GunTrigger>().primaryWeapon.gunSound = ((ItemData.Weapon)id).gunSound;
            if (((ItemData.Weapon)id).reloadSound)
                GetComponent<GunTrigger>().primaryWeapon.reloadSound = ((ItemData.Weapon)id).reloadSound;
  		GetComponent<GunTrigger>().primaryWeapon.soundRadius = ((ItemData.Weapon)id).soundRadius;
  		
  		GetComponent<GunTrigger>().primaryWeapon.automatic = ((ItemData.Weapon)id).automatic;
  		GetComponent<GunTrigger>().primaryWeapon.animateWhileMoving = ((ItemData.Weapon)id).animateWhileMoving;
  		if(((ItemData.Weapon)id).aimIcon){
  			GetComponent<GunTrigger>().primaryWeapon.aimIcon = ((ItemData.Weapon)id).aimIcon;
  		}
            if (((ItemData.Weapon)id).zoomIcon)
                GetComponent<GunTrigger>().primaryWeapon.zoomIcon = ((ItemData.Weapon)id).zoomIcon;
  		GetComponent<GunTrigger>().primaryWeapon.zoomLevel = ((ItemData.Weapon)id).zoomLevel;
  	}
  	if(equipType == 1){ // Secondary Weapon
		GetComponent<GunTrigger>().secondaryWeapon.attackDelay = ((ItemData.Weapon)id).attackDelay;
  		GetComponent<GunTrigger>().secondaryWeapon.maxAmmo = ((ItemData.Weapon)id).maxAmmo;
  		GetComponent<GunTrigger>().secondaryWeapon.useAmmo = ((ItemData.Weapon)id).useAmmo;
            if (((ItemData.Weapon)id).gunFireEffect)
                GetComponent<GunTrigger>().secondaryWeapon.gunFireEffect = ((ItemData.Weapon)id).gunFireEffect;
  		GetComponent<GunTrigger>().secondaryWeapon.cameraShakeValue = ((ItemData.Weapon)id).cameraShakeValue;
  		GetComponent<GunTrigger>().secondaryWeapon.shootAnimationSpeed = ((ItemData.Weapon)id).attackAnimationSpeed;
            if (((ItemData.Weapon)id).gunSound)
                GetComponent<GunTrigger>().secondaryWeapon.gunSound = ((ItemData.Weapon)id).gunSound;
            if (((ItemData.Weapon)id).reloadSound)
                GetComponent<GunTrigger>().secondaryWeapon.reloadSound = ((ItemData.Weapon)id).reloadSound;
  		GetComponent<GunTrigger>().secondaryWeapon.soundRadius = ((ItemData.Weapon)id).soundRadius;
  		
  		GetComponent<GunTrigger>().secondaryWeapon.automatic = ((ItemData.Weapon)id).automatic;
  		GetComponent<GunTrigger>().secondaryWeapon.animateWhileMoving = ((ItemData.Weapon)id).animateWhileMoving;
  		if(((ItemData.Weapon)id).aimIcon){
  			GetComponent<GunTrigger>().secondaryWeapon.aimIcon = ((ItemData.Weapon)id).aimIcon;
  		}
            if (((ItemData.Weapon)id).zoomIcon)
                GetComponent<GunTrigger>().secondaryWeapon.zoomIcon = ((ItemData.Weapon)id).zoomIcon;
  		GetComponent<GunTrigger>().secondaryWeapon.zoomLevel = ((ItemData.Weapon)id).zoomLevel;
  	}
  	if(equipType == 2){ // Melee Weapon
  		GetComponent<GunTrigger>().meleeAttack.meleeAnimationSpeed = ((ItemData.Weapon)id).attackAnimationSpeed;
  		GetComponent<GunTrigger>().meleeAttack.meleeCast = ((ItemData.Weapon)id).attackCast;
  		GetComponent<GunTrigger>().meleeAttack.meleeDelay = ((ItemData.Weapon)id).attackDelay;
            if (((ItemData.Weapon)id).gunSound)
                GetComponent<GunTrigger>().meleeAttack.meleeSound = ((ItemData.Weapon)id).gunSound;
  	}
  	GetComponent<GunTrigger>().SettingWeapon();
}

    public void AssignWeaponAnimation ( ItemData.Equip id  ){
	int equipType = (int)id.equipmentType;
	
	//Assign Attack Animation for Primary Weapon
	if(equipType == 0){
	  	GetComponent<GunTrigger>().primaryWeapon.shootAnimation = ((ItemData.Weapon)id).attackAnimation[0];

	 	if(((ItemData.Weapon)id).reloadAnimation){
	  		GetComponent<GunTrigger>().primaryWeapon.reloadAnimation = ((ItemData.Weapon)id).reloadAnimation;
	 	}
	 	if(((ItemData.Weapon)id).equipAnimation){
	  		GetComponent<GunTrigger>().primaryWeapon.equipAnimation = ((ItemData.Weapon)id).equipAnimation;
	 	}
 	}
 	
 	//Assign Attack Animation for Secondary Weapon
	if(equipType == 1){
	  	GetComponent<GunTrigger>().secondaryWeapon.shootAnimation = ((ItemData.Weapon)id).attackAnimation[0];

	 	if(((ItemData.Weapon)id).reloadAnimation){
	  		GetComponent<GunTrigger>().secondaryWeapon.reloadAnimation = ((ItemData.Weapon)id).reloadAnimation;
	 	}
	 	if(((ItemData.Weapon)id).equipAnimation){
	  		GetComponent<GunTrigger>().secondaryWeapon.equipAnimation = ((ItemData.Weapon)id).equipAnimation;
	 	}
 	}
 	
	//--------------------------------------------------------
	
	//Assign All Attack Combo Animation for Melee
	if(equipType == 2 && ((ItemData.Weapon)id).attackAnimation.Length > 0){
  		int allPrefab = ((ItemData.Weapon)id).attackAnimation.Length;
  		GetComponent<GunTrigger>().meleeAttack.meleeAnimation = new AnimationClip[allPrefab];
  		GetComponent<GunTrigger>().c = 0;
  			
  		int a = 0;
  		if(allPrefab > 0){
   			while(a < allPrefab){
    			GetComponent<GunTrigger>().meleeAttack.meleeAnimation[a] = ((ItemData.Weapon)id).attackAnimation[a];
    			a++;
   			}
  		}
  		
 	}
 	
 	PlayerLegacyAnimation playerAnim = GetComponent<PlayerLegacyAnimation>();
	if(GetComponent<GunTrigger>().useMecanim){
		//If use Mecanim
		AssignMecanimAnimation(id , equipType);
		return;
	}
	//--------------------------------------------------------
	//equipType , 0 = Primary , 1 = Secondary
	if(equipType <= 1){//Assign Idle and Move Animation if it's a Primary or Secondary Weapon
		if(((ItemData.Weapon)id).legacyAnimationSet.idle){
			playerAnim.weaponAnimSet[equipType].idle = ((ItemData.Weapon)id).legacyAnimationSet.idle;
		}
		if(((ItemData.Weapon)id).legacyAnimationSet.run){
			playerAnim.weaponAnimSet[equipType].run = ((ItemData.Weapon)id).legacyAnimationSet.run;
		}
		if(((ItemData.Weapon)id).legacyAnimationSet.right){
			playerAnim.weaponAnimSet[equipType].right = ((ItemData.Weapon)id).legacyAnimationSet.right;
		}
		if(((ItemData.Weapon)id).legacyAnimationSet.left){
			playerAnim.weaponAnimSet[equipType].left = ((ItemData.Weapon)id).legacyAnimationSet.left;
		}
		if(((ItemData.Weapon)id).legacyAnimationSet.back){
			playerAnim.weaponAnimSet[equipType].back = ((ItemData.Weapon)id).legacyAnimationSet.back;
		}
		if(((ItemData.Weapon)id).legacyAnimationSet.jump){
			playerAnim.weaponAnimSet[equipType].jump = ((ItemData.Weapon)id).legacyAnimationSet.jump;
		}
		if(((ItemData.Weapon)id).legacyAnimationSet.hurt){
			playerAnim.weaponAnimSet[equipType].hurt = ((ItemData.Weapon)id).legacyAnimationSet.hurt;
		}
		if(((ItemData.Weapon)id).legacyAnimationSet.crouchIdle){
			playerAnim.weaponAnimSet[equipType].crouchIdle = ((ItemData.Weapon)id).legacyAnimationSet.crouchIdle;
		}
		if(((ItemData.Weapon)id).legacyAnimationSet.crouchForward){
			playerAnim.weaponAnimSet[equipType].crouchForward = ((ItemData.Weapon)id).legacyAnimationSet.crouchForward;
		}
		if(((ItemData.Weapon)id).legacyAnimationSet.crouchRight){
			playerAnim.weaponAnimSet[equipType].crouchRight = ((ItemData.Weapon)id).legacyAnimationSet.crouchRight;
		}
		if(((ItemData.Weapon)id).legacyAnimationSet.crouchLeft){
			playerAnim.weaponAnimSet[equipType].crouchLeft = ((ItemData.Weapon)id).legacyAnimationSet.crouchLeft;
		}
		if(((ItemData.Weapon)id).legacyAnimationSet.crouchBack){
			playerAnim.weaponAnimSet[equipType].crouchBack = ((ItemData.Weapon)id).legacyAnimationSet.crouchBack;
		}
	}
	playerAnim.SetAnimation();

}

    public void AssignMecanimAnimation ( ItemData.Equip id  ,   int type  ){
	//Set Weapon Type ID to Mecanim Animator and Set New Idle
	if(type == 0){
		GetComponent<PlayerMecanimAnimation>().primaryWeaponType = id.weaponAnimationType;
	}else if(type == 1){
		GetComponent<PlayerMecanimAnimation>().secondaryWeaponType = id.weaponAnimationType;
	}
	GetComponent<PlayerMecanimAnimation>().SetAnimation();
	GetComponent<PlayerMecanimAnimation>().SetIdle();
}

    public void ResetPosition (){
		//Reset GUI Position when it out of Screen.
		if(windowRect.x >= Screen.width -30 || windowRect.y >= Screen.height -30 || windowRect.x <= -70 || windowRect.y <= -70 ){
			windowRect = originalRect;
		}
}

    public void RemoveWeaponMesh (){
	if(primaryEquip == null){
	//	gun.primaryWeapon.bulletPrefab = null;
		if(gun.primaryWeaponModel){
  			gun.primaryWeaponModel.SetActive(false);
  		}
  		//------------------------
	}
	if(secondaryEquip == null){
		//gun.secondaryWeapon.bulletPrefab = null;
		if(gun.secondaryWeaponModel){
  			gun.secondaryWeaponModel.SetActive(false);
  		}
		//--------------------
	}
	if(meleeEquip == null){
		gun.meleeAttack.meleePrefab = null;
		if(gun.meleeWeaponModel){
  			gun.meleeWeaponModel.SetActive(false);
  		}
		//--------------------
	}
}

    public void AddAmmo(int amout,AmmoType ammo)
    {
        GunTrigger gun = GetComponent<GunTrigger>();

        if (ammo == AmmoType.Handgun)
            gun.allAmmo.handgunAmmo += amout;
        if (ammo == AmmoType.Machinegun)
            gun.allAmmo.machinegunAmmo += amout;
        if (ammo == AmmoType.Magnum)
            gun.allAmmo.magnumAmmo += amout;
        if (ammo == AmmoType.Shotgun)
            gun.allAmmo.shotgunAmmo += amout;
        if (ammo == AmmoType.SMG)
            gun.allAmmo.smgAmmo += amout;
        if (ammo == AmmoType.SniperRifle)
            gun.allAmmo.sniperRifleAmmo += amout;
        if (ammo == AmmoType.GrenadeRounds)
            gun.allAmmo.grenadeRounds += amout;
    }
}