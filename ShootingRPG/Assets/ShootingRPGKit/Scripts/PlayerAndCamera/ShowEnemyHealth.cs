using UnityEngine;
using System.Collections;
using Photon.Pun;

public class ShowEnemyHealth : MonoBehaviour {

public Texture2D border;
    public Texture2D hpBar;
    public Texture2D shieldBar;
private string enemyName = "";
    public float duration = 7.0f;
private bool  show = false;

    public int borderWidth = 200;
    public int borderHeigh = 26;
    public int hpBarHeight = 20;
    public float hpBarY = 28.0f;
    public float barMultiply = 1.8f;
private float hpBarWidth;

    public GUIStyle textStyle;

private int maxHp;
private float hp;
private int maxShield;
private float shield;
private float wait;
private GameObject target;
private string lv = "";

void Start (){
	hpBarWidth = 100 * barMultiply;
}

void Update (){
        if (!GetComponent<CharakterSync>().isMine())
            return;

        if (show){
	  	if(wait >= duration){
	       show = false;
	     }else{
	      	wait += Time.deltaTime;
	     }
	 
	 }
	 if(show && !target){
	 	hp = 0;
	 	shield = 0;
	 }else if(show && target){
	 	hp = target.GetComponent<status>().health;
	 	shield = target.GetComponent<status>().shield;
	 }

}

void OnGUI (){
        if (!GetComponent<CharakterSync>().isMine())
            return;

        if (show){
		int hpPercent = (int)(hp * 100 / maxHp *barMultiply);
		
		GUI.DrawTexture( new Rect(Screen.width /2 - borderWidth /2 , 25 , borderWidth, borderHeigh), border);
    	GUI.DrawTexture( new Rect(Screen.width /2 - hpBarWidth /2 , hpBarY , hpPercent, hpBarHeight), hpBar);
    	
    	if(maxShield > 0){
			int shieldPercent = (int)(shield * 100 / maxShield *barMultiply);
			GUI.DrawTexture( new Rect(Screen.width /2 - hpBarWidth /2 , hpBarY , shieldPercent, hpBarHeight), shieldBar);
		}
    	
    	GUI.Label ( new Rect(Screen.width /2 - hpBarWidth /2 , hpBarY, hpBarWidth, hpBarHeight), enemyName + " (" + lv + ")" , textStyle); //Draw Enemy's name and level.
	
	}

}

    public void GetHP ( int mhealth  ,   GameObject mon  ,   string monName  ){
	maxHp = mhealth;
	maxShield = mon.GetComponent<status>().maxShield;
	target = mon;
	enemyName = monName;
	wait = 0;
	lv = mon.GetComponent<status>().level.ToString();
	show = true;

}
}