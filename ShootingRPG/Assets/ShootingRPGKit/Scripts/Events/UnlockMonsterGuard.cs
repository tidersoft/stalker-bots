using UnityEngine;
using System.Collections;

public class UnlockMonsterGuard : MonoBehaviour {

//Use with Trigger to unlock the guard of monster.
GameObject target;

void OnTriggerEnter ( Collider other  ){
	if (other.gameObject.tag == "Player") {
		target.GetComponent<status>().guard = false;
     }
}
}