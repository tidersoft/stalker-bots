using UnityEngine;
using System.Collections;

public class HelpHostage : MonoBehaviour {

    public Transform hostage;
[HideInInspector]
    public GameObject player;

    public GameObject operatorObj;
    public OperatorSet[] operatorMessage = new OperatorSet[1];

    public GameObject[] activateObj;

void Start (){
	if(!hostage){
		hostage = transform.root;
	}
	hostage.GetComponent<AIfriend>().enabled = false;
}

    public void Rescue (){
	player = GetComponent<Dialogue>().player;
	if(!player){
		player = GameObject.FindWithTag("Player");
	}
	hostage.GetComponent<AIfriend>().master = player.transform;
	hostage.GetComponent<AIfriend>().enabled = true;
	
	AllyHealthBar hb = hostage.GetComponent<AllyHealthBar>();
	if(hb){
		hb.enabled = true;
	}
	
	if(!operatorObj){
		operatorObj = GameObject.Find("Mission");
	}
	operatorObj.GetComponent<Operator>().otherMessage = operatorMessage;
	operatorObj.GetComponent<Operator>().ShowOtherMessage();
	if(activateObj.Length > 0){
		ActivateObject();
	}
	Destroy(gameObject);
}

    public void ActivateObject (){
	foreach(GameObject ob in activateObj) {
    	ob.SetActive(true);
    }
}
    

}