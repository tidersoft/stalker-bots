using UnityEngine;
using System.Collections;

public class PrisonDoor : MonoBehaviour {

    public Transform door;
    public Vector3 doorMoveDirection = Vector3.down;
    public float moveSpeed = 4.0f;
    public float moveDuration = 3.0f;
private bool  opened = false;
private bool  done = false;
    public GameObject missionObj;

    public TextDialogue[] message = new TextDialogue[1];

void Start (){
	if(!missionObj){
		missionObj = GameObject.Find("Mission");
	}
	if(!door){
		door = transform.root;
	}
}

void Update (){
	if(opened && !done){
		door.Translate(doorMoveDirection * moveSpeed * Time.deltaTime);
	}
}

    public IEnumerator Open (){
	if(done || opened){
		yield return false;
	}
	if(missionObj.GetComponent<RescueMission>().prisonUnlock){
		opened = true;
		yield return new WaitForSeconds(moveDuration);
		done = true;
		opened = false;
		Destroy(gameObject);
	}else{
		GetComponent<Dialogue>().message = message;
		//GetComponent<Dialogue>().NextPage();
	}

}
    

}