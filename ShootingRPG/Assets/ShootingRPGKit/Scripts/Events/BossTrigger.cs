using UnityEngine;
using System.Collections;

public class BossTrigger : MonoBehaviour {

GameObject boss;
GameObject[] destroyObj;

void OnTriggerEnter ( Collider other  ){
	if (other.gameObject.tag == "Player") {
		boss.GetComponent<status>().guard = false;
		boss.GetComponent<AIenemy>().followState = 0;
		if(destroyObj.Length > 0){
			foreach(GameObject ob in destroyObj) {
		    	Destroy(ob.gameObject);
		    }
		}
     }
}
}