using UnityEngine;
using System.Collections;

public class DoorTrigger : MonoBehaviour {

    public Transform door;
    public Vector3 doorMoveDirection = Vector3.down;
    public float moveSpeed = 4.0f;
    public float moveDuration = 3.0f;
private bool  opened = false;
private bool  done = false;

void Start (){
	if(!door){
		door = transform.root;
	}
}

void Update (){
	if(opened && !done){
		door.Translate(doorMoveDirection * moveSpeed * Time.deltaTime);
	}
}

void OnTriggerEnter ( Collider other  ){
	if(other.tag == "Player"){
		Open();
	}

}

    public IEnumerator Open (){
	if(done || opened){
            yield return false;
	}
	opened = true;
	yield return new WaitForSeconds(moveDuration);
	done = true;
	opened = false;
	Destroy(gameObject);

}

}