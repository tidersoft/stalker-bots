using UnityEngine;
using System.Collections;
[System.Serializable]
public class OperatorSet
{
    public string message;
    public int pictureId = 0;
}

public class Operator : MonoBehaviour {

    public Texture2D[] operatorPicture = new Texture2D[3];
    private Texture2D showPicture;
    public string showText;
    public GUISkin skin;
    [SerializeField]
    public OperatorSet[] startMessage = new OperatorSet[1];
    [SerializeField]
    public OperatorSet[] clearMissionMessage = new OperatorSet[1];
    [SerializeField]
    public OperatorSet[] otherMessage = new OperatorSet[1];


    public int[] talkPicId = new int[2] ;
    public float textDuration = 4.5f;

private bool  show = false;

   

IEnumerator Start (){
	yield return new WaitForSeconds(1.2f);
	show = false;
	show = true;
	int i = 0;
	while(i < startMessage.Length && show){
	StartCoroutine(	AnimateText(startMessage[i].message,startMessage[i].pictureId));
		yield return new WaitForSeconds(textDuration);
		i++;
	}
	show = false;
}

void Update (){

}

void OnGUI (){
	if(show){
		if(showPicture)
			GUI.DrawTexture( new Rect(Screen.width - 285, Screen.height /2 - 150, 200, 200), showPicture);
		
		GUI.skin = skin;
		GUI.Box(new Rect(Screen.width - 330, Screen.height /2 + 110 ,300,40), showText);
	}
}
    public void MissionClear()
    {
        StartCoroutine(Clear());
    }

    public IEnumerator Clear (){
	show = false;
	show = true;
	int i = 0;
	while(i < clearMissionMessage.Length && show){
	StartCoroutine(	AnimateText(clearMissionMessage[i].message,clearMissionMessage[i].pictureId));
		yield return new WaitForSeconds(textDuration);
		i++;
	}
	show = false;
}

    public void ShowOtherMessage (){
	//Call other function to prevent yield problem when calling from other object.
StartCoroutine(	OtherMessage());
}

    public IEnumerator OtherMessage (){
	show = false;
	show = true;
	int i = 0;
  	while(i < otherMessage.Length && show){
		StartCoroutine(AnimateText(otherMessage[i].message,otherMessage[i].pictureId));
		yield return new WaitForSeconds(textDuration);
		i++;
       
	}
	show = false;
}
    public IEnumerator AnimateText ( string strComplete  ,int pic){
	int i = 0;
	showText = "";
        int anim = 0;
        float delay = 0.05f;
	while(i < strComplete.Length){
		showText += strComplete[i++];
            showPicture = operatorPicture[talkPicId[anim]];

            yield return new WaitForSeconds(0.05f);
            delay += 0.05f;
            if (delay > 0.1f)
            {
                anim++;
                delay = 0;
            }
            if (anim > 1)
                anim = 0;
        }
        showPicture = operatorPicture[pic];


    }


}