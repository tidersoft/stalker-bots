using UnityEngine;
using System.Collections;

public class OperatorTrigger : MonoBehaviour {

GameObject operatorObj;
OperatorSet[] message = new OperatorSet[1];

void OnTriggerEnter ( Collider other  ){
	if (other.gameObject.tag == "Player") {
		operatorObj.GetComponent<Operator>().otherMessage = message;
		operatorObj.GetComponent<Operator>().ShowOtherMessage();
		Destroy(gameObject);
     }
}
 
}