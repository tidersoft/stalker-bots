using UnityEngine;
using System.Collections;

public class Keycard : MonoBehaviour {

//Use with Rescue Mission.
private GameObject mission;
private Transform master;

void OnTriggerEnter ( Collider other  ){
		//Pick up Item
	if (other.gameObject.tag == "Player") {
		mission = GameObject.Find("Mission");
		if(mission){
			mission.GetComponent<RescueMission>().prisonUnlock = true;
		}
		master = transform.root;
    	Destroy(master.gameObject);
     }
 }
}