using UnityEngine;
using System.Collections;

public class BulletShot : MonoBehaviour {

    public float speed = 20;
    public Vector3 relativeDirection = Vector3.forward;
    public float duration = 1.0f;
private GameObject hitEffect;
private bool  bomb;

void Start (){
	GetComponent<Rigidbody>().isKinematic = true;
	hitEffect = GetComponent<BulletColider>().hitEffect;
	bomb = GetComponent<BulletColider>().bombHitSetting.enable;
	GetComponent<Collider>().isTrigger = true;
	Destroy();
}
void Update (){
    transform.Translate(relativeDirection * speed * Time.deltaTime);
}

    public void Destroy (){
	Destroy (gameObject, duration);

}
void OnTriggerEnter ( Collider other  ){

  if (other.gameObject.tag == "Wall") {
		if(hitEffect){
			Instantiate(hitEffect, transform.position , transform.rotation);
		}
		if(bomb){
			GetComponent<BulletColider>().ExplosionDamage();
		}
    	Destroy (gameObject);
    	
	}
}

}