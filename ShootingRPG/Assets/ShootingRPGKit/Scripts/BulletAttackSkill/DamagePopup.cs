using UnityEngine;
using System.Collections;

public class DamagePopup : MonoBehaviour {

public Vector3 targetScreenPosition;
public string damage = "";
public GUIStyle fontStyle;
public GUIStyle criticalFontStyle;

public float duration = 0.5f;

private int glide = 65;

public bool  critical = false;
  

IEnumerator Start (){
	Destroy (gameObject, duration);
   
	int a = 0;
	while(a < 100){
		glide += 2;
		yield return new WaitForSeconds(0.03f); 
	}
}

void OnGUI (){
       
            targetScreenPosition = Camera.main.WorldToScreenPoint(new Vector3(transform.position.x,transform.position.y+transform.lossyScale.y,transform.position.z));
            targetScreenPosition.y = Screen.height - targetScreenPosition.y - glide;
            targetScreenPosition.x = targetScreenPosition.x - 6;
            if (targetScreenPosition.z > 0)
            {
                if (critical)
                {
                    GUI.Label(new Rect(targetScreenPosition.x, targetScreenPosition.y, 100, 50), damage, criticalFontStyle);
                }
                else
                {
                    GUI.Label(new Rect(targetScreenPosition.x, targetScreenPosition.y, 100, 50), damage, fontStyle);
                }
            }
      
}
}