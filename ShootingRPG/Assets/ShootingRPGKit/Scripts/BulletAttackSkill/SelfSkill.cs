using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelfSkill : MonoBehaviour {

    public Transform Popup;

  
      public List<int> bufs;
    public string shooterTag = "Player";
    public GameObject hitEffect;
private GameObject target;

void Start (){
			target = GetComponent<BulletColider>().shooter;
			ApplyEffect();
}

void ApplyEffect (){
		
		if(hitEffect){
    		Instantiate(hitEffect, target.transform.position , hitEffect.transform.rotation);
 		}

        //Call Function ApplyBuff in Status Script
        bufs.ForEach(index => {
            SkillData.BuffEfect buf = SkillData.instance.Buffs[index];
            target.GetComponent<status>().ApplyBuff(buf.type, buf.dur, buf.amount);

        });
           
        
       //.ApplyBuff(buffs ,statusDuration , statusAmount);
			Destroy(gameObject);
}
    
}