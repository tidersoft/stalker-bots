﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletForce : MonoBehaviour {


    public float Force;
    private GameObject hitEffect;
    private bool bomb;

    // Use this for initialization
    void Start () {
        hitEffect = GetComponent<BulletColider>().hitEffect;
        bomb = GetComponent<BulletColider>().bombHitSetting.enable;

        Vector3 vec = transform.forward * Force;
        GetComponent<Rigidbody>().AddForce(vec);	
	}
    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Wall")
        {
            if (hitEffect)
            {
                Instantiate(hitEffect, transform.position, transform.rotation);
            }
            if (bomb)
            {
                GetComponent<BulletColider>().ExplosionDamage();
            }
            Destroy(gameObject);

        }
    }
}
