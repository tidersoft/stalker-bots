using UnityEngine;
using System.Collections;
using Photon.Pun;

public class BulletStatus : MonoBehaviour {

    public int damage = 10;
    public int damageMax = 20;

public int playerAttack = 5;
    public int totalDamage = 0;
    public int variance = 15;
    public string shooterTag = "Player";

public GameObject shooter;

    public Transform Popup;

    public float traceStart = 0.01f;
    public Color traceColor= Color.yellow;

    public GameObject hitEffect;
    public bool flinch = false;
    public bool penetrate = false;
    public Vector3 parentV; 
    public bool ignoreGuard = false;

    public bool drawTracer = true;
private string popDamage = "";

    public enum AtkType {
	Physic = 0,
	Magic = 1,
}

    public AtkType AttackType = AtkType.Physic;

    public enum Elementala
    {
	Normal = 0,
	Fire = 1,
	Ice = 2,
	Earth = 3,
	Lightning = 4,
}
    public Elementala element = Elementala.Normal;
    [System.Serializable]
    public class BombHit
    {
	public bool  enable = false;
	public GameObject bombEffect;
        public float bombRadius = 20;
}
    [SerializeField]
    public BombHit bombHitSetting;

    public GameObject smokeEfect;

    void Start()
    {
        gameObject.layer = 2;
        if (variance >= 100)
        {
            variance = 100;
        }
        if (variance <= 1)
        {
            variance = 1;
        }
      
    }

public void Setting ( int str  ,   int mag  ,   string tag  ,   GameObject owner  ,Vector3 vec,Vector3 parent_vec){
	if(AttackType == AtkType.Physic){
		playerAttack = str;
	}else{
		playerAttack = mag;
	}
        parentV = parent_vec;
	shooterTag = tag;
	shooter = owner;
	int varMin = 100 - variance;
	int varMax = 100 + variance;
	int randomDmg = Random.Range(damage, damageMax);
	totalDamage = (randomDmg + playerAttack) * Random.Range(varMin ,varMax) / 100;
        RaycastHit hit;
        Vector3 targetpos = transform.forward +vec  ;
       // targetpos *= 100000.0f;
       
        if (Physics.Raycast(transform.position, vec, out hit, 1000.0f))
        {
             print("Found an object - distance: " + hit.distance + " " + hit.collider.transform.name);
            TriggerEnter(hit.collider,hit.point);
            targetpos = hit.point;
            Debug.DrawRay(transform.position, vec*hit.distance, Color.red, 60);

           
            if (hit.collider.gameObject.tag == "Wall")
            {

               
                if (hitEffect!=null)
                {
                    Instantiate(hitEffect, hit.point,Quaternion.FromToRotation( hit.point,transform.position));
                }
                if (bombHitSetting.enable)
                {
                    ExplosionDamage();
                }
                Destroy(gameObject);

            }

        }
        if (drawTracer)
        {
            GameObject line = new GameObject();
            //line.transform.SetParent(transform.root);
            line.name = "tracer";
            LineRenderer ln = line.AddComponent<LineRenderer>();
            ln.startWidth = traceStart;
            ln.endWidth = traceStart;
            ln.material = new Material(Shader.Find("Diffuse"));
            ln.material.color = traceColor;
            ln.SetPosition(0, transform.position);
            ln.SetPosition(1, targetpos);
            Destroy(line, 0.05f);
        }
      

    }

void TriggerEnter ( Collider other ,Vector3 vec ){  	
    //When Player Shoot at Enemy		   
    if(shooterTag == "Player" && other.tag == "Enemy"){
    	DamageToEnemy(other.transform,vec);
    	if(bombHitSetting.enable){
    		ExplosionDamage();
    	}
		//When Enemy Shoot at Player
    }else if(shooterTag == "Enemy" && other.tag == "Player" || shooterTag == "Enemy" && other.tag == "Ally"){  	
		DamageToPlayer(other.transform,vec);
		if(bombHitSetting.enable){
    		ExplosionDamage();
    	}
    }else if(shooterTag == "Player" && other.tag == "WeakPoint"){ 
    	DamageWeakPoint(other.transform,vec);
    	if(bombHitSetting.enable){
    		ExplosionDamage();
    	}
    }else if(other.tag == "Breakable"){ 
    	DamageToEnemy(other.transform,vec);
    	if(bombHitSetting.enable){
    		ExplosionDamage();
    	}
    }
}

public void DamageToEnemy ( Transform other ,Vector3 point ){
        if (Popup != null)
        {
            Transform dmgPop = Instantiate(Popup, other.transform.position, transform.rotation);

            if (AttackType == AtkType.Physic)
            {
                popDamage = other.GetComponent<status>().OnDamage(totalDamage, (int)element, ignoreGuard, shooter.transform);
            }
            else
            {
                popDamage = other.GetComponent<status>().OnMagicDamage(totalDamage, (int)element, ignoreGuard);
            }
            dmgPop.GetComponent<DamagePopup>().damage = popDamage;

        }
        if (shooter && shooter.GetComponent<ShowEnemyHealth>()){
    		shooter.GetComponent<ShowEnemyHealth>().GetHP(other.GetComponent<status>().maxHealth , other.gameObject , other.name);
    	}
		
		if(hitEffect){
    		Instantiate(hitEffect, point, Quaternion.FromToRotation(point, transform.position));
 		}
 		if(flinch){
 		 	other.GetComponent<status>().Flinch();
 		}
		if(!penetrate){
 		 	Destroy (gameObject);
 		}
}

public void DamageToPlayer ( Transform other ,Vector3 point ){
		if(AttackType == AtkType.Physic){
			popDamage = other.GetComponent<status>().OnDamage(totalDamage , (int)element , ignoreGuard,shooter.transform);
		}else{
			popDamage = other.GetComponent<status>().OnMagicDamage(totalDamage , (int)element , ignoreGuard);
		}
        if (Popup != null)
        {
            Transform dmgPop = Instantiate(Popup, other.transform.position, other.transform.rotation);

            dmgPop.GetComponent<DamagePopup>().damage = popDamage;
        }
  		 if(hitEffect){
    		Instantiate(hitEffect, point, Quaternion.FromToRotation(point, transform.position));
 		 }
 		 if(flinch){
 		 	other.GetComponent<status>().Flinch();
 		 }
 		 if(!penetrate){
 		 	 Destroy (gameObject);
 		 }
}

public void ExplosionDamage (){
		Collider[] hitColliders= Physics.OverlapSphere(transform.position, bombHitSetting.bombRadius);
		if(bombHitSetting.bombEffect){
    		Instantiate(bombHitSetting.bombEffect , transform.position , transform.rotation);
 		}
 		 
		for (int i = 0; i < hitColliders.Length; i++) {
			if(shooterTag == "Player" && hitColliders[i].tag == "Enemy"){	  
		    	DamageToEnemy(hitColliders[i].transform,hitColliders[i].transform.position);
		    }else if(shooterTag == "Enemy" && hitColliders[i].tag == "Player" || shooterTag == "Enemy" && hitColliders[i].tag == "Ally"){  	
				DamageToPlayer(hitColliders[i].transform, hitColliders[i].transform.position);
		    }
		}
}

public void DamageWeakPoint ( Transform mon ,Vector3 point ){
	if(!mon.GetComponent<WeakPoint>()){
		return;
	}
	Transform other = mon.GetComponent<WeakPoint>().master;
	int realDamage = (int)(totalDamage * mon.GetComponent<WeakPoint>().damageMultiply);
	bool  igg = mon.GetComponent<WeakPoint>().ignoreGuard;
        if (Popup != null)
        {
            Transform dmgPop = Instantiate(Popup, other.transform.position, transform.rotation);

            if (AttackType == AtkType.Physic)
            {
                popDamage = other.GetComponent<status>().OnDamage(realDamage, (int)element, igg, shooter.transform);
            }
            else
            {
                popDamage = other.GetComponent<status>().OnMagicDamage(realDamage, (int)element, igg);
            }
            dmgPop.GetComponent<DamagePopup>().damage = popDamage;
            dmgPop.GetComponent<DamagePopup>().critical = mon.GetComponent<WeakPoint>().isCritical;

        }
        if (shooter && shooter.GetComponent<ShowEnemyHealth>()){
    		shooter.GetComponent<ShowEnemyHealth>().GetHP(other.GetComponent<status>().maxHealth , other.gameObject , other.name);
    	}
		
		if(hitEffect){
    		Instantiate(hitEffect, point, Quaternion.FromToRotation(point, transform.position));
 		}
 		if(flinch){
 		 	other.GetComponent<status>().Flinch();
 		}
		if(!penetrate){
 		 Destroy (gameObject);
 		}

}
	
}