﻿using GooglePlayGames;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour {

   public  GameObject prefab;
   public  string folder;
	// Use this for initialization
	void Start () {
        string prefabName=folder+"/"+prefab.name;
        if (PlayGamesPlatform.Instance.RealTime.GetConnectedParticipants().IndexOf(PlayGamesPlatform.Instance.RealTime.GetSelf()) == 1 || GameMenager.Instance.isLocalGame) {
            Instantiate(prefab,transform.position,transform.rotation);
        }

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
