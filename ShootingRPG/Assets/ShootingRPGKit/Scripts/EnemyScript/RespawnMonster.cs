using UnityEngine;
using System.Collections;

public class RespawnMonster : MonoBehaviour {

public Transform enemy;
    //float point = 10;
    public string pointName = "SpawnPoint";
    public float delay = 3.0f;
    public float randomPoint = 10.0f;

IEnumerator Start (){
	GameObject[] spawnpoints = GameObject.FindGameObjectsWithTag (pointName);
	if(spawnpoints.Length > 0){
			Transform spawnpoint = spawnpoints[Random.Range(0, spawnpoints.Length)].transform;
		
			yield return new WaitForSeconds (delay);
			Vector3 ranPos = spawnpoint.position; //Slightly Random x z position from respawn point.
			ranPos.x += Random.Range(0.0f,randomPoint);
			ranPos.z += Random.Range(0.0f,randomPoint);
			Transform m = Instantiate(enemy, ranPos , spawnpoint.rotation);
			m.name = enemy.name;
			Destroy (gameObject, 1);
	}else{
			Destroy (gameObject, delay+1);
	}
	
}
}