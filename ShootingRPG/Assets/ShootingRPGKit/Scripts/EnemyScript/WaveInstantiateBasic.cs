using UnityEngine;
using System.Collections;

public class WaveInstantiateBasic : MonoBehaviour {

    public GameObject[] monsterPrefab = new GameObject[3]; //Store All prefab of Monster.
    public string spawnpointTag = "SpawnPoint"; //Spawn Point Tag where we will spawn monster.
    public float randomPoint = 10.0f; //For slightly Random position from respawn point.
    public string setTagTo = "Enemy"; //Set tag of object we spawn.

    public class WaveBasic
    {
        public string waveName = "";
        public int[] spawnStep = new int[30]; //Spawn Monster from ID.
        public int maxMonster = 5;
        public float spawnDelay = 2.0f;
        public string sendMessage = ""; //Send Message to Object after spawn.
        public int sendValue = 0;
}

    public WaveBasic[] waveManage = new WaveBasic[5];

    public int wave = 0; //Current Wave
    public bool begin = false;
private int maxStep;
private int step;
private float wait = 0;
private bool  freeze = false;
private bool  showGui = false;
private string showText = "";
    public string completeSendMessage = "MissionClear";

    public GameObject[] ignoreObject; //Store the GameObject that Monster will ignore it's collision

    public GUISkin uiSkin;

IEnumerator Start (){
	maxStep = waveManage[wave].spawnStep.Length;
	yield return new WaitForSeconds(10);
	BeginWave();
}

    public IEnumerator BeginWave (){
	int w = wave + 1;
	showText = "Wave " + w.ToString();
	showGui = true;
	yield return new WaitForSeconds(4.5f);
	begin = true;
	showGui = false;
}

void OnGUI (){
	if(showGui){ //Draw Current Wave GUI
		GUI.skin = uiSkin;
		GUI.Box ( new Rect(Screen.width /2 -120 ,Screen.height /2 -40 ,240,80), showText);
	}
}

void Update (){
	if(!begin || freeze){
		return;
	}
	
	if(step >= maxStep){ //Check After we spawn all of objects in current wave.
		GameObject[] h = GameObject.FindGameObjectsWithTag(setTagTo);
		if(h.Length <= 0){
			WaveClear();
		}
		return;
	}
	
	if(wait >= waveManage[wave].spawnDelay){
		SpawnMon();
		wait = 0;
	}else{
		wait += Time.deltaTime;
	}
}

    public void SpawnMon (){
	GameObject[] h = GameObject.FindGameObjectsWithTag(setTagTo);
	if(h.Length > waveManage[wave].maxMonster){ // Not spawn if current monsters more than Max Monster
		return;
	}
	
	GameObject[] spawnpoints = GameObject.FindGameObjectsWithTag (spawnpointTag);
	 if(spawnpoints.Length > 0){
		Transform spawnpoint = spawnpoints[Random.Range(0, spawnpoints.Length)].transform;
		 
		Vector3 ranPos = spawnpoint.position; //Slightly Random position from respawn point.
		ranPos.x += Random.Range(0.0f,randomPoint);
		ranPos.z += Random.Range(0.0f,randomPoint);
		
		//Spawn Prefab
		GameObject en = Instantiate(monsterPrefab[waveManage[wave].spawnStep[step]], ranPos , spawnpoint.rotation);
		//Set Tag and Name
		en.name = monsterPrefab[waveManage[wave].spawnStep[step]].name;
		en.tag = setTagTo;
		//Set Ignore Collision
		if(ignoreObject.Length > 0){
			IgnoreCol(en);
		}
		//Send Message to the Object we spawned
		if(waveManage[wave].sendMessage != ""){
			en.SendMessage(waveManage[wave].sendMessage , waveManage[wave].sendValue);
		}
	 }
	step++;
}

public IEnumerator WaveClear (){
	freeze = true;
	showText = "Wave Clear";
	showGui = true;
	yield return new WaitForSeconds(4.5f);
	showGui = false;
	yield return new WaitForSeconds(2.5f);
	//Begin New Wave
	if(wave < waveManage.Length -1){
		wave++;
		step = 0;
		maxStep = waveManage[wave].spawnStep.Length;
		BeginWave();
		freeze = false;
	}else{
		ClearMission();
	}
}

    public IEnumerator ClearMission (){
	showText = "Mission Clear";
	showGui = true;
	yield return new WaitForSeconds(4.5f);
	showGui = false;
	//Clear
	if(completeSendMessage != ""){
		SendMessage(completeSendMessage);
	}
}

    public IEnumerator MissionFail (){
	freeze = true;
	showText = "Mission Fail";
	showGui = true;
	yield return new WaitForSeconds(4.5f);
	showGui = false;
	yield return new WaitForSeconds(3);
}

    public void IgnoreCol ( GameObject mon  ){
	//Physics.IgnoreCollision(collider, master.collider);
    foreach(GameObject pl in ignoreObject) {
    	if(pl != this.gameObject){
    		Physics.IgnoreCollision(mon.GetComponent<Collider>(), pl.GetComponent<Collider>());
    	}
    }

}

}