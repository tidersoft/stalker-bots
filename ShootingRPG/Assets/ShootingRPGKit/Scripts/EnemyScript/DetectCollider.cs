using UnityEngine;
using System.Collections;

public class DetectCollider : MonoBehaviour {

    public Transform master;
private AIenemy ai;

void Start (){
	if(!master){
		master = transform.root;
	}
	ai = GetComponentInParent<AIenemy>();
	gameObject.layer = 2;
	GetComponent<Rigidbody>().isKinematic = true;
	GetComponent<Collider>().isTrigger = true;
        if (FindObjectOfType<SpawnPlayer>().PrefabsToInstantiate[0].name.Contains("Mech")) {
            BoxCollider box = GetComponent<BoxCollider>();
            box.size = new Vector3(box.size.x, box.size.y, box.size.z * 2);
            box.center = new Vector3(0, 0, 0.5f);
        }
}

void OnTriggerEnter ( Collider other  ){
        if (ai == null)
            return;

	if(ai.followState == AIState.Moving || ai.followState == AIState.Pausing){
		return;
	}
  	if (other.gameObject.tag == "Player" || other.gameObject.tag == "Ally") {
		ai.followState = AIState.Moving;
           
	}
}
    
}