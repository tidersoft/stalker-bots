using UnityEngine;
using System.Collections;
using Photon.Pun;

public class StealthKillArea : MonoBehaviour {

private GameObject master;
public Texture2D button;
public GameObject hitEffect;
public Transform popup;
public bool  showMeleeWeapon = true;
private bool  enter = false;
private bool  attacking = false;
private GameObject player;

public float damageMultiply = 10.0f;
public AnimationClip playerAnimation;
public AnimationClip enemyAnimation;
public float enemyAnimationDelay = 0.5f;

private bool  useMecanim = true;
private bool  useMecanimMon = true;
private AIenemy ai;

void Start (){
	if(!master){
		master = transform.root.gameObject;
	}
	ai = master.GetComponent<AIenemy>();
	useMecanimMon = ai.useMecanim;
}

void Update (){
	if(Input.GetKeyDown("e") && enter && !attacking && ai.followState == AIState.Idle){
	StartCoroutine(	Attacking());
	}
}

void OnGUI (){
	if(!player ){
		return;
	}

        
	
	if(enter){
		GUI.DrawTexture( new Rect(Screen.width / 2 - 145, Screen.height - 180, 290, 80), button);
	}
}

void OnTriggerEnter ( Collider other  ){
	if (other.gameObject.tag == "Player" && ai.followState == AIState.Idle) {
		player = other.gameObject;
     if (!player.GetComponent<CharakterSync>().isMine())
       return;
            enter = true;
	}
	
}

void OnTriggerExit ( Collider other  ){
	if (other.gameObject == player) {
		enter = false;
	}
}

    public IEnumerator Attacking (){
	if(attacking){
		yield return false;
	}
	attacking = true;
	useMecanim = player.GetComponent<GunTrigger>().useMecanim;
	player.GetComponent<status>().freeze = true;
	master.GetComponent<status>().freeze = true;
	if(showMeleeWeapon){
		player.GetComponent<GunTrigger>().ShowMeleeWeapon(true);
	}
	
	if(!useMecanim && playerAnimation){
		//For Legacy Animation
		player.GetComponent<status>().mainModel.GetComponent<Animation>()[playerAnimation.name].layer = 18;
		player.GetComponent<status>().mainModel.GetComponent<Animation>().PlayQueued(playerAnimation.name, QueueMode.PlayNow);
	}else if(playerAnimation){
		//For Mecanim Animation
		player.GetComponent<PlayerMecanimAnimation>().AttackAnimation(playerAnimation.name);
	}
	yield return new WaitForSeconds(enemyAnimationDelay);
	
	if(!useMecanimMon && enemyAnimation){
		//For Legacy Animation
		master.GetComponent<status>().mainModel.GetComponent<Animation>()[enemyAnimation.name].layer = 18;
		master.GetComponent<status>().mainModel.GetComponent<Animation>().PlayQueued(enemyAnimation.name, QueueMode.PlayNow);
	}else if(enemyAnimation){
		//For Mecanim Animation
		master.GetComponent<AIenemy>().animator.Play(enemyAnimation.name);
	}
	
	if(showMeleeWeapon){
		player.GetComponent<GunTrigger>().ShowMeleeWeapon(false);
	}
	
	player.GetComponent<status>().freeze = false;
	master.GetComponent<status>().freeze = false;
	attacking = false;
	CalculateDamage();
	
}

void CalculateDamage (){
		Transform dmgPop = Instantiate(popup, master.transform.position , transform.rotation);
    	int totalDamage = (int)(player.GetComponent<status>().melee * damageMultiply);

		string popDamage = master.GetComponent<status>().OnDamage(totalDamage , 0 , true,master.transform);

		if(player && player.GetComponent<ShowEnemyHealth>()){
    		player.GetComponent<ShowEnemyHealth>().GetHP(master.GetComponent<status>().maxHealth , master.gameObject , master.name);
    	}
		dmgPop.GetComponent<DamagePopup>().damage = popDamage;	
		
		if(hitEffect){
    		Instantiate(hitEffect, transform.position , transform.rotation);
 		}
}


}