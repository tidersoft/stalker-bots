using UnityEngine;
using System.Collections;

public class WeakPoint : MonoBehaviour {

public Transform master;
public float damageMultiply = 2;
public bool  isCritical = true;
public bool  ignoreGuard = true;

void Start (){
	if(!master){
		master = transform.root;
	}
	this.tag = "WeakPoint";
}

}