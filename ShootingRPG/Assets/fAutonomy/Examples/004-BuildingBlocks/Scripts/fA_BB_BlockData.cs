﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fA_BB_BlockData : MonoBehaviour {

    [HideInInspector]
    public List<fA_BB_Block> blocks;

    //private string blockId = "";

    private fA_BB_Ground ground = null;

    //private bool readyToDestory = true;

    // Use this for initialization
    void Start ()
    {
        // Find the Ground script(component), attached only to the GameObject Lv/Ground...
        ground = GameObject.Find("Ground").GetComponent<fA_BB_Ground>();
    }
	
	// Update is called once per frame
	void Update ()
    {
    }
    /*
    public string GetNameTopOfBlock()
    {
        blocks = new List<Block>(GameObject.FindObjectsOfType<Block>());
        if (blocks != null)
        {
            foreach (Block block in blocks)
            {
                if (block.isTop)
                {
                    return block.entityName;
                }

            }
        }
        return ground.entityName;
    }
    */
    public AIBrain.AIEntity GetTopOfBlock()
    {
        blocks = new List<fA_BB_Block>(GameObject.FindObjectsOfType<fA_BB_Block>());
        if (blocks != null)
        {
            foreach (fA_BB_Block block in blocks)
            {
                if (block.isTop)
                {
                    return block;
                }

            }
        }
        return ground;
    }

    public void SetTopOfBlock(fA_BB_Block newtop)
    {
        blocks = new List<fA_BB_Block>(GameObject.FindObjectsOfType<fA_BB_Block>());
        if (blocks != null)
        {
            foreach (fA_BB_Block block in blocks)
            {
                if (block.name != newtop.name)
                {
                    block.isTop = false;
                }
                else
                {
                    block.isTop = true;
                }
            }
        }
    }
}
