﻿using System;
using System.Collections;
using System.Collections.Generic;
using AIBrain;
using UnityEngine;
using UnityEngine.SceneManagement;

public class fA_BB_AIEnvironmentBlock : AIEntityEnvironment
{
    public string       remainingTime;       // Remaining time in seconds...
    public float        goalLevel;           // The goal-level set by the user...
    public int          heightOfTower;       // The height of the current tower (non-negative integer - can be zero)...
    public AIEntity     topBlock;            // The name of the Block on top of the tower. Can be empty, if the is no tower...
    public AIEntity     topLevel;            // initially ground
    public AIEntity     firstFreeLevel;      // 타겟이되어야할 레벨 예를들면 처음에는 lv1 다음부터는 그 위로..

    public fA_BB_NPC.STATE    npcState;

    // Use this for initialization
    void Start()
    {
        goalLevel = fA_BB_GameManager.instance.goalLv;
        //blockData = FindObjectOfType<BlockData>();

        // IF the scene is played in fA AI mode, THEN...
        if (fA_BB_GameManager.instance.mode == fA_BB_GameManager.MODE.AI)
        {
            // Initialize fA AI...
            AIBrainAPI.Instance.InitialiseSceneAI(SceneManager.GetActiveScene().name, this);

            // Send a complete initial percept to each agent separately...
            foreach (AIEntityAgent agent in AIBrainAPI.Instance.GetAIBrainAgentEntitiesList())
            {
                string jsonData = AIBrainAPI.Instance.GenerateCompleteVisualJsonForAIEntityAgent(agent).ToString();

                Debug.Log("[fA_BB_AIEnvironmentBlock::Start/SendPerceptInitialToAI] agent.name = " + agent.name + "\njsonData = " + jsonData);

                AIBrainAPI.Instance.SendPerceptInitialToAI(new List<AIEntityAgent>() { agent }, jsonData);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        topBlock        = fA_BB_GameManager.instance.topOfBlock;
        /*
        if (topBlock)
            Debug.LogWarning("UPDATED TOPBLOCK IN ENVIRONMENT TO: " + topBlock.name);
        else
            Debug.LogWarning("UPDATED TOPBLOCK IN ENVIRONMENT TO NULL");
            */
        topLevel        = fA_BB_GameManager.instance.topLevel;
        firstFreeLevel  = fA_BB_GameManager.instance.GetFirstFreeLevel();
        heightOfTower   = fA_BB_GameManager.instance.heightOfTower;

        npcState        = fA_BB_GameManager.instance.agents[0].getState();
        if (npcState == fA_BB_NPC.STATE.IDLE || npcState == fA_BB_NPC.STATE.ACTION_SUCCESS || npcState == fA_BB_NPC.STATE.ACTION_FAIL)
        {
            //Debug.Log(">>>>>>>> CALLING AIBRAIN UPDATE <<<<<<<<");

            // From AIEntityEnvironment.cs...
            AIBrainAPI.Instance.AIBrainUpdate();
        }

    }

    public override string GetPropertyValue(string PropertyName) // 추가된 변수들 확장시켜야함
    {
        switch (PropertyName)
        {
            case "remainingTime":

                return remainingTime;

            case "goalLevel":

                return System.Convert.ToDouble(goalLevel.ToString()).ToString();

            case "heightOfTower":

                return heightOfTower.ToString();

            case "topBlock":

                return topBlock ? topBlock.entityName : "";

            case "topLevel":

                return topLevel ? topLevel.entityName : "";

            case "firstFreeLevel":

                return firstFreeLevel ? firstFreeLevel.entityName : "";

            default:

                return "";

        }
    }

}

