﻿using System.Collections;
using System.Collections.Generic;
using AIBrain;
using UnityEngine;

public class fA_BB_Level : AIBrain.AIEntity
{ 
    List<fA_BB_Block> blockList;

    fA_BB_Block block;

    public string blockNameInLv;

    private fA_BB_Ground ground;

    void Start ()
    {
        // Find the Ground script(component), attached only to the GameObject Lv/Ground...
        ground = GameObject.Find("Ground").GetComponent<fA_BB_Ground>();
    }
	
	void LateUpdate () {
        blockList = new List<fA_BB_Block>(GameObject.FindObjectsOfType<fA_BB_Block>()) as List<fA_BB_Block>;
        if (block != null && blockNameInLv != "" && blockList.Contains(block))
        {
            blockNameInLv = block.name;
        }
        else
            blockNameInLv = "";
    }

    void OnTriggerStay(Collider col)
    {
        if(col.tag == "Block" && col.GetComponent<fA_BB_Block>().blockState == fA_BB_Block.STATE.STACKED || col.GetComponent<fA_BB_Block>().blockState == fA_BB_Block.STATE.IDLE || col.GetComponent<fA_BB_Block>().blockState == fA_BB_Block.STATE.THROWING)
        {
            block = col.GetComponent<fA_BB_Block>();

            blockNameInLv = block.entityName;

            if(transform.name == "Lv1")
            {
                block.onTopOf = ground;
            }
            // Debug.Log(transform.name + " entered : " +col.name);
        }
    }
    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Block" && col.GetComponent<fA_BB_Block>().blockState == fA_BB_Block.STATE.STACKED || col.GetComponent<fA_BB_Block>().blockState == fA_BB_Block.STATE.IDLE || col.GetComponent<fA_BB_Block>().blockState == fA_BB_Block.STATE.THROWING)
        {
            block = null;
            blockNameInLv = "";
            // Debug.Log(transform.name + " exited : " + col.name);
        }
    }

    public override string GetPropertyValue(string PropertyName, AIEntityAgent sensingEntity, AIBrainSensor sensor)
    {
        switch (PropertyName)
        {
            case "blockNameInLv":

                return blockNameInLv;

            default:
                return "";
        }
    }
}
