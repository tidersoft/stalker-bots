﻿using System;
using System.Collections;
using System.Collections.Generic;
using AIBrain;
using UnityEngine;

public abstract class fA_BB_Block : AIEntity
{ 
    public static string BLOCK = "Block";
    public static float Z_COORD = -0.5f;

    protected int weightOnMe;
    public List<string> onTopOfMe = new List<string>();

    [HideInInspector]
    public bool readyToDestroy = false;
    [HideInInspector]
    public bool isCollision = false;

    public fA_BB_NPC carriedBy;
    public int weight;
    public AIEntity onTopOf;

    protected fA_BB_Ground ground;

    public int currentWeightOnMe;
    public bool isTop = false;

    private bool NothingOnTopOfMe = true;
    public bool isReleased = false;

    private RaycastHit hit;
    private FixedJoint fj;

    private RaycastHit[] hits;
    private List<string> tmp = new List<string>();
    private List<RaycastHit> _hits = new List<RaycastHit>();

    public TYPES blockType;
    public STATE blockState;

    public enum TYPES
    {
        GLASS, STONE, WOOD, UNKNOWN
    }

    public enum STATE
    {
        IDLE, PICKED, THROWING, STACKED, RELEASED
    }

    void Start()
    {
        init();
    }

    void FixedUpdate()
    {
        transform.Find("WeightOnMe").GetComponent<TextMesh>().text = blockState.ToString();
    }

    public void init()
    {
        readyToDestroy = false;

        switch (blockType)
        {
            case TYPES.STONE:
                weight = 60;
                break;
            case TYPES.WOOD:
                weight = 50;
                break;
            case TYPES.GLASS:
                weight = 20;
                break;
            case TYPES.UNKNOWN:
                weight = 0;
                break;
        }

        blockState = STATE.IDLE;

        foreach (BoxCollider col in GetComponents<BoxCollider>())
        {
            col.enabled = true;
        }

        transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;
        transform.GetComponent<Rigidbody>().isKinematic = false;
        transform.position = new Vector3(UnityEngine.Random.Range(-6f, 6f), UnityEngine.Random.Range(9f, 18f), Z_COORD);
        //if (name == "ConeBlock1") transform.position += new Vector3(0, 0, -1.5f);
        transform.rotation = Quaternion.Euler(0, 0, 0);

        // Find the Ground script(component), attached only to the GameObject Lv/Ground...
        ground = GameObject.Find("Ground").GetComponent<fA_BB_Ground>();
    }

    public void ShotRayCast()
    {
        // raycast shape is boxy, and the area can be set with the 'halfExtend...
        hits = Physics.BoxCastAll(transform.position, transform.lossyScale / 5f, transform.up, transform.rotation, 5f);

        onTopOfMe.Clear();
        weightOnMe = 0;
        currentWeightOnMe = 0;

        // To avoid overrapping name...
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].transform.tag != BLOCK)
            {
                continue;
            }

            if (hits[i].transform.name == transform.name)
            {
                continue;
            }

            if (tmp.Contains(hits[i].transform.name))
            {
                continue;
            }

            tmp.Add(hits[i].transform.name);
            _hits.Add(hits[i]);
        }

        hits = _hits.ToArray();

        // If a block is stacked on other...
        if (blockState == STATE.STACKED)
        {
            // If there is no block on block itself...
            if (hits.Length == 0)
            {
                isTop = true;
                return;
            }
            else
            {
                isTop = false;
            }

            // When block stacked shoot raycast upside on itself, then get a current weight on itself.
            for (int i = 0; i < hits.Length; i++)
            { 
                hit = hits[i];

                if (hit.transform.name != transform.name)
                {
                    if (hit.transform.tag == BLOCK)
                    {
                        onTopOfMe.Add(hit.transform.name);

                        if (hit.transform.GetComponent<fA_BB_Block>() != null)
                        {
                            weightOnMe += hit.transform.GetComponent<fA_BB_Block>().weight;
                            currentWeightOnMe = weightOnMe;
                        }
                    }
                }
            }        

            // To find and destory a block which exceeds max weight...
            FindDestoyableBlock();
        }
     
        tmp.Clear();
        _hits.Clear();
    }

        // To see the gizmo in editor...
        void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, transform.forward * hit.distance);
        Gizmos.DrawWireCube(transform.position + transform.forward * hit.distance, transform.lossyScale);
    }
    
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == BLOCK)
        {
            if (col.GetComponent<fA_BB_Block>().GetBlockType() == fA_BB_Block.TYPES.UNKNOWN)
            {
                //Debug.LogError("*** this block = " + this.name + ", other block = " + col.name);

                isTop = false;
                transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;
                transform.GetComponent<Rigidbody>().isKinematic = false;
                blockState = STATE.IDLE;
                transform.GetComponent<Rigidbody>().AddForce(UnityEngine.Random.Range(UnityEngine.Random.Range(-20f, -10f), UnityEngine.Random.Range(10f, 20f)), 10f, 0f);
                //StartCoroutine(DestroyBlock(2f));

                // As a result of the collision with the unknown(actually unstable) block, the this block is on the ground...
                ///this.onTopOf = ground;
                
                // ...also an event occurs, when the above happens...implying a regular percept for the AI...
                ///EventOccur();

                return;
            }

            if (col.GetComponent<fA_BB_Block>().blockState != STATE.STACKED)
            {
                return;
            }

            carriedBy = null;
            blockState = STATE.STACKED;
            transform.position = new Vector3(col.transform.position.x, transform.position.y, Z_COORD);
            transform.rotation = Quaternion.Euler(0, 0, 0);

            RaycastHit[] downHit = Physics.BoxCastAll(transform.position, transform.lossyScale / 5f, -transform.up, transform.rotation, transform.lossyScale.y);
            for (int i = 0; i < downHit.Length; i++)
            {
                if (downHit[i].transform.name != transform.name && downHit[i].transform.tag == BLOCK && downHit[i].transform.GetComponent<fA_BB_Block>().blockState == STATE.STACKED)
                {
                    //onTopOf = downHit[i].transform.name;
                    onTopOf = downHit[i].transform.GetComponent<fA_BB_Block>();
                }
            }

            foreach (fA_BB_Block b in GameObject.FindObjectsOfType<fA_BB_Block>())
            {
                if (b.blockState == fA_BB_Block.STATE.STACKED)
                    b.ShotRayCast();
                else
                    b.onTopOf = ground;
            }

        }
        else if (col.tag == "Bottom" && onTopOfMe.Count == 0)
        {
            onTopOf = ground;
            blockState = STATE.IDLE;
        }

        if (fA_BB_GameManager.instance.goalLine.GetComponent<fA_BB_GoalLine>().goalTouchedStable)
        {
            weight = 0;

        }
    }

    void OnTriggerExit(Collider col)
    {
        foreach (fA_BB_Block b in GameObject.FindObjectsOfType<fA_BB_Block>())
        {
            if (b.blockState == fA_BB_Block.STATE.STACKED)
                b.ShotRayCast();
        }
    }
    
    void FindDestoyableBlock()
    {
        List<fA_BB_Block> blocks = new List<fA_BB_Block>(GameObject.FindObjectsOfType<fA_BB_Block>());

        float minWeight = int.MaxValue;
  
        foreach (fA_BB_Block block in blocks)
        {
            if (blockType == TYPES.UNKNOWN)
                return;

            //block.gameObject.transform.Find("WeightOnMe").GetComponent<TextMesh>().text = block.currentWeightOnMe.ToString();
            if (block.currentWeightOnMe >= block.GetMaxWeightOnMe())
            {

                if (minWeight > block.weight)
                    minWeight = block.weight;
            }           
        }
        if (currentWeightOnMe >= GetMaxWeightOnMe() && minWeight >= weight && !readyToDestroy)
        {
            readyToDestroy = true;
            StartCoroutine(DestroyBlock());
        }
        else if(readyToDestroy)
        {
            float lerp = Mathf.PingPong(Time.time, 0.5f) / 0.5f;
            transform.GetComponent<Renderer>().material.color = Color.Lerp(transform.GetComponent<Renderer>().material.color, Color.red, lerp);
        }          
    }

    void OnMouseOver()
    {
        if(Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo = new RaycastHit();
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo))
            {
                Rigidbody rid = GetComponent<Rigidbody>();

                if (blockState == STATE.IDLE)
                {
                    rid.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
                    rid.isKinematic = false;
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                    blockState = STATE.STACKED;
                    transform.position = fA_BB_GameManager.instance.firstFreeLevel.transform.position;
                }
                else
                {              
                    foreach (fA_BB_Level lv in fA_BB_GameManager.instance.lvs)
                    {
                        if (lv.blockNameInLv == transform.name)
                        {
                            lv.blockNameInLv = "";
                        }                          
                    }
                    isTop = false;
                    rid.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;
                    rid.isKinematic = false;
                    blockState = STATE.IDLE;
                    transform.position  = new Vector3(UnityEngine.Random.Range(-6f, 6f), UnityEngine.Random.Range(1f, 2f), transform.position.z);
                }

                // IF the teleported block is now on the ground, THEN...
                if (this.onTopOf == ground)
                {
                    AIEntity prevtop = fA_BB_GameManager.instance.topOfBlock;

                    // The teleported block instantly goes to the top!!
                    fA_BB_GameManager.instance.blockData.SetTopOfBlock(this);
                    fA_BB_GameManager.instance.topOfBlock = this;



                    this.onTopOf = prevtop;

                    Debug.Log("[fA_BB_Block::onMouseOver] USER STACKED BLOCK: " + name);
                }

                Invoke("EventOccur", 0.1f);

                // Debug.Log("Object Hit is " + hitInfo.collider.gameObject.name);
            }
        }
        
    }

    private void EventOccur()
    {
        //Debug.LogError("EventOccur");
        fA_BB_GameManager.instance.eventOccurs = true;
    }

    IEnumerator DestroyBlock()
    {      
        yield return new WaitForSeconds(0.1f);
        transform.gameObject.SetActive(false);
        fA_BB_GameManager.instance.eventOccurs = true;

    }

    IEnumerator DestroyBlock(float delay)
    {
        yield return new WaitForSeconds(delay);
        Destroy(transform.gameObject);
    }

    public abstract int GetMaxWeightOnMe();
    
    public virtual TYPES GetBlockType()
    {
        return blockType;
    }
    public virtual int GetCurrentWeightOnMe()
    {
        return weightOnMe;
    }
    public virtual string GetOwnerName()
    {
        return carriedBy.entityName;
    }
    public virtual List<string> GetOnTopOfMe()
    {
        return onTopOfMe;
    }
    public virtual string GetOnTopOf()
    {
        return onTopOf.entityName;
    }

    public override string GetPropertyValue(string PropertyName, AIEntityAgent sensingEntity, AIBrainSensor sensor)
    {
        switch (PropertyName)
        {
            case "weightOnMe":

                return weightOnMe.ToString();

            case "onTopOfMe":
                string retVal = "";
                if (onTopOfMe.Count > 0)
                {
                    retVal = onTopOfMe[0];
                }
                return retVal;

            case "onTopOf":

                return onTopOf ? onTopOf.entityName : "";

            case "carriedBy":

                return carriedBy ? carriedBy.entityName : "";

            case "carried": // Now this is interesting: a property, that the AI may ask for, but it actually doesn't exist...
                            // ...so the AI 'sees / perceives' the world / environment differently, from how it is in 'reality'... ;)
                            // Not speaking of the additional, AIBehaviour specific properties in fA...pure 'mind-concepts'... ;)
                            // So, anyway, even though the Unity game has no such 'carried' property for Block-s, we prepare the...
                            // ...interface with fA (using fA C# API) for the AI asking about such prop-values, and responding to it...
                            // ...by deriving it (the value of 'carried') from the value of the 'carriedBy' property, that really exists.

                return carriedBy ? true.ToString() : false.ToString();

            case "weight":

                return weight.ToString();

            case "currentWeightOnMe":

                return currentWeightOnMe.ToString();

            case "NothingOnTopOfMe":

                return NothingOnTopOfMe.ToString();

            case "selfId":  // This is the only way, currently (given the limitation of fA, that it cannot yet work with...
                            // ...the built-in 'entity-type' and 'entity-name' percept-properties), for the AI to perceive the
                            // ID of a block................. TODO...

                return entityName;

            default:

                return "";
        }

    }

}
