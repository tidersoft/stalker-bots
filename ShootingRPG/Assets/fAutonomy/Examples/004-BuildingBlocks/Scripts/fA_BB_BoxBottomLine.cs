﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fA_BB_BoxBottomLine : MonoBehaviour {

    public bool initialized = false;
    private int blockCountInTrigger;

    private int blockCount;

    private List<string> blockNames = new List<string>();

	// When starts, box collider on the bottom counts number of blocks.
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {

    }

    // To do raycasting after all blocks fall down
    void OnTriggerEnter(Collider col)
    {
        if(blockNames.Count != 0)
        {
            if (blockNames.Contains(col.transform.name))
            {
                blockNames.Remove(col.transform.name);

                if (blockNames.Count == 0)
                {
                    GetComponent<BoxCollider>().size = new Vector3(0.32f, 0f, 8.2f);
                    GetComponent<BoxCollider>().isTrigger = false;
                    initialized = true;
                    Debug.Log("[BoxBottomLine::OnTriggerEnter] initialized (i.e. all blocks touched the ground, even though may bounce a bit depending on rigidbody physics)...");
                }
            }
        }
    }

    public void init()
    {
        initialized = false;
        GetComponent<BoxCollider>().isTrigger = true;

        Transform blockParent = GameObject.Find("Blocks").transform;
        for (int i = 0; i < blockParent.childCount; i++)
        {
            if (blockParent.GetChild(i).gameObject.activeSelf == true)
            {
                if (blockParent.GetChild(i).tag == "Block")
                {
                    blockNames.Add(blockParent.GetChild(i).name);
                    blockCount++;
                }
            }
        }
    }
}
