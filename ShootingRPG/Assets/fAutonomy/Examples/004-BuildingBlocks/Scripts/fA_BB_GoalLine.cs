﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fA_BB_GoalLine : MonoBehaviour {

    public bool goalTouched = false;
    private bool StableChecked = false;
    public bool goalTouchedStable = false;

    void Start()
    {
        init();
    }

    void OnTriggerStay(Collider col)
    {
        if (col.tag == "Block" && GameObject.FindObjectOfType<fA_BB_BoxBottomLine>().initialized && col.GetComponent<fA_BB_Block>().isTop && col.GetComponent<fA_BB_Block>().onTopOf.name != "Ground")
        {
            goalTouched = true;
        }

        if (!StableChecked)
        {
            StableChecked = true;
            StartCoroutine(CheckBlockStable(2f));
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Block" && GameObject.FindObjectOfType<fA_BB_BoxBottomLine>().initialized && col.GetComponent<fA_BB_Block>().isTop && col.GetComponent<fA_BB_Block>().onTopOf.name != "Ground")
        {
            goalTouched = false;
        }
    }

    IEnumerator CheckBlockStable(float delay)
    {
        yield return new WaitForSeconds(delay);

        if (goalTouched)
            goalTouchedStable = true;
        else
            goalTouchedStable = false;

        StableChecked = false;
    }

    public void init()
    {
        goalTouched = false;
    }
}
