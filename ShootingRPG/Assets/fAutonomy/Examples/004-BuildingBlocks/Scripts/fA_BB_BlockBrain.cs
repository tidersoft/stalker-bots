﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fA_BB_BlockBrain : MonoBehaviour {

    public List<fA_BB_Block> blocks;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public Transform SearchBlockToStack()
    {
        blocks = new List<fA_BB_Block>(GameObject.FindObjectsOfType<fA_BB_Block>());

        int value = int.MinValue;
        Transform highPriorityBlock = null;
        foreach (fA_BB_Block block in blocks)
        {
            if(block.blockState == fA_BB_Block.STATE.IDLE)
            {
                int valueOfblock = block.GetMaxWeightOnMe() - block.weight;
                if(value < valueOfblock)
                {
                    value = valueOfblock;
                    highPriorityBlock = block.transform;
                }
            }
        }
        return highPriorityBlock;
    }
}
