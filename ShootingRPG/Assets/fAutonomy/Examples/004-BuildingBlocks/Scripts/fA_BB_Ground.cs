﻿using System;
using System.Collections;
using System.Collections.Generic;
using AIBrain;
using UnityEngine;

public class fA_BB_Ground : AIBrain.AIEntity {

    public override string GetPropertyValue(string PropertyName, AIEntityAgent sensingEntity, AIBrainSensor sensor)
    {
        switch (PropertyName)
        {
            case "selfId":  // This is the only way, currently (given the limitation of fA, that it cannot yet work with...
                            // ...the built-in 'entity-type' and 'entity-name' percept-properties), for the AI to perceive the
                            // ID of the ground................. TODO...

                return entityName;

            default:

                return "";
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
