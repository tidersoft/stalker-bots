﻿using System;
using System.Collections;
using System.Collections.Generic;
using AIBrain;
using UnityEngine;
using UnityEngine.AI;

public class fA_BB_NPC : AIEntityAgent
{
    [HideInInspector]
    public NavMeshAgent agent;

    public int strengthLv   = 1111; // TODO: such value-initialization should later be removed... It is here now however, so that we can add NPC GameObject-s with fA to the AI scene (attaching to them this script in the process), with the value of strength initialized properly (so that the agent, GameObject, to which this script is attached, is strong enough to lift any block:)
    public int speedLv;

    public bool hasBlock = false;
    private fA_BB_Block carriedBlock;

    private Transform npc, block, blockParent, target;
    private GameObject[] tps;

    private string parameter = "";

    private fA_BB_Level[] targetlv;
    private int numTarget = 0;
    private float targetMargine;
    protected STATE state;
    protected MODE mode;

    // Did this NPC ever receive an AI action?
    private bool receivedAction = false;

    // Script Mode...
    private Transform firstTarget;
    private bool isFirstTarget = true;

    public enum MODE
    {
        AI = 0, MLA = 1, BT = 2, SCRIPT = 3
    }

    public enum STATE
    {
        IDLE, MOVING, PICKING, SHOOTING, UNSTACKING, RELEASE_BLOCK, DOING_SOMETHING, WAITING, ACTION_SUCCESS, ACTION_FAIL
    }

    public STATE getState()
    {
        return state;
    }

    // Use this for initialization
    void Start()
    {
        targetlv = GameObject.FindObjectsOfType<fA_BB_Level>();
        blockParent = GameObject.Find("Blocks").transform;
        tps = GameObject.FindGameObjectsWithTag("ThrowingPoint");
        agent = GetComponent<NavMeshAgent>();
        npc = gameObject.transform;
    }
    /*
    private void Update()
    {
        if (carriedBlock)
            Debug.LogWarning(name + " CARRIES " + carriedBlock.name);
        else
            Debug.LogWarning(name + " CARRIES NOTHING...");
    }
    */
    // Update is called once per frame
    void LateUpdate()
    {
        Debug.Log("[fA_BB_NPC::LateUpdate] current game-state of agent, " + transform.name + ", is: " + state.ToString());

        //if (mode == MODE.AI && (state == STATE.ACTION_FAIL/* || state == STATE.ACTION_SUCCESS*/))
            //GameManager.instance.SendPerceptionToAll(null); // IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        if (IsDetroying())
            return;

        if (Input.GetKeyDown(KeyCode.Q))
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("p0", "ConeBlock1");
            ReceiveActionFromAI("PickUp", 1234, dic);
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            agent.SetDestination(target.position);
        }

        else if (Input.GetKeyDown(KeyCode.E))
        {
            GetAction("PickUp", "test");
        }
        else if (Input.GetKeyDown(KeyCode.Z))
        {
            state = STATE.SHOOTING;
        }
        else if (Input.GetKeyDown(KeyCode.X))
        {
            //target = GameObject.Find("Blocks").transform.Find(GameObject.Find("Blocks").GetComponent<BlockData>().GetNameTopOfBlock());
            target = GameObject.Find("Blocks").GetComponent<fA_BB_BlockData>().GetTopOfBlock().transform;
            // targetMargine = targetMargine - target.transform.lossyScale.y;
            state = STATE.UNSTACKING;
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            numTarget++;
            if (numTarget >= targetlv.Length)
                numTarget = 0;
            target = targetlv[numTarget].transform;
            Debug.Log(target.name);
        }
        else if (Input.GetKeyDown(KeyCode.C))
        {
            state = STATE.RELEASE_BLOCK;
        }

        switch (state)
        {
            case STATE.MOVING:

                break;

            case STATE.PICKING:

                if (hasBlock)
                {
                    //if (mode == MODE.AI)
                        //Invoke("DelayActionDone_Temp", 1f); // IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                    state = STATE.ACTION_FAIL;
                    
                    return;
                }
                if (parameter != "")
                {
                    PickUp(GameObject.Find("Blocks").transform.Find(parameter).transform); /// TRANSLATE OBJNAME (BASE32 DECODE)..........
                }
                else
                {
                    /*
                    float maxDist = 20f;

                    Collider[] hitColliders = Physics.OverlapSphere(npc.position, maxDist);
                    Collider shortDistCollder = hitColliders[0];
                    float shortDist = 100f;
                    foreach (Collider col in hitColliders)
                    {
                        float dist = Vector3.Distance(npc.position, col.transform.position);
                        if (dist < shortDist && col.tag == BLOCK && col.GetComponent<Block>().blockState != Block.STATE.STACKED)
                        {
                            shortDistCollder = col;
                            shortDist = dist;
                        }
                    }

                    PickUp(shortDistCollder.transform);
                    */

                    PickUp(GameObject.Find("Blocks").GetComponent<fA_BB_BlockBrain>().SearchBlockToStack());

                }

                break;

            case STATE.SHOOTING:

                if (parameter != "")
                {
                    //string objName;
                    //if (GameManager.instance.lvDic.TryGetValue(parameter, out objName))
                    //    Shoot(GameObject.Find("Lv").transform.Find(objName).transform); /// TRANSLATE OBJNAME (BASE32 DECODE)..........

                    Transform t = GameObject.Find("Lv").transform.Find(parameter);

                    if (t)
                        Shoot(t.transform);
                }
                else
                {
                    if (mode == MODE.SCRIPT)
                    {
                        // target = GameObject.Find("Lv").transform.Find(GameManager.instance.GetFirstFreeLevel());
                        // targetMargine = targetMargine + target.transform.lossyScale.y;
                        Shoot(target);
                    }
                    else
                    {
                        //target = GameObject.Find("Lv").transform.Find(GameManager.instance.GetFirstFreeLevel());
                        target = fA_BB_GameManager.instance.GetFirstFreeLevel().transform;
                        Shoot(target);
                    }

                }

                break;

            case STATE.UNSTACKING:

                if (!hasBlock)
                {
                    if (!fA_BB_GameManager.instance.topOfBlock)
                    {
                        state = STATE.ACTION_FAIL;
                        return;
                    }
                    Unstack(target);
                }
                else
                {
                    Unstack(carriedBlock.transform);/// TRANSLATE blockId (BASE32 DECODE)..........
                }

                break;

            case STATE.WAITING:

                break;

            case STATE.RELEASE_BLOCK:

                Release();

                break;

            case STATE.ACTION_SUCCESS:

                break;

            case STATE.ACTION_FAIL:

                state = STATE.IDLE;
                break;

            case STATE.IDLE:

                break;
        }

        if (fA_BB_GameManager.instance.goalLine.GetComponent<fA_BB_GoalLine>().goalTouchedStable)
        {
            Release();
            state = STATE.IDLE;
        }
    }

    void GetAction(string action, string param)
    {
        switch (action)
        {
            case "PickUp":
                state = STATE.PICKING;
                break;
            case "Shoot":
                state = STATE.SHOOTING;
                break;
            case "Unstack":
                state = STATE.UNSTACKING;
                break;
            case "ReleaseBlock":
                state = STATE.RELEASE_BLOCK;
                break;
            case "MoveTo":
                state = STATE.MOVING;
                break;
        }
    }

    bool ArrivedDestination(Vector3 destination)
    {
        float dist = Vector3.Distance(npc.position, destination);

        if (dist < 1)
        {
            return true;
        }
        try
        {
            agent.SetDestination(destination);
        }
        catch
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, 10 * Time.deltaTime);
        }

        return false;
    }

    bool ArrivedDestination(float x_coord, float level)
    {
        Vector3 destination = new Vector3(x_coord, level, 0f);
        float dist = Vector3.Distance(npc.position, destination);

        if (dist < 0.5)
        {
            return true;
        }

        agent.SetDestination(destination);

        // Only agent moves to the specific position, then set the state.
        state = STATE.MOVING;

        return false;
    }

    bool IsEnoughStrength(Transform block)
    {
        if (strengthLv >= block.GetComponent<fA_BB_Block>().weight)
            return true;

        return false;
    }

    void PickUp(Transform block)
    {
        // Script mode...
        if (block == null)
        {
            StartCoroutine(Speak("There is no block anymore."));
            return;
        }

        if (ArrivedDestination(block.position))
        {
            if (hasBlock)
            {
                //if (mode == MODE.AI)
                    //Invoke("DelayActionDone_Temp", 1f); // IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                state = STATE.ACTION_FAIL;

                return;
            }

            if (block.GetComponent<fA_BB_Block>().blockState == fA_BB_Block.STATE.STACKED || block.GetComponent<fA_BB_Block>().blockState == fA_BB_Block.STATE.PICKED)
            {
                return;
            }

            if (IsEnoughStrength(block))
            {
                block.SetParent(npc); 
                block.transform.rotation = Quaternion.Euler(0, 0, 0);
                block.position = new Vector3(npc.position.x, npc.position.y + 1f, npc.position.z);
                block.GetComponent<Rigidbody>().isKinematic = true;
                block.GetComponent<fA_BB_Block>().blockState = fA_BB_Block.STATE.PICKED;
                carriedBlock = block.GetComponent<fA_BB_Block>();
                hasBlock = true;

                foreach (fA_BB_Block b in GameObject.FindObjectsOfType<fA_BB_Block>())
                {
                    if (b.blockState == fA_BB_Block.STATE.STACKED)
                        b.ShotRayCast();
                }

                if (mode == MODE.AI)
                {
                    state = STATE.IDLE;
                    Invoke("DelayActionDone_Temp", 0f);
                }
                else
                    state = STATE.ACTION_SUCCESS;

                // START - Script mode...
                if (mode == MODE.SCRIPT)
                {
                    target = fA_BB_GameManager.instance.firstFreeLevel.transform;
                    targetMargine = 0;
                    state = STATE.SHOOTING;
                }
                // END - Script mode...
                return;
            }
            else
            {
                StartCoroutine(Speak("I can't pick it up."));

                //if (mode == MODE.AI)
                    //Invoke("DelayActionDone_Temp", 1f); // IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                state = STATE.ACTION_FAIL;

                return;
            }
        }
    }

    void Shoot(Transform target)
    {
        if (carriedBlock)
        {
            // START - Script mode...
            if (mode == MODE.SCRIPT)
            {
                if (GameObject.Find("Blocks").GetComponent<fA_BB_BlockData>().GetTopOfBlock())
                {
                    /* target = GameObject.Find("Blocks").transform.Find(GameObject.Find("Blocks").GetComponent<BlockData>().GetNameTopOfBlock());

                       if (target != null && target.GetComponent<Block>().readyToDestory)
                          target.position = new Vector3(target.position.x, target.position.y + target.lossyScale.y, target.position.z); */

                    // target = GameManager.instance.fisrtFreeLevel;
                    targetMargine = 0;
                }

                else if (firstTarget != null)
                {
                    target = firstTarget;
                    // target.position = new Vector3(target.position.x, target.position.y + target.lossyScale.y, target.position.z);
                    firstTarget = null;
                }
                else if (isFirstTarget)
                {
                    firstTarget = target;
                    isFirstTarget = false;
                }
                else
                {
                    //target = GameObject.Find("Blocks").transform.Find(GameObject.Find("Blocks").GetComponent<BlockData>().GetNameTopOfBlock());
                }
            }
            // END - Script mode

            Transform block = carriedBlock.transform;          /// TODO.....................

            float shortDist = int.MaxValue;

            Transform tp = null;
            foreach (GameObject go in tps)
            {
                if (target == null)
                {
                    return;
                }

                float _shortDist = Vector3.Distance(transform.position, go.transform.position) + Vector3.Distance(target.position, go.transform.position);
                if (shortDist > _shortDist)
                {
                    shortDist = _shortDist;
                    tp = go.transform;
                }
            }

            foreach (fA_BB_Block b in fA_BB_GameManager.instance.blocks)
            {
                if (b.blockState == fA_BB_Block.STATE.THROWING && (!b.carriedBy || b.carriedBy.entityName != npc.GetComponent<fA_BB_NPC>().entityName) && (!b.carriedBy || b.carriedBy.state != STATE.UNSTACKING))
                {
                    return;
                }
            }

            if (!ArrivedDestination(tp.position))
            {
                return;
            }
            // Throwing Process  
            if (!MoveBlockToTargetLocation(block, target))
            {
                return;
            }

            block.SetParent(blockParent);
            block.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            block.GetComponent<Rigidbody>().isKinematic = false;
            block.transform.rotation = Quaternion.Euler(0, 0, 0);
            block.GetComponent<fA_BB_Block>().carriedBy = null;
            //block.position = new Vector3(target.position.x, target.position.y + target.lossyScale.y, target.position.z);

            // The previously carried block (which was shot), is not on top 
            carriedBlock.onTopOf = target.GetComponent<AIEntity>();

            //Block targetEntity = target.GetComponent<Block>();
            // IF the target was a block (and not the ground...at the 1st level of the tower), THEN...
            //if (targetEntity)
                // The previously carried block (which was shot), is now added to the list of blocks, which are on top of the target...s
                //targetEntity.onTopOfMe.Add(carriedBlock.entityName);

            carriedBlock = null;

            hasBlock = false;

            foreach (fA_BB_Block b in GameObject.FindObjectsOfType<fA_BB_Block>())
            {
                if (b.blockState == fA_BB_Block.STATE.STACKED)
                    b.ShotRayCast();
            }

            if (mode == MODE.AI)
            {
                Invoke("DelayActionDone_Temp", 2f); // IMPORTANT FOR THE PHYSICS SIMULATION TO COMPLETE AFTER THIS SUCCESSFUL SHOOTING ACTION.......
                state = STATE.IDLE; // No more shooting...not to fail...while the physics simulation is running in the game... ;)
            }
            else
                state = STATE.ACTION_SUCCESS;

            // Script mode...
            if (mode == MODE.SCRIPT)
            {
                state = STATE.PICKING;
            }
            return;
        }
        state = STATE.ACTION_FAIL;
    }

    void Unstack(Transform block)
    {      
        if (hasBlock && block.GetComponent<fA_BB_Block>().blockState == fA_BB_Block.STATE.STACKED)
        {
            StartCoroutine(Speak("I have a block already"));
            state = STATE.ACTION_FAIL;
            return;
        }

        float shortDist = int.MaxValue;
        Transform tp = null;

        foreach (GameObject go in tps)
        {
            if (target == null)
            {
                return;
            }

            float _shortDist = Vector3.Distance(transform.position, go.transform.position) + Vector3.Distance(target.position, go.transform.position);
            if (shortDist > _shortDist)
            {
                shortDist = _shortDist;
                tp = go.transform;
            }
        }

        if (!ArrivedDestination(tp.position))
        {
            return;
        }
        if (!MoveBlockToTargetLocation(block, npc))
        {
            foreach (Collider col in block.GetComponents<Collider>())
            {
                col.enabled = false;
            }

            return;
        }

        fA_BB_Level[] lvs = GameObject.FindObjectsOfType<fA_BB_Level>();
        foreach (fA_BB_Level lv in lvs)
        {
            if (lv.blockNameInLv == block.name)
            {
                lv.blockNameInLv = "";
            }
        }

        foreach (fA_BB_Block b in GameObject.FindObjectsOfType<fA_BB_Block>())
        {
            if (b.blockState == fA_BB_Block.STATE.STACKED)
                b.ShotRayCast();
        }

        block.SetParent(npc);
        block.GetComponent<Rigidbody>().isKinematic = true;
        block.rotation = Quaternion.Euler(0, 0, 0);
        block.position = new Vector3(block.position.x, block.position.y + block.lossyScale.y, block.position.z);
        block.GetComponent<fA_BB_Block>().blockState = fA_BB_Block.STATE.PICKED;
        carriedBlock = block.GetComponent<fA_BB_Block>();
        block.GetComponent<fA_BB_Block>().isTop = false;
        hasBlock = true;

        block.GetComponent<fA_BB_Block>().onTopOf = null;

        if (mode == MODE.AI)
        {
            state = STATE.IDLE;
            Invoke("DelayActionDone_Temp", 2f);
        }

        if (mode == MODE.SCRIPT)
        {
            target = fA_BB_GameManager.instance.GetFirstFreeLevel().transform;
            state = STATE.SHOOTING;
        }
    }

    void Release()
    {
        if (carriedBlock && hasBlock)
        {
            block = carriedBlock.transform;                    /// TODO....................................
            block.SetParent(blockParent);
            block.GetComponent<fA_BB_Block>().isReleased = true;
            block.GetComponent<Rigidbody>().isKinematic = false;
            block.transform.rotation = Quaternion.Euler(0, 0, 0);
            block.GetComponent<fA_BB_Block>().carriedBy = null;
            block.GetComponent<fA_BB_Block>().blockState = fA_BB_Block.STATE.IDLE;
            carriedBlock = null;
            hasBlock = false;
            block.GetComponent<fA_BB_Block>().isTop = false;

            block.GetComponent<fA_BB_Block>().onTopOf = fA_BB_GameManager.instance.ground;

            if (block.name == "ConeBlock1")
            {
                Transform newtarget = block;
                newtarget.position += new Vector3(0, 0, -1.5f);
                if (MoveBlockToTargetLocation(block, newtarget))
                {
                    if (mode == MODE.AI)
                    {
                        state = STATE.IDLE;
                        Invoke("DelayActionDone_Temp", 1f);
                    }
                }
            }
            else
            {
                if (mode == MODE.AI)
                {
                    state = STATE.IDLE;
                    Invoke("DelayActionDone_Temp", 0f);
                }
                else
                    state = STATE.ACTION_SUCCESS;
            }
        }
        else
        {
            state = STATE.ACTION_FAIL;
        }
    }

    public void SetMode(int modeNum)
    {
        if (modeNum == 0)
        {
            mode = MODE.AI;
        }
        else if (modeNum == 1)
        {
            mode = MODE.BT;
        }
        else if (modeNum == 2)
        {
            mode = MODE.MLA;
        }
        else if (modeNum == 3)
        {
            mode = MODE.SCRIPT;
        }
    }

    private bool MoveBlockToTargetLocation(Transform block, Transform target)
    {
        Vector3 targetLocation = target.position;
        targetLocation.y = targetLocation.y + targetMargine;
        //targetLocation.y = targetMargine;
        if (block == null)
            return false; ;
        block.position = Vector3.Lerp(block.position, targetLocation, 1.5f * Time.deltaTime);
        float dist = Vector3.Distance(block.position, targetLocation);
        if (dist <= 0.05f)
        {
            block.GetComponent<fA_BB_Block>().blockState = fA_BB_Block.STATE.STACKED;
            block.GetComponent<Rigidbody>().isKinematic = false;
            return true;
        }
        else if (dist <= 0.5f)
        {
            if (target.tag == "Lv" && target.GetComponent<fA_BB_Level>().blockNameInLv != "")
            {
                // Debug.Log(block.name + " Collision");
                StartCoroutine(Speak("There is already another block."));
                this.target = block;
                state = STATE.UNSTACKING;
            }
            return false; ;
        }
        else
        {
            block.GetComponent<fA_BB_Block>().blockState = fA_BB_Block.STATE.THROWING;
            block.GetComponent<Rigidbody>().isKinematic = true;
            return false;
        }
    }

    private void ThrowBlockAtTargetLocation(Transform block, Vector3 targetLocation, float initialVelocity)
    {
        Vector3 direction = (targetLocation - transform.position).normalized;
        float distance = Vector3.Distance(targetLocation, transform.position);

        float firingElevationAngle = FiringElevationAngle(Physics.gravity.magnitude, distance, initialVelocity);
        Vector3 elevation = Quaternion.AngleAxis(firingElevationAngle, transform.right) * transform.up;
        float directionAngle = AngleBetweenAboutAxis(transform.forward, direction, transform.up);
        Vector3 velocity = Quaternion.AngleAxis(directionAngle, transform.up) * elevation * initialVelocity;

        block.GetComponent<Rigidbody>().AddForce(velocity, ForceMode.VelocityChange);
    }

    private float AngleBetweenAboutAxis(Vector3 v1, Vector3 v2, Vector3 n)
    {
        return Mathf.Atan2(
            Vector3.Dot(n, Vector3.Cross(v1, v2)),
            Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
    }

    private float FiringElevationAngle(float gravity, float distance, float initialVelocity)
    {
        float angle = 0.5f * Mathf.Asin((gravity * distance) / (initialVelocity * initialVelocity)) * Mathf.Rad2Deg;
        return angle;
    }

    private bool IsDetroying()
    {
        fA_BB_Block[] blocks = GameObject.FindObjectsOfType<fA_BB_Block>();
        foreach (fA_BB_Block block in blocks)
        {
            if (block.readyToDestroy)
                return true;
        }
        return false;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Agent")
        {
            GetComponent<Rigidbody>().detectCollisions = false;
        }
    }

    IEnumerator Speak(string text)
    {
        yield return new WaitForSeconds(0.1f);
        transform.Find("Text").GetComponent<TextMesh>().text = text;
        yield return new WaitForSeconds(2f);
        transform.Find("Text").GetComponent<TextMesh>().text = "";
    }

    public override void ReceiveActionFromAI(string actionID, long timestamp, Dictionary<string, string> parameters)
    {
        receivedAction = true;

        string param;
        if (parameters.TryGetValue("p0", out param))
            parameter = param;

        if (string.IsNullOrEmpty(param))
            Debug.Log("[fA_BB_NPC::ReceiveActionFromAI] agent, " + transform.name + ", received action, " + actionID + ", without any parameters...");
        else
            Debug.Log("[" + transform.name + ".NPC::ReceiveActionFromAI] agent, " + transform.name + ", received action, " + actionID + ", with single parameter: " + param);

        switch (actionID)
        {
            case "pickup":
                state = STATE.PICKING; // block name
                break;
            case "shoot":
                state = STATE.SHOOTING; // target lv
                break;
            case "unstack":
                //target = GameObject.Find("Blocks").transform.Find(GameManager.instance.topOfBlock);
                if (fA_BB_GameManager.instance.topOfBlock)
                {
                    target = fA_BB_GameManager.instance.topOfBlock.transform;
                    if (target.GetComponent<fA_BB_Block>() == null)
                    {
                        state = STATE.ACTION_FAIL;
                        return;
                    }
                }
                else
                {
                    // Top block not yet set...needs time.....................................
                    //state = STATE.ACTION_FAIL;
                    return;
                }
                state = STATE.UNSTACKING;
                break;
            case "releaseblock":
                state = STATE.RELEASE_BLOCK;
                break;
                /*
                case "MoveTo":
                    state = STATE.MOVING;
                    break;
                */
        }

        //receivedAction = false;

    }

    void DelayActionDone_Temp()
    {
        state = STATE.ACTION_SUCCESS;
        fA_BB_GameManager.instance.SendPerceptionToAll(this);
    }

    public override void ReceiveCommandFromAI(string commandID, long timestamp, Dictionary<string, string> parameters)
    {
        throw new NotImplementedException();
    }

    public bool ActionDone(string actionID)
    {
        if(state == STATE.ACTION_SUCCESS || state == STATE.ACTION_FAIL)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool ReceivedAction()
    {
        return receivedAction;
    }

    public bool Idle()
    {
        return state == STATE.IDLE ? true : false;
    }

    public override string GetPropertyValue(string PropertyName, AIEntityAgent sensingEntity, AIBrainSensor sensor)
    {
        switch (PropertyName)
        {
            case "strengthLv":

                return strengthLv.ToString();

            case "hasBlock":

                return hasBlock.ToString();

            case "blockId":

                return carriedBlock ? carriedBlock.entityName : "";

            case "npcId":   // This is the only way, currently (given the limitation of fA, that it cannot yet work with...
                            // ...the built-in 'entity-type' and 'entity-name' percept-properties), for the AI to perceive it's
                            // selfId................. TODO...

                return entityName;

            default:

                return "";
        }
    }

    public void SetReceivedAction(bool value)
    {
        receivedAction = value;
    }
}

