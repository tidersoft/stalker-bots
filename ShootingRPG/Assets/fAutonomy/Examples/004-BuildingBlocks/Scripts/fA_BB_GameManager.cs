﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AIBrain;
using System;

[ExecuteInEditMode]
public class fA_BB_GameManager : MonoBehaviour
{
    public static fA_BB_GameManager instance = null;

    [HideInInspector]
    public Transform goalLine;

    [Range(3.2f, 9.0f)]
    public float goalLv = 5.5f;

    public fA_BB_Block[] blocks;

    public fA_BB_BlockData blockData;
    private fA_BB_BoxBottomLine line;

    //private int modeNum = 0;
    //private float goalLineY;

    [SerializeField]
    //private int blockNums;
    public AIEntity topOfBlock;
    [SerializeField]
    public fA_BB_NPC[] agents;

    public fA_BB_Level[] lvs;
    public AIEntity firstFreeLevel;
    public AIEntity topLevel;
    public int heightOfTower;

    public fA_BB_Ground ground;

    //public Dictionary<string, string> blockDic = new Dictionary<string, string>();
    //public Dictionary<string, string> lvDic = new Dictionary<string, string>();

    public bool eventOccurs = false;

    public MODE mode;
    public enum MODE
    {
        AI, SCRIPT, TEST
    }

    void Awake()
    {
        InitGame();

        if (instance == null)
            instance = this;

        // DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        // Find the Ground script(component), attached only to the GameObject Lv/Ground...
        ground = GameObject.Find("Ground").GetComponent<fA_BB_Ground>();
    }

    public void InitGame()
    {
        agents      = GameObject.FindObjectsOfType<fA_BB_NPC>();
        lvs         = GameObject.FindObjectsOfType<fA_BB_Level>();
        blockData   = GameObject.FindObjectOfType<fA_BB_BlockData>();
        blocks      = GameObject.FindObjectsOfType<fA_BB_Block>();
        line        = GameObject.FindObjectOfType<fA_BB_BoxBottomLine>();
        goalLine    = transform.Find("GoalLine").transform;

        //if (goalLine != null)
        //    goalLineY = goalLine.position.y;

        goalLv = 5.5f;

        foreach (fA_BB_NPC npc in agents)
        {
            if (mode == MODE.AI)
            {
                npc.SetMode(0);
            }
            else if (mode == MODE.SCRIPT)
            {
                npc.SetMode(3);
            }
        }
        
        goalLine.GetComponent<fA_BB_GoalLine>().init();
        line.init();

        foreach (fA_BB_Block block in blocks)
        {
            //blockDic.Add(block.entityName, block.transform.name);
            block.init();
        }

        lvs[0] = GameObject.Find("Lv").transform.Find("Lv1").GetComponent<fA_BB_Level>();
        lvs[1] = GameObject.Find("Lv").transform.Find("Lv2").GetComponent<fA_BB_Level>();
        lvs[2] = GameObject.Find("Lv").transform.Find("Lv3").GetComponent<fA_BB_Level>();
        lvs[3] = GameObject.Find("Lv").transform.Find("Lv4").GetComponent<fA_BB_Level>();
        lvs[4] = GameObject.Find("Lv").transform.Find("Lv5").GetComponent<fA_BB_Level>();
        lvs[5] = GameObject.Find("Lv").transform.Find("Lv6").GetComponent<fA_BB_Level>();
        lvs[6] = GameObject.Find("Lv").transform.Find("Lv7").GetComponent<fA_BB_Level>();
        lvs[7] = GameObject.Find("Lv").transform.Find("Lv8").GetComponent<fA_BB_Level>();
        lvs[8] = GameObject.Find("Lv").transform.Find("Lv9").GetComponent<fA_BB_Level>();
        lvs[9] = GameObject.Find("Lv").transform.Find("Lv10").GetComponent<fA_BB_Level>();

        //foreach(Level lv in lvs)
        //{
        //    lvDic.Add(lv.entityName, lv.transform.name);
        //}

        firstFreeLevel = lvs[0];
    }

    void Update()
    {
        goalLine.position = new Vector3(goalLine.position.x, goalLv, goalLine.position.z);

        //blockNums = blockData.blocks.Count;
        //Debug.LogError("****************** blockData.GetNameTopOfBlock() = " + blockData.GetNameTopOfBlock());
        topOfBlock = blockData.GetTopOfBlock();
        
        if (mode == MODE.AI && eventOccurs)
        {
            SendPerceptionToAll();
            eventOccurs = false;
        }
    }


    public AIEntity GetFirstFreeLevel()
    {
        for (int i = 0; i < lvs.Length; i++)
        {
            if (lvs[i].blockNameInLv == "")
            {
                if(i>0)
                {
                    topLevel = lvs[i - 1];
                }
                else
                {
                    topLevel = ground;
                }
                firstFreeLevel = lvs[i];
                heightOfTower = i;
                return firstFreeLevel;
            }
        }
        return firstFreeLevel;
    }

    // Called via Invoke with some delay (so that things manifest)...
    private void SendPerceptionToAll()
    {
        foreach (AIEntityAgent agent in GameObject.FindObjectsOfType<AIEntityAgent>())
        {
            AIBrainAPI.Instance.SendPerceptRegularToAI(agent, AIBrainAPI.Instance.GenerateCompleteVisualJsonForAIEntityAgent(agent).ToString());
        }
    }

    public void SendPerceptionToAll(AIEntityAgent excludeAgent)
    {
        // IF the excluded agent (whose action is supposed to be finished now) is not NULL, THEN...
        if (excludeAgent)
        {
            // Send a complete action-outcome percept to the excluded agent...
            AIBrainAPI.Instance.SendPerceptActionOutcomeToAI(excludeAgent, AIBrainAPI.Instance.GenerateCompleteVisualJsonForAIEntityAgent(excludeAgent).ToString());
        }

        // ...and send a complete regular percept for every other agent as well (except for the excluded agent)...
        foreach (AIEntityAgent agent in GameObject.FindObjectsOfType<AIEntityAgent>())
        {
            // IF this agent is not the excluded agent, THEN...
            if (excludeAgent == null || agent.name != excludeAgent.name)
            {
                // Send a complete regular percept to the non-excluded agent...
                AIBrainAPI.Instance.SendPerceptRegularToAI(agent, AIBrainAPI.Instance.GenerateCompleteVisualJsonForAIEntityAgent(agent).ToString());
            }
        }
    }
}