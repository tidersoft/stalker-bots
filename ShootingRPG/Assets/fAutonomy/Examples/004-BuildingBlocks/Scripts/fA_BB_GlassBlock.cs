﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fA_BB_GlassBlock : fA_BB_Block
{
    public int maxWeightOnMe;

    void Start()
    {
        blockType = TYPES.GLASS;

        // Find the Ground script(component), attached only to the GameObject Lv/Ground...
        ground = GameObject.Find("Ground").GetComponent<fA_BB_Ground>();
    }

    public override int GetMaxWeightOnMe()
    {
        return maxWeightOnMe;
    }

    void Update()
    {
        if (transform.parent.tag == "Agent")
        {
            carriedBy   = transform.parent.GetComponent<fA_BB_NPC>();
            onTopOf     = ground;

            foreach (BoxCollider col in GetComponents<BoxCollider>())
            {
                col.enabled = false;           
            }
        }
        else
        {
            foreach (BoxCollider col in GetComponents<BoxCollider>())
            {
                col.enabled = true;
            }
        }
    }
}
