=====================================
fAutonomy Example 004: BuildingBlocks
=====================================

A single fAutonomy-based agent is trying to build a tower of blocks of a preset height.

This is the 1st interactive fAutonomy demo, since the human user (player) can interfere with the plan of the agent.

The user can click on any block, which is on the ground, and teleport it up at the top of the tower of blocks.

Similarly the user can teleport any block in the tower back to the ground any time.

This example is intended to show a more complex setting, highlighting the following 3 features of fAutonomy in contrast to standard scripted game AI in the most simple way:

1) fAutonomy is GOAL-DRIVEN;
2) fAutonomy is ADAPTIVE; and
3) fAutonomy is able to LEARN in runtime, using the memory attached to the pre-trained DCNN (Deep Convolutional Neural Network), that implements the AI behaviour of agents.

Please open the Scene/fA_BuildingBlocks.unity scene and click play.

To switch to scripted game AI (which could be FSM or Behavior Tree based, but for now, it is hardcoded), please change the value of the 'Mode' property of the 'fA_BB_GameManager' C# script/component attached to the 'GameManager' GameObject from 'AI' to 'SCRIPT'. You can change it back anytime. Please change the Game-Mode only when the scene is not playing.

To start the agent in SCRIPT mode, please press 'E' on your keyboard.

In AI mode (which is when fAutonomy controls the agent in the scene) the agent starts acting automatically.

Please watch the following 5 minute long video for more details about this demo: https://www.youtube.com/watch?v=HTIexsOfqBU

For details of AI-implementation please open the above scene, and enter the Tools / fAutonomy / AI Scene setup or AI behaviour setup menu in the Unity Editor.

For more information please check the quick user guide, and the user manual, included in the package.