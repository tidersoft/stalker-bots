﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using AIBrain;

public class fA_DoorKey2_Agent : AIEntityAgent {

	NavMeshAgent navAgent;

    private bool hasKey = false;
    private bool escaped = false;

    /// <summary>
    /// Please change this float-value between 0 and 1 to affect the success-rate of the PICK-KEY game-action.
    /// NOTE: 1 is default value, which can be overriden by the user in the Unity Editor (by changing the relevant property-value of this script, attached to the Agent GameObject in the scene).
    /// </summary>
    public float pickKeySuccessProbability = 1;

    public Transform key;
    public Transform door;

    private STATE state = STATE.IDLE;
    private enum STATE { MOVING_TO_PICK_KEY, MOVING_TO_ESCAPE, IDLE, ACTION_SUCCESS, ACTION_FAILED } 

	// Use this for initialization
	void Start ()
    {
        navAgent    = GetComponent<NavMeshAgent>();
    }

    void Init()
    {        
        hasKey = false;
        escaped = false;
        key.gameObject.SetActive(true);
        key.position = new Vector3(Random.Range(-9, 9), key.position.y, Random.Range(-9, 9));
        door.position = new Vector3(Random.Range(-9, 9), door.position.y, Random.Range(-9, 9));

        GameObject.FindObjectOfType<fA_DoorKey2_Environment>().Initialize();
    }
	
	// Update is called once per frame
	void Update () {

        CheckState();

    }

    void CheckState()
    {
        switch(state)
        {
            case STATE.MOVING_TO_PICK_KEY:
                if(Arrived(key))
                {
                    PickKey();
                }
                break;
            case STATE.MOVING_TO_ESCAPE:
                if (Arrived(door))
                {
                    Escape();
                }
                break;
            case STATE.ACTION_SUCCESS:
                state = STATE.IDLE;
                StartCoroutine(SendPerception());
                break;
            case STATE.ACTION_FAILED:
                state = STATE.IDLE;
                // Send an action-outcome percept, of course, even if the execution of an action ended with failure (but the same should hold for interruption as well, in general)...
                StartCoroutine(SendPerception());
                break;
        }
    }

    void PickKey()
	{
        if (Random.value <= pickKeySuccessProbability)
        {
            hasKey = true;
            state = STATE.ACTION_SUCCESS;
        }
        else
        {
            // Currently, in this simple example, there is not much meaning to indicating, that the execution of a game-action (e.g. now PickKey) failed. Makes no difference, really, in this simple (minigame-like)example... :)
            state = STATE.ACTION_FAILED;
        }
        Debug.Log("[fA_DoorKey2_Agent::PickKey] state = " + state.ToString() + " (pickKeySuccessProbability = " + pickKeySuccessProbability + ")");
	}

    void Escape()
    {
        escaped = true;
        state = STATE.IDLE;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.name == key.name && state == STATE.ACTION_SUCCESS)
            key.gameObject.SetActive(false);
        else if (col.name == door.name && hasKey)
            Init();            
    }

    void OnTriggerExit(Collider col)
    {
        if (col.name == key.name)
        {
            key.gameObject.SetActive(false);
        }
    }

    bool Arrived(Transform destination)
    {
        if (destination != null)
        {
            if (Vector3.Distance(transform.position, destination.position) < 1f)
            {
                return true;
            }
            else
            {
                navAgent.SetDestination(destination.position);
                return false;
            }
        }

        return false;
    }

    IEnumerator SendPerception()
    {
        yield return new WaitForSeconds(0.1f);

        // Provide an action-outcome percept to the agent, after an action ended. The percept should not include anything about the environment, or anything visual, just perceive self (which has all the information needed)...
        AIBrainAPI.Instance.SendPerceptActionOutcomeToAI(this, AIBrainAPI.Instance.GenerateCompleteVisualJsonForAIEntityAgent(this, false, true, false).ToString());
        // NOTE: actually in this, slightly more complex DoorKey example (DoorKey2 w/ uncertainty) the contents of this action-outcome percept matter already. It is not enough for the agent to know, that it's action (pick-key or exit) finished execution in the game-environment, when it receives the action-outcome percept, but it should also know, whether e.g. as a result of pick-key, it really has the key, or not. So the contents of the above created an sent percept do matter now in this case, unlike in DoorKey1 (deterministic) example...
    }

    public override void ReceiveActionFromAI(string actionID, long timestamp, Dictionary<string, string> parameters)
    {
        switch(actionID)
        {
            case "pick-key":
                state = STATE.MOVING_TO_PICK_KEY;
                break;
            case "escape":
                state = STATE.MOVING_TO_ESCAPE;
                break;
        }
    }

    public override void ReceiveCommandFromAI(string commandID, long timestamp, Dictionary<string, string> parameters)
    {
    }

    public override string GetPropertyValue(string PropertyName, AIEntityAgent sensingAgent, AIBrainSensor sensor)
    {
        switch (PropertyName)
        {
            case "x-coord":
                return this.transform.position.x.ToString();
            case "z-coord":
                return this.transform.position.z.ToString();
            case "entity-type":
                return this.AIEntityTypeName;
            case "entity-name":
                return this.entityName;
            case "haskey":
                return hasKey ? "true" : "false";
            case "escaped":
                return escaped ? "true" : "false";
            case "door-key-agent-name":
                return gameObject.name;
        }
        return "";
    }
}
