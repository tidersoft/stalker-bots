==============================================
fAutonomy Example 002: DoorKey - deterministic
==============================================

The single fAutonomy-based agent is picking up a key in order to escape through a door.

The goal of the agent is to escape, and so it decides to pick-up the key first, because that is needed to escape through the door.

This example is intended to show in the most simple way, how goal-driven AI behaviour can be achieved with fAutonomy.

Please open the Scene/fA_DoorKey1_Scene.unity scene and click play.

For the details of AI-implementation please open the above scene, and enter the Tools / fAutonomy / AI Scene setup or AI behaviour setup menu in the Unity Editor.

For more information please check the quick user guide, and the user manual, included in the package.