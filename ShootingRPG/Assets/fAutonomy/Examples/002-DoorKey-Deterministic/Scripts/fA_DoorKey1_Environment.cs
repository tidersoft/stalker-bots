﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using AIBrain;

public class fA_DoorKey1_Environment : AIEntityEnvironment
{
    void Start()
    {
        Initialize();
    }

    public void Initialize()
    {
        // Initialize fAutonomy AI in this AI Scene...
        AIBrainAPI.Instance.InitialiseSceneAI(SceneManager.GetActiveScene().name, this);

        // Send a complete initial percept to each agent separately...
        foreach (AIEntityAgent agent in AIBrainAPI.Instance.GetAIBrainAgentEntitiesList())
        {
            string jsonData = AIBrainAPI.Instance.GenerateCompleteVisualJsonForAIEntityAgent(agent).ToString();

            Debug.Log("[fA_DoorKey1_Envrionment::Initialize/SendPerceptInitialToAI] agent.entityName = " + agent.entityName + "\njsonData = " + jsonData);

            AIBrainAPI.Instance.SendPerceptInitialToAI(new List<AIEntityAgent>() { agent }, jsonData);
        }
    }

    void Update()
    {
        AIBrainAPI.Instance.AIBrainUpdate();
    }

    public override string GetPropertyValue(string PropertyName)
    {
        return "";
    }
}
