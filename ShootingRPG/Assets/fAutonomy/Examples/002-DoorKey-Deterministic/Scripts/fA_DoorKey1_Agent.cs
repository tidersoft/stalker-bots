﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using AIBrain;

public class fA_DoorKey1_Agent : AIEntityAgent {

	NavMeshAgent navAgent;

    private bool hasKey = false;
    private bool escaped = false;

    public Transform key;
    public Transform door;

    private STATE state = STATE.IDLE;
    private enum STATE { MOVING_TO_PICK_KEY, MOVING_TO_ESCAPE, IDLE, ACTION_SUCCESS } 

	// Use this for initialization
	void Start ()
    {
        navAgent    = GetComponent<NavMeshAgent>();
    }

    void Init()
    {        
        hasKey = false;
        escaped = false;
        key.gameObject.SetActive(true);
        key.position = new Vector3(Random.Range(-9, 9), key.position.y, Random.Range(-9, 9));
        door.position = new Vector3(Random.Range(-9, 9), door.position.y, Random.Range(-9, 9));

        GameObject.FindObjectOfType<fA_DoorKey1_Environment>().Initialize();
    }
	
	// Update is called once per frame
	void Update () {

        CheckState();

    }

    void CheckState()
    {
        switch(state)
        {
            case STATE.MOVING_TO_PICK_KEY:
                if(Arrived(key))
                {
                    PickKey();
                }
                break;
            case STATE.MOVING_TO_ESCAPE:
                if (Arrived(door))
                {
                    Escape();
                }
                break;
            case STATE.ACTION_SUCCESS:
                state = STATE.IDLE;
                StartCoroutine(SendPerception());
                break;
        }
    }

    void PickKey()
	{
        hasKey = true;
        state = STATE.ACTION_SUCCESS;
	}

    void Escape()
    {
        escaped = true;
        state = STATE.IDLE;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.name == key.name)
            key.gameObject.SetActive(false);
        else if (col.name == door.name && hasKey)
            Init();            
    }

    bool Arrived(Transform destination)
    {
        if (destination != null)
        {
            if (Vector3.Distance(transform.position, destination.position) < 1f)
            {
                return true;
            }
            else
            {
                navAgent.SetDestination(destination.position);
                return false;
            }
        }

        return false;
    }

    IEnumerator SendPerception()
    {
        yield return new WaitForSeconds(0.1f);

        // Provide an action-outcome percept to the agent, after an action ended. The percept should not include anything about the environment, or anything visual, just perceive self (which has all the information needed)...
        AIBrainAPI.Instance.SendPerceptActionOutcomeToAI(this, AIBrainAPI.Instance.GenerateCompleteVisualJsonForAIEntityAgent(this, false, true, false).ToString());
        // NOTE: actually in this most simple, deterministic example only the fact, that an action-outcome percept was sent/received, matters, so that the agent knows, when to schedule a next action. The contents of the percept actually don't even matter in this simple case...
    }

    public override void ReceiveActionFromAI(string actionID, long timestamp, Dictionary<string, string> parameters)
    {
        switch(actionID)
        {
            case "pick-key":
                state = STATE.MOVING_TO_PICK_KEY;
                break;
            case "escape":
                state = STATE.MOVING_TO_ESCAPE;
                break;
        }
    }

    public override void ReceiveCommandFromAI(string commandID, long timestamp, Dictionary<string, string> parameters)
    {
    }

    public override string GetPropertyValue(string PropertyName, AIEntityAgent sensingAgent, AIBrainSensor sensor)
    {
        switch (PropertyName)
        {
            case "x-coord":
                return this.transform.position.x.ToString();
            case "z-coord":
                return this.transform.position.z.ToString();
            case "entity-type":
                return this.AIEntityTypeName;
            case "entity-name":
                return this.entityName;
            case "haskey":
                return hasKey ? "true" : "false";
            case "escaped":
                return escaped ? "true" : "false";
        }
        return "";
    }
}
