=================================
fAutonomy Example 001: HelloWorld
=================================

The single fAutonomy-based agent is switching the light in the scene ON and OFF and back again.

This most simple example is intended to kickstart development with fAutonomy for Unity.

Please open the Scene/fA_HelloWorld_Scene.unity scene and click play.

For the details of AI-implementation please open the above scene, and enter the Tools / fAutonomy / AI Scene setup or AI behaviour setup menu in the Unity Editor.

For more information please check the quick user guide, and the user manual, included in the package.