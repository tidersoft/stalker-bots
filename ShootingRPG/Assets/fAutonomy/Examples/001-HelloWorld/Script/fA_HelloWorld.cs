﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AIBrain;

public class fA_HelloWorld : AIEntityAgent {

    void TurnOn()
    {
        GameObject.FindObjectOfType<fA_HelloWorld_Environment>().SetActiveLight(true);
        StartCoroutine(SendPerception());
    }

    void TurnOff()
    {
        GameObject.FindObjectOfType<fA_HelloWorld_Environment>().SetActiveLight(false);
        StartCoroutine(SendPerception());
    }

    IEnumerator SendPerception()
    {
        yield return new WaitForSeconds(1f);
        AIBrainAPI.Instance.SendPerceptActionOutcomeToAI(this, AIBrainAPI.Instance.GenerateCompleteVisualJsonForAIEntityAgent(this).ToString());
    }

    public override string GetPropertyValue(string PropertyName, AIEntityAgent sensingAgent, AIBrainSensor sensor)
    {
        if (PropertyName == "x-coord")
        {
            return transform.position.x.ToString();
        }
        else if (PropertyName == "z-coord")
        {
            return transform.position.z.ToString();
        }
        else if (PropertyName == "entity-type")
        {
            return this.AIEntityTypeName;
        }
        else if (PropertyName == "entity-type")
        {
            return entityName;
        }
        return "";
    }

    public override void ReceiveActionFromAI(string actionID, long timestamp, Dictionary<string, string> parameters)
    {
        if (actionID == "turnon")
        {
            TurnOn();
        }
        else if (actionID == "turnoff")
        {
            TurnOff();
        }
    }

    public override void ReceiveCommandFromAI(string commandID, long timestamp, Dictionary<string, string> parameters)
    {
        throw new System.NotImplementedException();
    }

}
