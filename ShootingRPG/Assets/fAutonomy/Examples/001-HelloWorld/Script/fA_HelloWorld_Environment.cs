﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using AIBrain;

public class fA_HelloWorld_Environment : AIEntityEnvironment
{

    private bool isLightOn = false;

    void Start()
    {
        // Initialize fAutonomy AI in this AI Scene...
        AIBrainAPI.Instance.InitialiseSceneAI(SceneManager.GetActiveScene().name, this);

        // Send a complete initial percept to each agent separately...
        foreach (AIEntityAgent agent in AIBrainAPI.Instance.GetAIBrainAgentEntitiesList())
        {
            string jsonData = AIBrainAPI.Instance.GenerateCompleteVisualJsonForAIEntityAgent(agent).ToString();

            Debug.Log("[fA_HelloWorld_Environment::Start/SendPerceptInitialToAI] agent.entityName = " + agent.entityName + "\njsonData = " + jsonData);

            AIBrainAPI.Instance.SendPerceptInitialToAI(new List<AIEntityAgent>() { agent }, jsonData);
        }

    }

    // Update is called once per frame
    void Update()
    {
        AIBrainAPI.Instance.AIBrainUpdate();
    }

    public override string GetPropertyValue(string PropertyName)
    {
        return PropertyName == "islighton" && isLightOn ? "true" : "false";
    }

    public void SetActiveLight(bool tf)
    {
        isLightOn = tf;
        GetComponent<Light>().enabled = isLightOn;
    }

}
