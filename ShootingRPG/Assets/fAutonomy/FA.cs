﻿//-----------------------------------------------------
// fAutonomy for Unity
// Copyright (c) 2018 AIBrain Inc. All rights reserved.
//-----------------------------------------------------

using UnityEngine;

namespace AIBrain
{
    /// <summary>
    /// Please attach this C# script/class (as a component) to those Unity GameObject-s (or Prefabs), which you may want to include in a fAutonomy AIScene.
    /// Adding this script to a Unity GameObject of course does not mean, that the GameObject is going to be part of the AIScene.
    /// It is necessary, but not sufficient to have this script attached, in order to include a GameObject in an AIScene.
    /// To include a GameObject (with FA.cs attached) in an AIScene, please map it in STEP 2/3 of AIScene Setup in fA.
    /// Only GameObject-s with FA.cs attached can be mapped in STEP 2/3 of AIScene Setup.
    /// </summary>
    public class FA : MonoBehaviour {}
}
