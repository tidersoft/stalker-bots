========================================================
			  fAutonomy for Unity (tm)

			    Open Beta (v0.95)
			
			  QUICK SERVER-SETUP GUIDE
	
  Copyright (c) 2018 AIBrain Inc. All Rights Reserved.

  Unity Asset Store:
  http://u3d.as/1126
  
  Website:
  http://fautonomy.ai
  
  Forum:
  https://forum.unity.com/threads/released-fautonomy-for-unity-free-open-beta-worlds-1st-deep-learning-ai-plugin-for-unity.535740
  
  Quick User Guide:
  http://fautonomy.ai/file/QuickUserGuide.pdf
  
========================================================
HOW TO SET UP THE CONNECTION TO THE fAUTONOMY AI SERVERS

1. fAutonomy for Unity can be accessed fully for free, the user does not have to register on any external website to use the product. However training of AI Behaviours (using state-of-the-art Machine Learning) is performed on the server-side, using the fAutonomy AI servers. Accordingly, in order to be able to train AI Behaviours with the tool, the user has to set the IP address and port number of the fAutonomy AI servers in the Tools / fAutonomy / Preferences menu.

Please make sure that the AI server related information is entered correctly in the Tools / fAutonomy / Preferences menu, and fix it, if necessary. The correct information is the following:

	IP address:  http://211.118.38.59
	PORT number: 80

If that is set in the Tools / fAutonomy / Preferences menu, then please click on the "Test Connection" button. In case the server is accessible to you (e.g. your firewall is not blocking Unity, etc.), then you should receive a pop-up, that confirms, that the server is accessible. In this case you're good to go. You can now begin using fAutonomy for Unity in unregistered mode (skipping all next steps).

	IMPORTANT: to use fAutonomy for Unity in unregistered mode, please make sure that the username, password and SID fields are empty  in the Tools / fAutonomy / Preferences menu.

2. In case you intend to use fAutonomy for Unity in registered mode, that provides better AI server-side services, then please visit the fautonomy.ai website, and create a free account by clicking on the "Sign Up" link in the top-right corner of the screen.

3. Please provide your user credentials (such as your active email address, a password, etc.), and click on "Sign Up". Please remember these user-credentials.

4. With the provided user-credentials you can now "Sign In" on the fautonomy.ai website as a registered user, and see your user details, including your unique SID (Subscription ID). You are going to need this SID to use the product in registered mode, so please copy your SID to the clipboard (Ctrl+C on Windows).

	NOTE 1: the default SID received after website-registration provides unlimited-time free subscription.

	NOTE 2: the data provided during registration is not used for any commercial purpose. The fAutonomy for Unity client exchanges only the user-credentials, the SID and AI Behaviours with the fAutonomy AI servers. No other information or data is shared by the fAutonomy for Unity client with the fAutonomy AI servers. AI Behaviours are shared with the AI servers only in case of training.

5. After completing the previous step (STEP #4), please open your Unity project, import the fAutonomy unitypackage, and access the Tools / fAutonomy / Preferences menu on the top of the Unity Editor.

6. In the Preferences menu some things are already pre-set for you, for your convenience, however you have to enter your username, password and SID to access the fAutonomy AI servers in registered mode. Please provide all three pieces of data to do that. Reminder: you can also fully use fAutonomy for Unity in unregistered mode, as described in STEP 1 (cf. above).

7. After entering your user-credentials, please click on "Verify Details" button under your credentials. Please make sure, that your username, password and SID are all correct.

	NOTE: In case of successful verification you can start using the product by accessing the Tools / fAutonomy / AI Scene setup ...or... AI Behaviour setup menus (to make an AI Scene of your currently active Unity scene, and populate it with AI behaviours, which can then be trained into DNNs (Deep Neural Networks), that can drive your agents in the scene in your game).

	
===================================================
LICENSE / TERMS OF USE / END USER LICENSE AGREEMENT

By using AIBrain's fAutonomy for Unity the user agrees to the following.

Made with fAutonomy – this has to be included on one of the first flash screens, with large characters, similarly to "Made with Unity", when a game made with fAutonomy starts. A game is made with fAutonomy, if fAutonomy was used during the development of the Unity game to create the AI of the game, which is part of the distributed game. This means, that if fAutonomy is used by a game, then that game should acknowledge that fact clearly at startup.

Copyright (c) 2018 AIBrain Inc. All Rights Reserved.